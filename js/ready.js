
$(".buttonstr").hover(
        function () {
            $(this).find(".btnsit").fadeIn();
        }, function () {
    $(this).find(".btnsit").fadeOut();
}
);
$(".buttonman").hover(
        function () {
            $(this).find(".btnman").fadeIn();
        }, function () {
    $(this).find(".btnman").fadeOut();
}
);
$(".buttonpru").hover(
        function () {
            $(this).find(".btnpru").fadeIn();
        }, function () {
    $(this).find(".btnpru").fadeOut();
}
);
$(".buttonvis").hover(
        function () {
            $(this).find(".btnvis").fadeIn();
        }, function () {
    $(this).find(".btnvis").fadeOut();
}
);
$(".buttonfin").hover(
        function () {
            $(this).find(".btnfin").fadeIn();
        }, function () {
    $(this).find(".btnfin").fadeOut();
}
);

$("li").on("click", function () {
    $(".nav navbar-nav li").removeClass("active");
    $(this).addClass("active");
});

Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
    return {
        radialGradient: {
            cx: 0.5,
            cy: 0.3,
            r: 0.7
        },
        stops: [
            [0, color],
            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
        ]
    };
});

document.getElementById('lnkweb').addEventListener('click', function (event) {
    event.preventDefault(); //esto cancela el comportamiento del click
    setTimeout(function () {
        $("#modalAlerta").modal('show');
        $('#modalwait').fadeIn('fast');
        //location.href = "web";        
    }, 200);
});
document.getElementById('lnklla').addEventListener('click', function (event) {
    event.preventDefault(); //esto cancela el comportamiento del click
    setTimeout(function () {
        $("#modalAlerta").modal('show');
        $('#modalwait').fadeIn('fast');
//        location.href = "llamada";
    }, 200);
});
document.getElementById('lnkcha').addEventListener('click', function (event) {
    event.preventDefault(); //esto cancela el comportamiento del click
    setTimeout(function () {
        $("#modalAlerta").modal('show');
        $('#modalwait').fadeIn('fast');
//        location.href = "chat";
    }, 200);
});
document.getElementById('lnktot').addEventListener('click', function (event) {
    event.preventDefault(); //esto cancela el comportamiento del click
    setTimeout(function () {
        $("#modalAlerta").modal('show');
        $('#modalwait').fadeIn('fast');
//        location.href = "oportunidad";
    }, 200);
});


