function calendar(input) {
    $('#cal_' + input).datetimepicker({
        inline: false,
        timepicker: false,
        format: 'Y-m-d',
        onChangeDateTime: function (dp, $input) {
            $("#GestionComercial_" + input + "_Fecha_visita").val($input.val());
        }
    });
}

function error() {
    var args = arguments.length;
    switch (args) {
        case 1:
            swal("Error", arguments[0], "error");
            break;
        case 2:
            swal(arguments[0], arguments[1], "error");
            break;
        case 3:
            swal(arguments[0], arguments[1], arguments[2]);
            break;
    }
}

function exito() {
    var args = arguments.length;
    switch (args) {
        case 1:
            swal("Hecho", arguments[0], "success");
            break;
        case 2:
            swal(arguments[0], arguments[1], "success");
            break;
        case 3:
            swal(arguments[0], arguments[1], arguments[2]);
            break;
    }
}



function confirmar() {
    var accion = undefined;
    var args = arguments.length;
    switch (args) {
        case 1:
            var params = {
                title: "Confirmar",
                text: arguments[0]
            };
            break;
        case 2:
            var params = {
                title: arguments[0],
                text: arguments[1]
            };
            break;
        case 3:
            var params = {
                title: arguments[0],
                text: arguments[1],
                type: arguments[2]
            };
            break;
        case 4:
            var params = {
                title: arguments[0],
                text: arguments[1],
                type: arguments[2]
            };
            var accion = arguments[3];
            break;
    }

    var base = {
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        closeOnConfirm: true,
        closeOnCancel: true
    };

    if (accion !== undefined) {
        swal($.extend(params, base), function (pass) {
            accion(pass);
        });
    } else {
        swal($.extend(params, base));
    }
}

function notydesktop(ultid) {
    jQuery.ajax({
        'type': 'POST',
        'url': '/telemark/index.php/site/shownoty',
        'data': 'ultid=' + parseInt(ultid),
        'success': function (respuesta) {
            if (respuesta > 0) {
                showNotification('Registro Nuevo', 'Campaña Web');
            }
        },
        'cache': false
    });
    return false;
}


var n = 1;
function showNotification(message, z) {
    var icon = 'http://www.telesentinel.com/telemark/images/teleweb.png';
    var notif = showWebNotification('Mensaje TELEMARK ', message + '!\n Sin Ver : ' + z, icon, null, 7000);
    //handle different events
    notif.addEventListener("show", Notification_OnEvent);
    notif.addEventListener("click", Notification_OnEvent);
    notif.addEventListener("close", Notification_OnEvent);
    n++;
}

function Notification_OnEvent(event) {
    //A reference to the Notification object
    //var notif = event.currentTarget;
    //document.getElementById("msgs").innerHTML += "<br>Notification <strong>'" + notif.title + "'</strong> received event '" + event.type + "' at " + new Date().toLocaleString();
}


function spanCalendar(picker, range) {
    var date;
    $("#"+range).val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
    date = picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY');
    jQuery.ajax({
        'type': 'POST',
        'url': '/telelego/index.php/site/span',
        'data': 'span=' + date + '&range=' + range,
        'success': function (respuesta) {
            console.log(respuesta);
            $('#report'+range+' span').html(date);
        },
        'cache': false
    });
    return false;
}

function contadores(){
    jQuery.ajax({
        'type': 'POST',
        'url': '/telelego/index.php/site/contadores',
        'beforeSend' : function(){
            $('.contadores').fadeOut('fast');
        },
        'success': function (respuesta) {
            $('.contadores').html(respuesta);
            $('.contadores').fadeIn('fast');
        },
        'cache': false
    });
    return false;
}