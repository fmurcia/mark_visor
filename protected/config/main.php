<?php

/**
 * Configuracion de Componentes
 * @author Gustavo Carvajal <gcarvajal@telesentinel.com>
 */
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'ADMTELELEGO',
    'id' => '<id>',
    // preloading 'log' component
    'preload' => array(
        'log',
        'booster',
        'highcharts'
    ),
    'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'application.components.RequireLogin'
        )
    ),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '0v3rl0ad',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'booster' => array(
            'class' => 'ext.booster.components.Booster',
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',    
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        //Conexion a BD
        'db' => require(dirname(__FILE__) . '/database.php'),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
         ////////////////////////////ACTIVAR//////////////////////////
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CDbLogRoute',
                    'levels' => 'error,warning',
                    'autoCreateLogTable' => true,
                    'connectionID' => 'db',
                ),
                array(
                    'class' => 'CProfileLogRoute',
                    'report' => 'summary',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // Notificaciones PUSH APP
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'gcarvajal@telesentinel.com',
        'restUrl' => 'rest'
    ),
);
