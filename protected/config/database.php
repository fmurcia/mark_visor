<?php
// Conexion a la Base de Datos
return array(
    // uncomment the following lines to use a MySQL database
    'connectionString' => 'mysql:host=<host>;dbname=<database>',
    'emulatePrepare' => true,
    'username' => '<user>',
    'password' => '<passwd>',
    'charset' => 'utf8',
);
                