<?php

/**
 * Controlador de Eventos del Sitio Principal
 * Clase para el Manejo de Controladores Iniciales
 * 
 * @author Gustavo Carvajal <gcarvajal@telesentinel.com>
 * @version 1.0
 * 
 * @method Void MenuSuperior() Carga el Menu Superior de la Vista Inicial
 * @method Void Alertas() Carga las Alertas de la Aplicacion
 * @method Void Pausaactiva() Carga el Menu con la Pausa Seleccionada
 * @method Void Pausas() Carga el Menu de Pausas
 * @method Void Tablacerrados() Carga las Solicitudes Cerradas
 * @method Void Tablacontenido() Tabla de Contenidos
 * @method Void Tablamantenimiento() Carga las Solicitudes en Mantenimiento
 * @method Void Tablapruebas() Tabla de Pruebas Generadas
 * @method Void Tablasitio() Carga los Tecnicos en Sitio
 * @method Void Tablavistas() Carga las Solicitudes Vistas
 */
class VentasController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }
    
    /**
     * 
     * @param type $estado
     * @return type
     */
    public function unidadNegocioAgencia() {

        $critreal = new CDbCriteria();
        $critreal->select = 'IDRegionalCliente, AgenciaCliente, Regional, Agencia, CodigoServicioDetalle, Codigo4DDetalleServicio, SUM(ValorServicioDetalle) as Total';
        $critreal->addCondition('CierreClienteVenta = 48 AND Codigo4DDetalleServicio != 27');   
        $critreal->addCondition('EstadoDetalle = 2');
        $critreal->group = 'Agencia, CodigoServicioDetalle';
        $critreal->order = 'Agencia ASC';
        return XVentas::model()->findAll($critreal);
    }
}
