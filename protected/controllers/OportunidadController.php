<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DashboardController
 *
 * @author Gus
 */
class OportunidadController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'qryrender', 'detallado', 'detcomercial'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        /* Fecha */
        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $tercerdia = Yii::app()->user->getState('tercerdia');
        $cuartodia = Yii::app()->user->getState('cuartodia');

        $tipocontacto = 2;

        /* Regionales */
        $regionales = Contacto::model()->getRegionales();
        $arrreg = array();

        /* Estados */
        $arr_int = array(1); // 1
        $arr_des = array(2, 3, 4, 6, 7, 15, 16, 17, 18, 20); // 10
        $arr_seg = array(5, 19, 24); // 4

        $arr_cit = array(8); // 1
        $arr_cot = array(9, 10, 11, 12); // 4
        $arr_ven = array(13, 14); //2

        $item = array('ASIGNADO' => 8, 'PRESUPUESTO' => 16, 'COMPETENCIA' => 17, 'ERRADO' => 7);
        $items = array('INTERESADOS' => 1, 'SEGUIMIENTO' => 5, 'VOLVER A LLAMAR' => 24, 'REAGENDADOS' => 19, 'SOLO INFO' => 18, 'NO CONTESTA' => 6, 'DESCARTADO' => 15, 'NO LE INTERESA' => 20, 'DUPLICADO' => 2, 'TRABAJO' => 4, 'PQR' => 3, 'PRESUPUESTO' => 16, 'COMPETENCIA' => 17, 'ERRADO' => 7);

        $arr_medios = array();

        $leads_general = array('LEADS PENDIENTES' => $arr_int, 'DESCARTADOS' => $arr_des, 'EN GESTION' => $arr_seg, 'CITAS' => $arr_cit, 'COTIZACIONES' => $arr_cot, 'VENTAS' => $arr_ven);
        /* Contadores */
        $nuevos = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_int, $arrreg, $arr_medios); //interesado
        $descartados = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_des, $arrreg, $arr_medios);
        $seguimiento = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_seg, $arrreg, $arr_medios);
        $cita = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_cit, $arrreg, $arr_medios);
        $cotizados = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_cot, $arrreg, $arr_medios);
        $venta = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_ven, $arrreg, $arr_medios);

        $intervalo = Contacto::model()->getFechas($primerdia, $segundodia, 'P1D');

        $categorias = array();
        $arr_chat = array();
        foreach ($intervalo as $i) :
            $categorias[] = $i;
            $arr_chat[] = Contacto::model()->totalCampania($i, $tipocontacto, '', $arrreg);
        endforeach;

        $totalregistrado = $descartados + $nuevos + $seguimiento + $cita + $cotizados + $venta;

        /* Total Ventas */
        $totalvencitas = Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ven, 'con');

        /* Total Cotizaciones */
        $totalcotcitas = Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cot, 'con');

        /* Presupuesto Asignado Competencia Errado CON CITA */
        $totalestcitas = 0;
        foreach ($item as $i => $a) :
            $totalestcitas += Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $primerdia, $segundodia, $arrreg, array($a), 'con');
        endforeach;

        /* SIN CITA */
        $totalsincita = 0;
        foreach ($items as $i => $a) :
            $totalsincita += Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $primerdia, $segundodia, $arrreg, array($a), 'sin');
        endforeach;

        $totalconcita = $totalvencitas + $totalcotcitas + $totalestcitas;

        $grafica = array(array('name' => 'Oportunidad', 'data' => $arr_chat));

        $arr_tt = array('descartados' => $descartados, 'nuevos' => $nuevos, 'seguimiento' => $seguimiento, 'cita' => $cita, 'cotizados' => $cotizados, 'venta' => $venta);

        if (isset($_POST['comparacion']) && $_POST['comparacion'] == 'true') :
            $display = 'block';
        else :
            $display = 'none';
        endif;

        $this->render('index', array(
            'leads_general' => $leads_general,
            'tipocontacto' => $tipocontacto,
            'regionales' => $regionales,
            'item' => $item,
            'items' => $items,
            'categorias' => $categorias,
            'grafica' => $grafica,
            'primerdia' => $primerdia,
            'segundodia' => $segundodia,
            'tercerdia' => $tercerdia,
            'cuartodia' => $cuartodia,
            'intervalo' => $intervalo,
            'arr_tt' => $arr_tt,
            'arr_reg' => $arrreg,
            'arr_medios' => $arr_medios,
            'arr_ven' => $arr_ven,
            'arr_cot' => $arr_cot,
            'totalconcita' => $totalconcita,
            'totalsincita' => $totalsincita,
            'totalvencitas' => $totalvencitas,
            'totalcotcitas' => $totalcotcitas,
            'totalestcitas' => $totalestcitas,
            'totalregistrado' => $totalregistrado,
                )
        );
    }

    public function actionQryrender() {

        $primerdia = $_POST['fecha_rango1'];
        $segundodia = $_POST['fecha_rango2'];

        $tercerdia = $_POST['fecha_rango3'];
        $cuartodia = $_POST['fecha_rango4'];

        Yii::app()->user->setState('primerdia', $primerdia);
        Yii::app()->user->setState('segundodia', $segundodia);
        Yii::app()->user->setState('tercerdia', $tercerdia);
        Yii::app()->user->setState('cuartodia', $cuartodia);

        $tipocontacto = 2;

        /* Regionales */
        $arrreg = array();
        if ($_POST['regional'] != 'all') :
            $arrreg = array($_POST['regional']);
        endif;
        Yii::app()->user->setState('regional', $arrreg);

        /* Estados */
        $arr_int = array(1); // 1
        $arr_des = array(2, 3, 4, 6, 7, 15, 16, 17, 18, 20); // 10
        $arr_seg = array(5, 19, 24); // 4

        $arr_cit = array(8); // 1
        $arr_cot = array(9, 10, 11, 12); // 4
        $arr_ven = array(13, 14); //2

        $item = array('ASIGNADO' => 8, 'PRESUPUESTO' => 16, 'COMPETENCIA' => 17, 'ERRADO' => 7);
        $items = array('INTERESADOS' => 1, 'SEGUIMIENTO' => 5, 'VOLVER A LLAMAR' => 24, 'REAGENDADOS' => 19, 'SOLO INFO' => 18, 'NO CONTESTA' => 6, 'DESCARTADO' => 15, 'NO LE INTERESA' => 20, 'DUPLICADO' => 2, 'TRABAJO' => 4, 'PQR' => 3, 'PRESUPUESTO' => 16, 'COMPETENCIA' => 17, 'ERRADO' => 7);

        $arr_medios = array();

        $leads_general = array('LEADS PENDIENTES' => $arr_int, 'DESCARTADOS' => $arr_des, 'EN GESTION' => $arr_seg, 'CITAS' => $arr_cit, 'COTIZACIONES' => $arr_cot, 'VENTAS' => $arr_ven);

        /* Contadores */
        $nuevos = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_int, $arrreg, $arr_medios); //interesado
        $descartados = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_des, $arrreg, $arr_medios);
        $seguimiento = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_seg, $arrreg, $arr_medios);
        $cita = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_cit, $arrreg, $arr_medios);
        $cotizados = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_cot, $arrreg, $arr_medios);
        $venta = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $arr_ven, $arrreg, $arr_medios);

        $nuevos2 = Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $arr_int, $arrreg, $arr_medios); //interesado
        $descartados2 = Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $arr_des, $arrreg, $arr_medios);
        $seguimiento2 = Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $arr_seg, $arrreg, $arr_medios);
        $cita2 = Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $arr_cit, $arrreg, $arr_medios);
        $cotizados2 = Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $arr_cot, $arrreg, $arr_medios);
        $venta2 = Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $arr_ven, $arrreg, $arr_medios);

        $intervalo = Contacto::model()->getFechas($primerdia, $segundodia, 'P1D');
        $intervalo2 = Contacto::model()->getFechas($tercerdia, $cuartodia, 'P1D');

        $arr_chat = array();
        $arr_chat2 = array();
        $categorias = array();
        foreach ($intervalo as $i) :
            $categorias[] = $i;
            $arr_chat[] = Contacto::model()->totalCampania($i, $tipocontacto, '', $arrreg);
        endforeach;

        $categorias2 = array();
        foreach ($intervalo2 as $i) :
            $categorias2[] = $i;
            $arr_chat2[] = Contacto::model()->totalCampania($i, $tipocontacto, '', $arrreg);
        endforeach;

        $totalregistrado = $descartados + $nuevos + $seguimiento + $cita + $cotizados + $venta;
        $totalregistrado2 = $descartados2 + $nuevos2 + $seguimiento2 + $cita2 + $cotizados2 + $venta2;

        /* Total Ventas */
        $totalvencitas = Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ven, 'con');

        /* Total Cotizaciones */
        $totalcotcitas = Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cot, 'con');

        /* Total Ventas */
        $totalvencitas2 = Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_ven, 'con');

        /* Total Cotizaciones */
        $totalcotcitas2 = Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_cot, 'con');

        /* Presupuesto Asignado Competencia Errado CON CITA */
        $totalestcitas = 0;
        $totalestcitas2 = 0;
        foreach ($item as $i => $a) :
            $totalestcitas += Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $primerdia, $segundodia, $arrreg, array($a), 'con');
            $totalestcitas2 += Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $tercerdia, $cuartodia, $arrreg, array($a), 'con');
        endforeach;

        /* SIN CITA */
        $totalsincita = 0;
        $totalsincita2 = 0;
        foreach ($items as $i => $a) :
            $totalsincita += Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $primerdia, $segundodia, $arrreg, array($a), 'sin');
            $totalsincita2 += Contacto::model()->estadosRegistrosLlamada($arr_medios, $tipocontacto, $tercerdia, $cuartodia, $arrreg, array($a), 'sin');
        endforeach;

        $totalconcita = $totalvencitas + $totalcotcitas + $totalestcitas;
        $totalconcita2 = $totalvencitas2 + $totalcotcitas2 + $totalestcitas2;

        $grafica = array(array('name' => 'Oportunidad', 'data' => $arr_chat));
        $grafica2 = array(array('name' => 'Oportunidad', 'data' => $arr_chat2));

        $arr_tt = array('descartados' => $descartados, 'nuevos' => $nuevos, 'seguimiento' => $seguimiento, 'cita' => $cita, 'cotizados' => $cotizados, 'venta' => $venta,
            'descartados2' => $descartados2, 'nuevos2' => $nuevos2, 'seguimiento2' => $seguimiento2, 'cita2' => $cita2, 'cotizados2' => $cotizados2, 'venta2' => $venta2);

        if (isset($_POST['comparacion']) && $_POST['comparacion'] == 'true') :
            $display = 'block';
        else :
            $display = 'none';
        endif;

        $space = 2;
        $space1 = 1;
        $space2 = 1;
        $campania = "";

        $this->renderPartial('contenido', array(
            'leads_general' => $leads_general,
            'tipocontacto' => $tipocontacto,
            'oculto' => $display . ';color:#FF8316',
            'graficaoculta' => $display,
            'comparacion' => $_POST['comparacion'],
            'item' => $item,
            'items' => $items,
            'categorias' => $categorias,
            'categorias2' => $categorias2,
            'grafica' => $grafica,
            'grafica2' => $grafica2,
            'primerdia' => $primerdia,
            'segundodia' => $segundodia,
            'intervalo' => $intervalo,
            'campania' => $campania,
            'space' => $space,
            'space1' => $space1,
            'space2' => $space2,
            'arr_tt' => $arr_tt,
            'arr_reg' => $arrreg,
            'arr_medios' => $arr_medios,
            'arr_ven' => $arr_ven,
            'arr_cot' => $arr_cot,
            'totalconcita' => $totalconcita,
            'totalconcita2' => $totalconcita2,
            'totalsincita' => $totalsincita,
            'totalsincita2' => $totalsincita2,
            'totalvencitas' => $totalvencitas,
            'totalvencitas2' => $totalvencitas2,
            'totalcotcitas' => $totalcotcitas,
            'totalcotcitas2' => $totalcotcitas2,
            'totalestcitas' => $totalestcitas,
            'totalestcitas2' => $totalestcitas2,
            'totalregistrado' => $totalregistrado,
            'totalregistrado2' => $totalregistrado2,
                )
        );
    }
    
    public function actionDetallado() {

        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $arr_reg = Yii::app()->user->getState('regional');

        $texto = explode('-', $_POST['texto']);
        $estado = $texto[0];
        $medio = $texto[1];
        $tipocontacto = $_POST['tipo'];

        $leads = array();

        if ($medio == 'digital') :
            $leads = array('AOL.COM', 'ASK.COM', 'BING.COM', 'GOOGLE', 'PAGINAS AMARILLAS.COM', 'PESCA TELEVIDEO', 'PUBLICIDAD WEB', 'SEARCH.COM', 'TELESENTINEL.COM (ACCESO DIRECTO)', 'VENTAS WEB', 'YAHOO.COM', 'FACEBOOK', 'GOOGLE', 'INSTAGRAM', 'PINTEREST', 'TWITTER', 'DISPLAY', 'PINTEREST', 'DIRECTORIO DIGITAL', 'CHAT', 'CORREO ELECTRONICO');
        else :
            $leads = array('ACTIVO CON COMPETENCIA', 'CALCOMANIA Y/O PLACA', 'CENTRO COMERCIAL SANDIEGO', 'CENTRO COMERCIAL UNICENTRO', 'CLIENTE CORFERIAS', 'CLIENTE TELESENTINEL', 'CONOCE FUNCIONARIOS DE TELESENTINEL', 'DIRECTORIO PAG AMARILLAS', 'EXPOCAMACOL', 'FAJITAS', 'HOMECENTER BOGOTA', 'HOMECENTER MEDELLIN', 'LLAMADA 113', 'MAPFRE SEGUROS', 'OPORTUNIDAD TRIANGULO', 'PAUTA EN RADIO', 'PAUTA EN TV', 'FUE CLIENTE Y QUIERE VOLVER', 'STAN CENTROS COMERCIALES', 'VALLAS', 'VOLANTES', 'MAS DE UN CONTACTO', 'TOMA BARRIO', 'TELEMERCADEO', 'VISITA OFICINA', 'TRABAJO EN FRIO', 'REFERIDO Y/O AMIGO', 'FUE CLIENTE Y QUIERE VOLVER', 'RED 360Âº', 'DIRECTORIO IMPRESO', 'MOTOS', 'VALLAS', 'VOLANTES', 'EVENTO', 'BASE DE DATOS', 'INSTALE YA', 'TODOS A VENDER', 'CENTRO COMERCIAL PLAZA LAS AMERICAS', 'EXPOCONSTRUCCIONES', 'CENTRO COMERCIAL MAYALES', 'FERIA DEL HOGAR', 'FACTURA', 'OTROS', 'CLIENTE VIGENTE', 'CALCOMANIA');
        endif;

        $arr_status = array('LEADS PENDIENTES' => 0, 'DESCARTADOS' => 1, 'EN GESTION' => 2, 'CITAS' => 3, 'COTIZACIONES' => 4, 'VENTAS' => 5);

        $item = 0;
        foreach($arr_status as $ar => $n) :
            if($estado == 'engestion') :
                $item = 2;
            endif;
            if($ar == strtoupper($estado)) :
                $item = $n;
            endif;
        endforeach;

        if ($item == 0) :
            $arr_estado = array(1); // 1
        endif;
        if ($item == 1) :
            $arr_estado = array(2, 3, 4, 6, 7, 15, 16, 17, 18, 20); // 10
        endif;
        if ($item == 2) :
            $arr_estado = array(5, 19, 24); // 4
        endif;
        if ($item == 3) :
            $arr_estado = array(8); // 1
        endif;
        if ($item == 4) :
            $arr_estado = array(9, 10, 11, 12); // 4
        endif;
        if ($item == 5) :
            $arr_estado = array(13, 14); //2
        endif;

        $ttcon = Contacto::model()->statusTiposAsignado($tipocontacto, $primerdia, $segundodia, $arr_reg, array($es), $leads);

        $this->renderPartial('detallado', array('texto' => strtoupper($estado), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $medio, 'arr_reg' => $arr_reg, 'ttcon' => $ttcon, 'leads' => $leads));
    }

    public function actionDetcomercial() {

        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $arr_reg = Yii::app()->user->getState('regional');

        $estado = $_POST['texto'];
        $tipocontacto = $_POST['tipo'];

        $arr_status = array('LEADS PENDIENTES' => 0, 'DESCARTADOS' => 1, 'EN GESTION' => 2, 'CITAS' => 3, 'COTIZACIONES' => 4, 'VENTAS' => 5);

        $item = 0;
        foreach ($arr_status as $ar => $n) :
            if ($estado == 'engestion') :
                $item = 2;
            endif;
            if ($ar == strtoupper($estado)) :
                $item = $n;
            endif;
        endforeach;

        if ($item == 0) :
            $arr_estado = array(1); // 1
        endif;
        if ($item == 1) :
            $arr_estado = array(2, 3, 4, 6, 7, 15, 16, 17, 18, 20); // 10
        endif;
        if ($item == 2) :
            $arr_estado = array(5, 19, 24); // 4
        endif;
        if ($item == 3) :
            $arr_estado = array(8); // 1
        endif;
        if ($item == 4) :
            $arr_estado = array(9, 10, 11, 12); // 4
        endif;
        if ($item == 5) :
            $arr_estado = array(13, 14); //2
        endif;

        $ttcon = Contacto::model()->statusTiposAsignadoComercial($tipocontacto, $primerdia, $segundodia, $arr_reg, $arr_estado);

        $this->renderPartial('detallado_1', array('texto' => strtoupper($estado), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $estado, 'arr_reg' => $arr_reg, 'ttcon' => $ttcon));
    }
}