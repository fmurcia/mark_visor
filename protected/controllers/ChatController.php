<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DashboardController
 *
 * @author Gus
 */
class ChatController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'qryrender', 'detallado', 'detcomercial'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        /* Fecha */
        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $tercerdia = Yii::app()->user->getState('tercerdia');
        $cuartodia = Yii::app()->user->getState('cuartodia');

        $tipocontacto = 5;
        /* Regionales */
        $regionales = Contacto::model()->getRegionales();
        $ar_reg = Yii::app()->user->getState('regional');
        /* Regionales */
        $arrreg = isset($ar_reg) ? $ar_reg : array();

        /* Estados */
        $arr_int = array(1); // Nuevo
        $arr_dup = array(3,45,76,77,78,79,80,81,82); // En Mailing
        $arr_seg = array(5,24,29,31,32,36,37); // En Gestion
        $arr_err = array(4,30,35,57,58,59,60,61,62,63,64,65); // Errado
        $arr_cit = array(8,19); // Cita
        $arr_cot = array(9,10,11,12); //  Cotizacion
        $arr_ven = array(13,14,74,98,99); // Venta
        $arr_dea = array(17,25,28,40,66,67,69,70,71,72,73); // Descartado Antes de Cita
        $arr_ded = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); // Descartado Despues de Cita

        $item = array('ASIGNADO' => 8, 'PRESUPUESTO' => 16, 'COMPETENCIA' => 17, 'ERRADO' => 7);
        $items = array('INTERESADOS' => 1, 'SEGUIMIENTO' => 5, 'VOLVER A LLAMAR' => 24, 'REAGENDADOS' => 19, 'SOLO INFO' => 18, 'NO CONTESTA' => 6, 'DESCARTADO' => 15, 'NO LE INTERESA' => 20, 'DUPLICADO' => 2, 'TRABAJO' => 4, 'PQR' => 3, 'PRESUPUESTO' => 16, 'COMPETENCIA' => 17, 'ERRADO' => 7);

        $arr_medios = array();

        $leads_general = array(
            'LEADS PENDIENTES' => $arr_int, 'EN MAILING' => $arr_dup, 'ERRADOS' => $arr_err, 'DESCARTADOS' => $arr_dea, 
            'EN GESTION' => $arr_seg, 'CITAS' => $arr_cit, 'DESCARTADOS CD' => $arr_ded, 'COTIZACIONES' => $arr_cot, 'VENTAS' => $arr_ven
                );
        
        /* Contadores */
        $nuevos = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_int, $arr_medios);
        $duplicados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_dup, $arr_medios);
        $errados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_err, $arr_medios);
        $descartados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_dea, $arr_medios);
        $seguimiento = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_seg, $arr_medios);
        $cita = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cit, $arr_medios);
        $descartadosdc = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ded, $arr_medios);
        $cotizados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cot, $arr_medios);
        $venta = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ven, $arr_medios);

        $intervalo = Contacto::model()->getFechas($primerdia, $segundodia, 'P1D');

        $categorias = array();
        $arr_chat = array();
        foreach ($intervalo as $i) :
            $categorias[] = $i;
            $arr_chat[] = Contacto::model()->totalCampania($i, $tipocontacto, array(), $arrreg);
        endforeach;

        $totalregistrado = $descartados + $nuevos + $seguimiento + $cita + $cotizados + $venta + $errados + $duplicados + $descartadosdc;

        $grafica = array(array('name' => 'CHAT', 'data' => $arr_chat));

        $arr_tt = array('descartados' => $descartados, 'nuevos' => $nuevos, 'seguimiento' => $seguimiento, 'cita' => $cita, 'cotizados' => $cotizados, 'venta' => $venta, 'duplicados' => $duplicados, 'errados' => $errados, 'descartadosdc' => $descartadosdc);

        if (isset($_POST['comparacion']) && $_POST['comparacion'] == 'true') :
            $display = 'block';
        else :
            $display = 'none';
        endif;

        $this->render('index', array(
            'leads_general' => $leads_general,
            'tipocontacto' => $tipocontacto,
            'regionales' => $regionales,
            'item' => $item,
            'items' => $items,
            'categorias' => $categorias,
            'grafica' => $grafica,
            'primerdia' => $primerdia,
            'segundodia' => $segundodia,
            'tercerdia' => $tercerdia,
            'cuartodia' => $cuartodia,
            'intervalo' => $intervalo,
            'arr_tt' => $arr_tt,
            'arr_reg' => $arrreg,
            'arr_medios' => $arr_medios,
            'arr_ven' => $arr_ven,
            'arr_cot' => $arr_cot,
            'totalregistrado' => $totalregistrado,
                )
        );
    }

    public function actionQryrender() {

        $primerdia = $_POST['fecha_rango1'];
        $segundodia = $_POST['fecha_rango2'];

        $tercerdia = $_POST['fecha_rango3'];
        $cuartodia = $_POST['fecha_rango4'];

        Yii::app()->user->setState('primerdia', $primerdia);
        Yii::app()->user->setState('segundodia', $segundodia);
        Yii::app()->user->setState('tercerdia', $tercerdia);
        Yii::app()->user->setState('cuartodia', $cuartodia);

        $tipocontacto = 5;

        /* Regionales */
        $arrreg = array();
        if ($_POST['regional'] != 'all') :
            $arrreg = array($_POST['regional']);
        endif;
        Yii::app()->user->setState('regional', $arrreg);

        /* Estados */
        $arr_int = array(1); // Nuevo
        $arr_dup = array(3,45,76,77,78,79,80,81,82); // En Mailing
        $arr_seg = array(5,24,29,31,32,36,37); // En Gestion
        $arr_err = array(4,30,35,57,58,59,60,61,62,63,64,65); // Errado
        $arr_cit = array(8,19); // Cita
        $arr_cot = array(9,10,11,12); //  Cotizacion
        $arr_ven = array(13,14,74,98,99); // Venta
        $arr_dea = array(17,25,28,40,66,67,69,70,71,72,73); // Descartado Antes de Cita
        $arr_ded = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); // Descartado Despues de Cita

        $item = array('ASIGNADO' => 8, 'PRESUPUESTO' => 16, 'COMPETENCIA' => 17, 'ERRADO' => 7);
        $items = array('INTERESADOS' => 1, 'SEGUIMIENTO' => 5, 'VOLVER A LLAMAR' => 24, 'REAGENDADOS' => 19, 'SOLO INFO' => 18, 'NO CONTESTA' => 6, 'DESCARTADO' => 15, 'NO LE INTERESA' => 20, 'DUPLICADO' => 2, 'TRABAJO' => 4, 'PQR' => 3, 'PRESUPUESTO' => 16, 'COMPETENCIA' => 17, 'ERRADO' => 7);

        $arr_medios = array();

        $leads_general = array(
            'LEADS PENDIENTES' => $arr_int, 'EN MAILING' => $arr_dup, 'ERRADOS' => $arr_err, 'DESCARTADOS' => $arr_dea, 
            'EN GESTION' => $arr_seg, 'CITAS' => $arr_cit, 'DESCARTADOS CD' => $arr_ded, 'COTIZACIONES' => $arr_cot, 'VENTAS' => $arr_ven
                );

        /* Contadores */
        $nuevos = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_int, $arr_medios);
        $duplicados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_dup, $arr_medios);
        $errados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_err, $arr_medios);
        $descartados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_dea, $arr_medios);
        $seguimiento = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_seg, $arr_medios);
        $cita = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cit, $arr_medios);
        $descartadosdc = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ded, $arr_medios);
        $cotizados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cot, $arr_medios);
        $venta = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ven, $arr_medios);
        
        $nuevos2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_int, $arr_medios);
        $duplicados2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_dup, $arr_medios);
        $errados2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_err, $arr_medios);
        $descartados2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_dea, $arr_medios);
        $seguimiento2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_seg, $arr_medios);
        $cita2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_cit, $arr_medios);
        $descartadosdc2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_ded, $arr_medios);
        $cotizados2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_cot, $arr_medios);
        $venta2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_ven, $arr_medios);

        $intervalo = Contacto::model()->getFechas($primerdia, $segundodia, 'P1D');
        $intervalo2 = Contacto::model()->getFechas($tercerdia, $cuartodia, 'P1D');

        $arr_chat = array();
        $arr_chat2 = array();
        $categorias = array();
        foreach ($intervalo as $i) :
            $categorias[] = $i;
            $arr_chat[] = Contacto::model()->totalCampania($i, $tipocontacto, '', $arrreg);
        endforeach;

        $categorias2 = array();
        foreach ($intervalo2 as $i) :
            $categorias2[] = $i;
            $arr_chat2[] = Contacto::model()->totalCampania($i, $tipocontacto, '', $arrreg);
        endforeach;

        $totalregistrado = $descartados + $nuevos + $seguimiento + $cita + $cotizados + $venta;
        $totalregistrado2 = $descartados2 + $nuevos2 + $seguimiento2 + $cita2 + $cotizados2 + $venta2;

        $grafica = array(array('name' => 'CHAT', 'data' => $arr_chat));
        $grafica2 = array(array('name' => 'CHAT', 'data' => $arr_chat2));

        $arr_tt = array(
            'descartados' => $descartados, 'nuevos' => $nuevos, 'seguimiento' => $seguimiento, 'cita' => $cita, 'cotizados' => $cotizados, 'venta' => $venta, 'duplicados' => $duplicados, 'errados' => $errados, 'descartadosdc' => $descartadosdc,
            'descartados2' => $descartados2, 'nuevos2' => $nuevos2, 'seguimiento2' => $seguimiento2, 'cita2' => $cita2, 'cotizados2' => $cotizados2, 'venta2' => $venta2, 'duplicados2' => $duplicados2, 'errados2' => $errados2, 'descartadosdc2' => $descartadosdc2
                );

        if (isset($_POST['comparacion']) && $_POST['comparacion'] == 'true') :
            $display = 'block';
        else :
            $display = 'none';
        endif;

        $space = 2;
        $space1 = 1;
        $space2 = 1;
        $campania = "";

        $this->renderPartial('contenido', array(
            'leads_general' => $leads_general,
            'tipocontacto' => $tipocontacto,
            'oculto' => $display . ';color:#FF8316',
            'graficaoculta' => $display,
            'comparacion' => $_POST['comparacion'],
            'item' => $item,
            'items' => $items,
            'categorias' => $categorias,
            'categorias2' => $categorias2,
            'grafica' => $grafica,
            'grafica2' => $grafica2,
            'primerdia' => $primerdia,
            'segundodia' => $segundodia,
            'intervalo' => $intervalo,
            'campania' => $campania,
            'space' => $space,
            'space1' => $space1,
            'space2' => $space2,
            'arr_tt' => $arr_tt,
            'arr_reg' => $arrreg,
            'arr_medios' => $arr_medios,
            'arr_ven' => $arr_ven,
            'arr_cot' => $arr_cot,
            'totalregistrado' => $totalregistrado,
            'totalregistrado2' => $totalregistrado2,
                )
        );
    }

     public function actionDetallado() {

        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $arr_reg = Yii::app()->user->getState('regional');

        $texto = explode('-', $_POST['texto']);
        $estado = $texto[0];
        $medio = $texto[1];
        $tipocontacto = $_POST['tipo'];

        $leads = array();

        if ($medio == 'digital') :
            $leads = array('AOL.COM', 'ASK.COM', 'BING.COM', 'GOOGLE', 'PAGINAS AMARILLAS.COM', 'PESCA TELEVIDEO', 'PUBLICIDAD WEB', 'SEARCH.COM', 'TELESENTINEL.COM (ACCESO DIRECTO)', 'VENTAS WEB', 'YAHOO.COM', 'FACEBOOK', 'GOOGLE', 'INSTAGRAM', 'PINTEREST', 'TWITTER', 'DISPLAY', 'PINTEREST', 'DIRECTORIO DIGITAL', 'CHAT', 'CORREO ELECTRONICO');
        elseif ($medio == 'tradicional') :
            $leads = array('ACTIVO CON COMPETENCIA', 'CALCOMANIA Y/O PLACA', 'CENTRO COMERCIAL SANDIEGO', 'CENTRO COMERCIAL UNICENTRO', 'CLIENTE CORFERIAS', 'CLIENTE TELESENTINEL', 'CONOCE FUNCIONARIOS DE TELESENTINEL', 'DIRECTORIO PAG AMARILLAS', 'EXPOCAMACOL', 'FAJITAS', 'HOMECENTER BOGOTA', 'HOMECENTER MEDELLIN', 'LLAMADA 113', 'MAPFRE SEGUROS', 'OPORTUNIDAD TRIANGULO', 'PAUTA RADIO', 'PAUTA EN TV', 'FUE CLIENTE Y QUIERE VOLVER', 'STAN CENTROS COMERCIALES', 'VALLAS', 'VOLANTES', 'MAS DE UN CONTACTO', 'TOMA BARRIO', 'TELEMERCADEO', 'VISITA OFICINA', 'TRABAJO EN FRIO', 'REFERIDO Y/O AMIGO', 'FUE CLIENTE Y QUIERE VOLVER', 'RED 360Âº', 'DIRECTORIO IMPRESO', 'MOTOS', 'VALLAS', 'VOLANTES', 'EVENTO', 'BASE DE DATOS', 'INSTALE YA', 'TODOS A VENDER', 'CENTRO COMERCIAL PLAZA LAS AMERICAS', 'EXPOCONSTRUCCIONES', 'CENTRO COMERCIAL MAYALES', 'FERIA DEL HOGAR', 'FACTURA', 'OTROS', 'CLIENTE VIGENTE', 'CALCOMANIA');
        endif;

        $arr_status = array('', 'LEADS PENDIENTES', 'EN MAILING', 'ERRADOS', 'DESCARTADOS', 'EN GESTION', 'CITAS', 'DESCARTADO DC', 'COTIZACIONES', 'VENTAS');
        $text = $arr_status[$estado];
                
        /* Estados */        
        if ($estado == 1) :
            $arr_estado = array(1); // 1
        endif;
        if ($estado == 2) :
            $arr_estado = array(3,45,76,77,78,79,80,81,82); // 10
        endif;
        if ($estado == 3) :
            $arr_estado = array(4,30,35,57,58,59,60,61,62,63,64,65); // 3
        endif;
        if ($estado == 4) :
            $arr_estado = array(17,25,28,40,66,67,69,70,71,72,73); // 1
        endif;
        if ($estado == 5) :
            $arr_estado = array(5,24,29,31,32,36,37); // 4
        endif;
        if ($estado == 6) :
            $arr_estado = array(8,19); //2
        endif;
        if ($estado == 7) :
            $arr_estado = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); //2
        endif;
        if ($estado == 8) :
            $arr_estado = array(9,10,11,12); //2
        endif;
        if ($estado == 9) :
            $arr_estado = array(13,14,74,98,99); //2
        endif;

        $ttcon = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $arr_estado, $leads);

        $this->renderPartial('detallado', array('texto' => strtoupper($text), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $medio, 'arr_reg' => $arr_reg, 'ttcon' => $ttcon, 'leads' => $leads));
    }

    public function actionDetcomercial() {

        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $arr_reg = Yii::app()->user->getState('regional');

        $estado = $_POST['texto'];
        $tipocontacto = $_POST['tipo'];

        $arr_status = array('', 'LEADS PENDIENTES', 'EN MAILING', 'ERRADOS', 'DESCARTADOS', 'EN GESTION', 'CITAS', 'DESCARTADO DC', 'COTIZACIONES', 'VENTAS');
        $text = $arr_status[$estado];

        /* Estados */        
        if ($estado == 1) :
            $arr_estado = array(1); // 1
        endif;
        if ($estado == 2) :
            $arr_estado = array(3,45,76,77,78,79,80,81,82); // 10
        endif;
        if ($estado == 3) :
            $arr_estado = array(4,30,35,57,58,59,60,61,62,63,64,65); // 3
        endif;
        if ($estado == 4) :
            $arr_estado = array(17,25,28,40,66,67,69,70,71,72,73); // 1
        endif;
        if ($estado == 5) :
            $arr_estado = array(5,24,29,31,32,36,37); // 4
        endif;
        if ($estado == 6) :
            $arr_estado = array(8,19); //2
        endif;
        if ($estado == 7) :
            $arr_estado = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); //2
        endif;
        if ($estado == 8) :
            $arr_estado = array(9,10,11,12); //2
        endif;
        if ($estado == 9) :
            $arr_estado = array(13,14,74,98,99); //2
        endif;

        $ttcon = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $arr_estado);

        $this->renderPartial('detallado_1', array('texto' => strtoupper($text), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $estado, 'arr_reg' => $arr_reg, 'ttcon' => $ttcon));
    }

}
