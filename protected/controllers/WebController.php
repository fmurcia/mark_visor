<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DashboardController
 *
 * @author Gus
 */
class WebController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'qryrender', 'detallado', 'detcomercial'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        /* Fecha */
        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $tercerdia = Yii::app()->user->getState('tercerdia');
        $cuartodia = Yii::app()->user->getState('cuartodia');

        $tipocontacto = 3;
        /* Regionales */
        $regionales = Contacto::model()->getRegionales();
        
        $ar_reg = Yii::app()->user->getState('regional');
        /* Regionales */
        $arrreg = isset($ar_reg) ? $ar_reg : array();
        
        $categorias = array();
        $arr_google = array();
        $arr_facebook = array();
        $arr_seo = array();
        $arr_mailchimp = array();

        $arr_isplay = array();
        $arr_busqueda = array();
        $arr_video = array();
        $arr_facebookp = array();
        $arr_facebooks = array();
        $arr_instagramp = array();  
        $arr_instagrams = array();

        /* Estados */
        $arr_int = array(1); // Nuevo
        $arr_dup = array(3,45,76,77,78,79,80,81,82); // En Mailing
        $arr_seg = array(5,24,29,31,32,36,37); // En Gestion
        $arr_err = array(4,30,35,57,58,59,60,61,62,63,64,65); // Errado
        $arr_cit = array(8,19); // Cita
        $arr_cot = array(9,10,11,12); //  Cotizacion
        $arr_ven = array(13,14,74,98,99); // Venta
        $arr_dea = array(17,25,28,40,66,67,69,70,71,72,73); // Descartado Antes de Cita
        $arr_ded = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); // Descartado Despues de Cita  
        
        /* Campañas */  
        $leads_google = array('Adwords', 'Display', 'video');
        $leads_facebook = array('facebook', 'facebooks', 'instagram', 'instagramp');
        $leads_seo = array('registroseo');
        $leads_mailchimp = array('mailchimp');
        
        $arr_medios = array_merge($leads_google, $leads_facebook, $leads_seo, $leads_mailchimp); //Estados Web

        $leads_general = array('LEADS PENDIENTES' => $arr_int, 'EN MAILING' => $arr_dup, 'ERRADOS' => $arr_err, 'DESCARTADOS' => $arr_dea, 'EN GESTION' => $arr_seg, 'CITAS' => $arr_cit, 'DESCARTADOS CD' => $arr_ded, 'COTIZACIONES' => $arr_cot, 'VENTAS' => $arr_ven);

        /* Contadores */
        $nuevos = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_int, $arr_medios);
        $duplicados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_dup, $arr_medios);
        $errados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_err, $arr_medios);
        $descartados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_dea, $arr_medios);
        $seguimiento = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_seg, $arr_medios);
        $cita = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cit, $arr_medios);
        $descartadosdc = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ded, $arr_medios);
        $cotizados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cot, $arr_medios);
        $venta = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ven, $arr_medios);

        $intervalo = Contacto::model()->getFechas($primerdia, $segundodia, 'P1D');

        foreach ($intervalo as $i) :
            $categorias[] = $i;

            $contador_google = 0;
            $contador_facebook = 0;
            $contador_registroseo = 0;
            $contador_mailchimp = 0;
            
            //($dia, $tipo, $medio, $ciudad)

            foreach ($leads_google as $lg) :
                $contador_google += Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
            endforeach;

            foreach ($leads_facebook as $lf) :
                $contador_facebook += Contacto::model()->totalCampania($i, $tipocontacto, array($lf), $arrreg);
            endforeach;

            foreach ($leads_seo as $lr) :
                $contador_registroseo += Contacto::model()->totalCampania($i, $tipocontacto, array($lr), $arrreg);
            endforeach;

            foreach ($leads_mailchimp as $lm) :
                $contador_mailchimp += Contacto::model()->totalCampania($i, $tipocontacto, array($lm), $arrreg);
            endforeach;

            $arr_google[] = $contador_google;
            $arr_facebook[] = $contador_facebook;
            $arr_seo[] = $contador_registroseo;
            $arr_mailchimp[] = $contador_mailchimp;

            $arr_video[] = Contacto::model()->totalCampania($i, $tipocontacto, array($leads_google[2]), $arrreg);
            $arr_isplay[] = Contacto::model()->totalCampania($i, $tipocontacto, array($leads_google[1]), $arrreg);
            $arr_busqueda[] = Contacto::model()->totalCampania($i, $tipocontacto, array($leads_google[0]), $arrreg);
            
            $arr_facebookp[] = Contacto::model()->totalCampania($i, $tipocontacto, array($leads_facebook[0]), $arrreg);
            $arr_facebooks[] = Contacto::model()->totalCampania($i, $tipocontacto, array($leads_facebook[1]), $arrreg);
            $arr_instagramp[] = Contacto::model()->totalCampania($i, $tipocontacto, array($leads_facebook[2]), $arrreg);
            $arr_instagrams[] = Contacto::model()->totalCampania($i, $tipocontacto, array($leads_facebook[3]), $arrreg);

        endforeach;
        
        $totalregistrado = $descartados + $nuevos + $seguimiento + $cita + $cotizados + $venta + $errados + $duplicados + $descartadosdc;

        $totalgoogle = (array_sum($arr_isplay) + array_sum($arr_busqueda) + array_sum($arr_video));
        $totalfacebook = (array_sum($arr_facebookp) + array_sum($arr_facebooks) + array_sum($arr_instagramp) + array_sum($arr_instagrams));

        $grafica = array(array('name' => 'GOOGLE', 'data' => $arr_google), array('name' => 'FACEBOOK', 'data' => $arr_facebook), array('name' => 'SEO', 'data' => $arr_seo), array('name' => 'MAILCHIMP', 'data' => $arr_mailchimp));

        $arr_tt = array('descartados' => $descartados, 'nuevos' => $nuevos, 'seguimiento' => $seguimiento, 'cita' => $cita, 'cotizados' => $cotizados, 'venta' => $venta, 'duplicados' => $duplicados, 'errados' => $errados, 'descartadosdc' => $descartadosdc);

        $this->render('index', array(
            'leads_general' => $leads_general,
            'tipocontacto' => $tipocontacto,
            'regionales' => $regionales,
            'oculto' => 'block',
            'categorias' => $categorias,
            'grafica' => $grafica,
            'primerdia' => $primerdia,
            'segundodia' => $segundodia,
            'tercerdia' => $tercerdia,
            'cuartodia' => $cuartodia,
            'intervalo' => $intervalo,
            'arr_tt' => $arr_tt,
            'arr_reg' => $arrreg,
            'arr_medios' => $arr_medios,
            'arr_ven' => $arr_ven,
            'arr_cot' => $arr_cot,
            'arr_cit' => $arr_cit,
            'arr_seg' => $arr_seg,
            'arr_int' => $arr_int,
            'arr_des' => $arr_dea,
            'arr_google' => $arr_google,
            'arr_seo' => $arr_seo,
            'arr_facebook' => $arr_facebook,
            'arr_facebookp' => $arr_facebookp,
            'arr_mailchimp' => $arr_mailchimp,
            'arr_facebooks' => $arr_facebooks,
            'arr_instagramp' => $arr_instagramp,
            'arr_instagrams' => $arr_instagrams,
            'arr_busqueda' => $arr_busqueda,
            'arr_video' => $arr_video,
            'arr_isplay' => $arr_isplay,
            'totalgoogle' => $totalgoogle,
            'totalfacebook' => $totalfacebook,
            'totalregistrado' => $totalregistrado,
                )
        );
    }

    public function actionQryrender() {

        $primerdia = $_POST['fecha_rango1'];
        $segundodia = $_POST['fecha_rango2'];

        $tercerdia = $_POST['fecha_rango3'];
        $cuartodia = $_POST['fecha_rango4'];

        Yii::app()->user->setState('primerdia', $primerdia);
        Yii::app()->user->setState('segundodia', $segundodia);
        Yii::app()->user->setState('tercerdia', $tercerdia);
        Yii::app()->user->setState('cuartodia', $cuartodia);

        $tipocontacto = 3;
        /* Regionales */
        $arrreg = array();
        if ($_POST['regional'] != 'all') :
            $arrreg = array($_POST['regional']);
        endif;
        Yii::app()->user->setState('regional', $arrreg);

        /* Estados */
        $arr_int = array(1); // Nuevo
        $arr_dup = array(3,45,76,77,78,79,80,81,82); // En Mailing
        $arr_seg = array(5,24,29,31,32,36,37); // En Gestion
        $arr_err = array(4,30,35,57,58,59,60,61,62,63,64,65); // Errado
        $arr_cit = array(8,19); // Cita
        $arr_cot = array(9,10,11,12); //  Cotizacion
        $arr_ven = array(13,14,74,98,99); // Venta
        $arr_dea = array(17,25,28,40,66,67,69,70,71,72,73); // Descartado Antes de Cita
        $arr_ded = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); // Descartado Despues de Cita 

        /* Campañas */
        $leads_google = array('Adwords', 'Display', 'video', 'Google', '(direct)', '');
        $leads_facebook = array('facebook', 'facebooks', 'instagram', 'instagramp');
        $leads_seo = array('registroseo');
        $leads_mailchimp = array('mailchimp');

        $leads_general = array(
            'LEADS PENDIENTES' => $arr_int, 'EN MAILING' => $arr_dup, 'ERRADOS' => $arr_err, 'DESCARTADOS' => $arr_dea, 
            'EN GESTION' => $arr_seg, 'CITAS' => $arr_cit, 'DESCARTADOS CD' => $arr_ded, 'COTIZACIONES' => $arr_cot, 'VENTAS' => $arr_ven
                );

        $asignado = array();
        $direccion = array();
        $sinasignar = array();

        $space = 20;
        $space1 = 2;
        $space2 = 1;
        /* Campañas y Subcampañas */
        if ($_POST['campania'] == 'Facebook') :
            $space = 10;
            $space1 = 9;
            $space2 = 1;
            if ($_POST['subcampania'] == 'all') :
                $leads_facebook = array('facebook', 'facebooks', 'instagram', 'instagramp');
            else :
                $leads_facebook = array($_POST['subcampania']);
            endif;
            $leads_google = array();
            $leads_seo = array();
            $leads_mailchimp = array();
        endif;
        if ($_POST['campania'] == 'Adwords') :
            $space = 8;
            $space1 = 7;
            $space2 = 1;
            if ($_POST['subcampania'] == 'all') :
                $leads_google = array('Adwords', 'Display', 'video');
            else :
                $leads_google = array($_POST['subcampania']);
            endif;
            $leads_facebook = array();
            $leads_seo = array();
            $leads_mailchimp = array();
        endif;
        if ($_POST['campania'] == 'registroseo') :
            $space = 4;
            $space1 = 1;
            $space2 = 1;
            $leads_google = array();
            $leads_facebook = array();
            $leads_seo = array('registroseo');
            $leads_mailchimp = array();
        endif;
        if ($_POST['campania'] == 'mailchimp') :
            $space = 4;
            $space1 = 1;
            $space2 = 1;
            $leads_google = array();
            $leads_facebook = array();
            $leads_seo = array();
            $leads_mailchimp = array('mailchimp');
        endif;

        if ($_POST['campania'] == 'all') :
            $leads_google = array('Adwords', 'Display', 'video', 'Google', '(direct)', '');
            $leads_facebook = array('facebook', 'facebooks', 'instagram', 'instagramp');
            $leads_seo = array('registroseo');
            $leads_mailchimp = array('mailchimp');
        endif;

        $arr_medios = array_merge($leads_google, $leads_facebook, $leads_seo, $leads_mailchimp);

        /* Contadores */
        $nuevos = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_int, $arr_medios);
        $duplicados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_dup, $arr_medios);
        $errados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_err, $arr_medios);
        $descartados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_dea, $arr_medios);
        $seguimiento = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_seg, $arr_medios);
        $cita = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cit, $arr_medios);
        $descartadosdc = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ded, $arr_medios);
        $cotizados = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_cot, $arr_medios);
        $venta = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arrreg, $arr_ven, $arr_medios);
        
        $nuevos2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_int, $arr_medios);
        $duplicados2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_dup, $arr_medios);
        $errados2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_err, $arr_medios);
        $descartados2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_dea, $arr_medios);
        $seguimiento2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_seg, $arr_medios);
        $cita2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_cit, $arr_medios);
        $descartadosdc2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_ded, $arr_medios);
        $cotizados2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_cot, $arr_medios);
        $venta2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arrreg, $arr_ven, $arr_medios);

        
        $intervalo = Contacto::model()->getFechas($primerdia, $segundodia, 'P1D');
        $intervalo2 = Contacto::model()->getFechas($tercerdia, $cuartodia, 'P1D');

        $categorias = array();
        $categorias2 = array();

        $arr_google = array();
        $arr_facebook = array();
        $arr_seo = array();
        $arr_mailchimp = array();

        $arr_google2 = array();
        $arr_facebook2 = array();
        $arr_seo2 = array();
        $arr_mailchimp2 = array();

        $arr_isplay = array();
        $arr_busqueda = array();
        $arr_video = array();
        $arr_facebookp = array();
        $arr_facebooks = array();
        $arr_instagramp = array();
        $arr_instagrams = array();

        $arr_isplay2 = array();
        $arr_busqueda2 = array();
        $arr_video2 = array();
        $arr_facebookp2 = array();
        $arr_facebooks2 = array();
        $arr_instagramp2 = array();
        $arr_instagrams2 = array();

        foreach ($intervalo as $i) :
            $categorias[] = $i;

            $contador_google = 0;
            $contador_facebook = 0;
            $contador_registroseo = 0;
            $contador_mailchimp = 0;

            foreach ($leads_google as $lg) :
                $contador_google += Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
            endforeach;

            foreach ($leads_facebook as $lf) :
                $contador_facebook += Contacto::model()->totalCampania($i, $tipocontacto, array($lf), $arrreg);
            endforeach;

            foreach ($leads_seo as $lr) :
                $contador_registroseo += Contacto::model()->totalCampania($i, $tipocontacto, array($lr), $arrreg);
            endforeach;

            foreach ($leads_mailchimp as $lm) :
                $contador_mailchimp += Contacto::model()->totalCampania($i, $tipocontacto, array($lm), $arrreg);
            endforeach;

            $arr_google[] = $contador_google;
            $arr_facebook[] = $contador_facebook;
            $arr_seo[] = $contador_registroseo;
            $arr_mailchimp[] = $contador_mailchimp;

            if (sizeof($leads_google) > 0) :
                foreach ($leads_google as $lg) :
                    if ($lg == 'Adwords') :
                        $arr_busqueda[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'video') :
                        $arr_video[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'Display') :
                        $arr_isplay[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'Google') :
                        $arr_busqueda[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == '(direct)') :
                        $arr_busqueda[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == '') :
                        $arr_busqueda[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    endif;
                endforeach;
            endif;

            if (sizeof($leads_facebook) > 0) :
                foreach ($leads_facebook as $lg) :
                    if ($lg == 'facebook') :
                        $arr_facebookp[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'facebooks') :
                        $arr_facebooks[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'instagram') :
                        $arr_instagramp[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'instagramp') :
                        $arr_instagrams[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    endif;
                endforeach;
            endif;
        endforeach;

        foreach ($intervalo2 as $i) :
            $categorias2[] = $i;

            $contador_google = 0;
            $contador_facebook = 0;
            $contador_registroseo = 0;
            $contador_mailchimp = 0;

            foreach ($leads_google as $lg) :
                $contador_google += Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
            endforeach;

            foreach ($leads_facebook as $lf) :
                $contador_facebook += Contacto::model()->totalCampania($i, $tipocontacto, array($lf), $arrreg);
            endforeach;

            foreach ($leads_seo as $lr) :
                $contador_registroseo += Contacto::model()->totalCampania($i, $tipocontacto, array($lr), $arrreg);
            endforeach;

            foreach ($leads_mailchimp as $lm) :
                $contador_mailchimp += Contacto::model()->totalCampania($i, $tipocontacto, array($lm), $arrreg);
            endforeach;

            $arr_google2[] = $contador_google;
            $arr_facebook2[] = $contador_facebook;
            $arr_seo2[] = $contador_registroseo;
            $arr_mailchimp2[] = $contador_mailchimp;

            if (sizeof($leads_google) > 0) :
                foreach ($leads_google as $lg) :
                    if ($lg == 'Adwords') :
                        $arr_busqueda2[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'video') :
                        $arr_video2[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'Display') :
                        $arr_isplay2[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    endif;
                endforeach;
            endif;

            if (sizeof($leads_facebook) > 0) :
                foreach ($leads_facebook as $lg) :
                    if ($lg == 'facebook') :
                        $arr_facebookp2[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'facebooks') :
                        $arr_facebooks2[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'instagram') :
                        $arr_instagramp2[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    elseif ($lg == 'instagramp') :
                        $arr_instagrams2[] = Contacto::model()->totalCampania($i, $tipocontacto, array($lg), $arrreg);
                    endif;
                endforeach;
            endif;
        endforeach;

        $totalregistrado = $descartados + $nuevos + $seguimiento + $cita + $cotizados + $venta + $errados + $duplicados + $descartadosdc;
        $totalregistrado2 = $descartados2 + $nuevos2 + $seguimiento2 + $cita2 + $cotizados2 + $venta2 + $errados2 + $duplicados2 + $descartadosdc2;

        $totalgoogle = (array_sum($arr_isplay) + array_sum($arr_busqueda) + array_sum($arr_video));
        $totalgoogle2 = (array_sum($arr_isplay2) + array_sum($arr_busqueda2) + array_sum($arr_video2));
        $totalfacebook = (array_sum($arr_facebookp) + array_sum($arr_facebooks) + array_sum($arr_instagramp) + array_sum($arr_instagrams));
        $totalfacebook2 = (array_sum($arr_facebookp2) + array_sum($arr_facebooks2) + array_sum($arr_instagrams2) + array_sum($arr_instagrams2));

        /* Campañas y Subcampañas */
        if ($_POST['campania'] == 'Facebook') :
            if ($_POST['subcampania'] == 'all') :
                $grafica = array(array('name' => 'FACEBOOK', 'data' => $arr_facebook));
                $grafica2 = array(array('name' => 'FACEBOOK', 'data' => $arr_facebook2));
            else :
                $grafica = array(array('name' => strtoupper($_POST['subcampania']), 'data' => $arr_facebook));
                $grafica2 = array(array('name' => strtoupper($_POST['subcampania']), 'data' => $arr_facebook2));
            endif;
        endif;

        if ($_POST['campania'] == 'Adwords') :
            if ($_POST['subcampania'] == 'all') :
                $grafica = array(array('name' => 'GOOGLE', 'data' => $arr_google));
                $grafica2 = array(array('name' => 'GOOGLE', 'data' => $arr_google2));
            else :
                $grafica = array(array('name' => strtoupper($_POST['subcampania']), 'data' => $arr_google));
                $grafica2 = array(array('name' => strtoupper($_POST['subcampania']), 'data' => $arr_google2));
            endif;
        endif;

        if ($_POST['campania'] == 'registroseo') :
            $grafica = array(array('name' => 'SEO', 'data' => $arr_seo));
            $grafica2 = array(array('name' => 'SEO', 'data' => $arr_seo2));
        endif;

        if ($_POST['campania'] == 'mailchimp') :
            $grafica = array(array('name' => 'MAILCHIMP', 'data' => $arr_mailchimp));
            $grafica2 = array(array('name' => 'MAILCHIMP', 'data' => $arr_mailchimp2));
        endif;

        if ($_POST['campania'] == 'all') :
            $grafica = array(array('name' => 'GOOGLE', 'data' => $arr_google), array('name' => 'FACEBOOK', 'data' => $arr_facebook), array('name' => 'SEO', 'data' => $arr_seo), array('name' => 'MAILCHIMP', 'data' => $arr_mailchimp));
            $grafica2 = array(array('name' => 'GOOGLE', 'data' => $arr_google2), array('name' => 'FACEBOOK', 'data' => $arr_facebook2), array('name' => 'SEO', 'data' => $arr_seo2), array('name' => 'MAILCHIMP', 'data' => $arr_mailchimp2));
        endif;

        $arr_tt = array('descartados' => $descartados, 'nuevos' => $nuevos, 'seguimiento' => $seguimiento, 'cita' => $cita, 'cotizados' => $cotizados, 'venta' => $venta, 'duplicados' => $duplicados, 'errados' => $errados, 'descartadosdc' => $descartadosdc,
                        'descartados2' => $descartados2, 'nuevos2' => $nuevos2, 'seguimiento2' => $seguimiento2, 'cita2' => $cita2, 'cotizados2' => $cotizados2, 'venta2' => $venta2, 'duplicados2' => $duplicados2, 'errados2' => $errados2, 'descartadosdc2' => $descartadosdc2);

        if ($_POST['comparacion'] == 'true') :
            $display = 'block';
        else :
            $display = 'none';
        endif;
        
        $this->renderPartial('contenido', array(
            'leads_general' => $leads_general,
            'tipocontacto' => $tipocontacto,
            'oculto' => $display . ';color:#FF8316',
            'graficaoculta' => $display,
            'comparacion' => $_POST['comparacion'],
            'asignado' => $asignado,
            'direccion' => $direccion,
            'sinasignar' => $sinasignar,
            'categorias' => $categorias,
            'categorias2' => $categorias2,
            'grafica' => $grafica,
            'grafica2' => $grafica2,
            'campania' => $_POST['campania'],
            'primerdia' => $primerdia,
            'segundodia' => $segundodia,
            'tercerdia' => $tercerdia,
            'cuartodia' => $cuartodia,
            'space' => $space,
            'space1' => $space1,
            'space2' => $space2,
            'intervalo' => $intervalo,
            'arr_tt' => $arr_tt,
            'arr_reg' => $arrreg,
            'arr_medios' => $arr_medios,
            'arr_ven' => $arr_ven,
            'arr_cot' => $arr_cot,
            'arr_seo' => $arr_seo,
            'arr_seo2' => $arr_seo2,
            'arr_google' => $arr_google,
            'arr_google2' => $arr_google2,
            'arr_facebook' => $arr_facebook,
            'arr_facebook2' => $arr_facebook2,
            'arr_facebookp' => $arr_facebookp,
            'arr_facebookp2' => $arr_facebookp2,
            'arr_mailchimp' => $arr_mailchimp,
            'arr_mailchimp2' => $arr_mailchimp2,
            'arr_facebooks' => $arr_facebooks,
            'arr_facebooks2' => $arr_facebooks2,
            'arr_instagramp' => $arr_instagramp,
            'arr_instagramp2' => $arr_instagramp2,
            'arr_instagrams' => $arr_instagrams,
            'arr_instagrams2' => $arr_instagrams2,
            'arr_busqueda' => $arr_busqueda,
            'arr_busqueda2' => $arr_busqueda2,
            'arr_video' => $arr_video,
            'arr_video2' => $arr_video2,
            'arr_isplay' => $arr_isplay,
            'arr_isplay2' => $arr_isplay2,
            'totalgoogle' => $totalgoogle,
            'totalgoogle2' => $totalgoogle2,
            'totalfacebook' => $totalfacebook,
            'totalfacebook2' => $totalfacebook2,
            'totalregistrado' => $totalregistrado,
            'totalregistrado2' => $totalregistrado2,
                )
        );
    }

    public function actionDetallado() {

        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $arr_reg = Yii::app()->user->getState('regional');

        $texto = explode('-', $_POST['texto']);
        $estado = $texto[0];
        $medio = $texto[1];
        $tipocontacto = $_POST['tipo'];

        $arr_status = array('', 'LEADS PENDIENTES', 'EN MAILING', 'ERRADOS', 'DESCARTADOS', 'EN GESTION', 'CITAS', 'DESCARTADO DC', 'COTIZACIONES', 'VENTAS');
        
        $text = $arr_status[$estado];   
        
        /* Estados */
        $arr_int = array(1); // Nuevo
        $arr_dup = array(3,45,76,77,78,79,80,81,82); // En Mailing
        $arr_seg = array(5,24,29,31,32,36,37); // En Gestion
        $arr_err = array(4,30,35,57,58,59,60,61,62,63,64,65); // Errado
        $arr_cit = array(8,19); // Cita
        $arr_cot = array(9,10,11,12); //  Cotizacion
        $arr_ven = array(13,14,74,98,99); // Venta
        $arr_dea = array(17,25,28,40,66,67,69,70,71,72,73); // Descartado Antes de Cita
        $arr_ded = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); // Descartado Despues de Cita 
                
        /* Estados */        
        if ($estado == 1) :
            $arr_estado = array(1); // 1
        endif;
        if ($estado == 2) :
            $arr_estado = array(3,45,76,77,78,79,80,81,82); // 10
        endif;
        if ($estado == 3) :
            $arr_estado = array(4,30,35,57,58,59,60,61,62,63,64,65); // 3
        endif;
        if ($estado == 4) :
            $arr_estado = array(17,25,28,40,66,67,69,70,71,72,73); // 1
        endif;
        if ($estado == 5) :
            $arr_estado = array(24,29,31,32,36,37); // 4
        endif;
        if ($estado == 6) :
            $arr_estado = array(8,19); //2
        endif;
        if ($estado == 7) :
            $arr_estado = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); //2
        endif;
        if ($estado == 8) :
            $arr_estado = array(9,10,11,12); //2
        endif;
        if ($estado == 9) :
            $arr_estado = array(13,14,74,98,99); //2
        endif;

        $ttcon = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $arr_estado, array($medio));

        $this->renderPartial('detallado', array('texto' => strtoupper($text), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $medio, 'arr_reg' => $arr_reg, 'ttcon' => $ttcon));
    }
    
    public function actionDetcomercial() {

        $primerdia = Yii::app()->user->getState('primerdia');
        $segundodia = Yii::app()->user->getState('segundodia');
        $arr_reg = Yii::app()->user->getState('regional');

        $estado =  $_POST['texto'];
        $tipocontacto = $_POST['tipo'];
        $arr_estado = array();

        $arr_status = array('', 'LEADS PENDIENTES', 'EN MAILING', 'ERRADOS', 'DESCARTADOS', 'EN GESTION', 'CITAS', 'DESCARTADO DC', 'COTIZACIONES', 'VENTAS');
  
        $texto = $arr_status[$estado];
        
       /* Estados */        
        if ($estado == 1) :
            $arr_estado = array(1); // 1
        endif;
        if ($estado == 2) :
            $arr_estado = array(3,45,76,77,78,79,80,81,82); // 10
        endif;
        if ($estado == 3) :
            $arr_estado = array(4,30,35,57,58,59,60,61,62,63,64,65); // 3
        endif;
        if ($estado == 4) :
            $arr_estado = array(17,25,28,40,66,67,69,70,71,72,73); // 1
        endif;
        if ($estado == 5) :
            $arr_estado = array(24,29,31,32,36,37); // 4
        endif;
        if ($estado == 6) :
            $arr_estado = array(8,19); //2
        endif;
        if ($estado == 7) :
            $arr_estado = array(41,42,43,44,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97); //2
        endif;
        if ($estado == 8) :
            $arr_estado = array(9,10,11,12); //2
        endif;
        if ($estado == 9) :
            $arr_estado = array(13,14,74,98,99); //2
        endif;

        $ttcon = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $arr_estado);

        $this->renderPartial('detallado_1', array('texto' => $texto, 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $estado, 'arr_reg' => $arr_reg, 'ttcon' => $ttcon));
    }
}
