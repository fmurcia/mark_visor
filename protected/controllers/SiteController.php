<?php

/**
 * Controlador de Eventos del Sitio Principal
 * Clase para el Manejo de Controladores Iniciales
 * 
 * @author Gustavo Carvajal <gcarvajal@telesentinel.com>
 * @version 1.0
 * 
 * @method Void MenuSuperior() Carga el Menu Superior de la Vista Inicial
 * @method Void Alertas() Carga las Alertas de la Aplicacion
 * @method Void Pausaactiva() Carga el Menu con la Pausa Seleccionada
 * @method Void Pausas() Carga el Menu de Pausas
 * @method Void Tablacerrados() Carga las Solicitudes Cerradas
 * @method Void Tablacontenido() Tabla de Contenidos
 * @method Void Tablamantenimiento() Carga las Solicitudes en Mantenimiento
 * @method Void Tablapruebas() Tabla de Pruebas Generadas
 * @method Void Tablasitio() Carga los Tecnicos en Sitio
 * @method Void Tablavistas() Carga las Solicitudes Vistas
 */
class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array("index", 'logout', 'login'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('shownoty', 'divcit', 'notificaciones', 'agendados', 'detallado', 'span', 'contadores'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->actionLogin();
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login_form') :
            echo CActiveForm::validate($model);
            Yii::app()->end();
        endif;

        // collect user input data
        if (isset($_POST['LoginForm'])) :
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) :
                $this->redirect(Yii::app()->createUrl('web/'));
            else :
                $this->layout = 'login';
                $this->render('login', array('model' => $model));
            endif;
        else :
            $this->layout = 'login';
            $this->render('login', array('model' => $model));
        endif;
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionSpan() {
        Yii::app()->user->setState($_POST['range'], $_POST['span']);
    }
    
    public function actionContadores(){
        $this->renderPartial('contadores');
    }
}
