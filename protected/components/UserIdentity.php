<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $usuario = Asesor::model()->findByAttributes(
                array('Email' => $this->username . "@telesentinel.com")
        );
        if ($usuario != null) :
            if ($usuario->Password !== MD5($this->password)) :
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            elseif ($usuario->Estado > 1) :
                $this->errorMessage = $usuario->estado->descripcion;
                $this->errorCode = 1;
            else :
                
                $permisos = AccionAsesor::model()->getPermisos($usuario->ID);
            
                Yii::app()->user->setState('id_usuario', $usuario->ID);
                Yii::app()->user->setState('nombre_usuario', $usuario->Nombre);
                Yii::app()->user->setState('id_cargo', $usuario->Nivel);
                Yii::app()->user->setState('id_estado', $usuario->Estado);
                Yii::app()->user->setState('namecargo', $usuario->nivel->descripcion);
                Yii::app()->user->setState('usr_permisos', $permisos);
                
                Yii::app()->user->setState('primerdia', Contacto::model()->_data_first_month_day());
                Yii::app()->user->setState('segundodia', Contacto::model()->_data_last_month_day());
                Yii::app()->user->setState('tercerdia', Contacto::model()->_data_first_month_day());
                Yii::app()->user->setState('cuartodia', Contacto::model()->_data_last_month_day());
                
                Yii::app()->user->setState('regional', array());
                                
                $this->errorCode = self::ERROR_NONE;
            endif;
        else :
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        endif;
        return !$this->errorCode;
    }
}