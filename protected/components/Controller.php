<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    public $buttons = null;

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    public $footer = "";

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     * @var arreglo para cargar las contantes de los archivos JS para el uso en las vistas
     * y su carga dinamica desde la carpeta de este    
     */
    private $jsfiles = array(
        CClientScript::POS_BEGIN => "/begin.js",
        CClientScript::POS_HEAD => "/head.js",
        CClientScript::POS_LOAD => "/load.js",
        CClientScript::POS_READY => "/ready.js",
        CClientScript::POS_END => "/end.js",
    );
    public $assets = "";
    public $assetsFull = "";
    public $usuario;
    public $nomusuario;
    public $agencia;
    public $cargo;
    public $estado;
    public $nomcargo;
    public $titulo = "";

    public function init() {
        $this->usuario = Yii::app()->user->getState("id_usuario");
        $this->nomusuario = Yii::app()->user->getState("nombre_usuario");
        $this->agencia = Yii::app()->user->getState("id_agencia");
        $this->cargo = Yii::app()->user->getState("id_cargo");
        $this->estado = Yii::app()->user->getState("id_estado");
        $this->nomcargo = Yii::app()->user->getState("namecargo");
        parent::init();
    }

    public function __construct($id, $module = null) {
        $this->usuario = Yii::app()->user->getState("id_usuario");
        $this->nomusuario = Yii::app()->user->getState("nombre_usuario");
        $this->agencia = Yii::app()->user->getState("id_agencia");
        $this->cargo = Yii::app()->user->getState("id_cargo");
        $this->estado = Yii::app()->user->getState("id_estado");
        $this->nomcargo = Yii::app()->user->getState("namecargo");
        parent::__construct($id, $module);
    }

    private function getNombre($llave = "", $general = false) {
        if ($llave != "") {
            $result = debug_backtrace();
            if ($result != null) {
                $linea = $result[2]["file"];
            } else {
                $linea = "";
            }
            if (strpos($linea, "views") > 0) {
                $pref = "vis";
            } else if (strpos($linea, "controllers") > 0) {
                $pref = "ctr";
            } else if (strpos($linea, "models") > 0) {
                $pref = "mod";
            } else {
                $pref = "otr";
            }

            if ($general == true) {
                return "global" . "." . $llave . ".";
            } else {
                return $this->id . "." . $llave . ".";
            }
        } else {
            throw new CHttpException(500, "Error en los parametros");
        }
    }

    private function validarTarea($task = "", $general = false) {
        $nombre = $this->getNombre($task, $general);
        $hash = md5($nombre);
        $permiso = Acciones::model()->findByAttributes(array(
            "Hash" => $hash
        ));
        if ($permiso == null) {
            $permiso = new Acciones();
            $permiso->Hash = $hash;
            $permiso->Descripcion = $nombre;
            $permiso->Tipo = "Tarea";
            $permiso->Medio = "Web";
            $permiso->save();
        }
        return $permiso->ID;
    }

    private function validarUsuario($id = 0) {
        if ($id > 0) {
            $permisos = Yii::app()->user->getState("usr_permisos");
            if (!empty($permisos)) {
                if (in_array($id, $permisos) > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public function entrar($acceso = "", $general = false, $usuario = 0) {
        if ($acceso != "") {
            if ($usuario == 0) {
                $usuario = Yii::app()->user->getState('id_usuario');
            }
            $idprm = $this->validarTarea($acceso, $general);
            return $this->validarUsuario($idprm, $usuario);
        }
        return false;
    }

    //////////////////////////ADMINISTRACION DE RECURSOS JS PARA VISTAS////////////////////////////

    protected function beforeRender($view) {
        $this->renderJs();
        return true;
        parent::beforeRender($view);
    }

    private function renderJs() {
        $this->assets = $this->publishFiles("js", false);
        $this->assetsFull = $this->publishFiles("js", true);
        $scan = array_diff(scandir($this->assetsFull), array('..', '.'));
        if (count($scan) > 0) {
            foreach ($scan as $js) {
                $pos = array_search("/" . $js, $this->jsfiles);
                if ($pos !== false) {
                    $tmppath = realpath($this->assetsFull . "/" . $js);
                    if ($tmppath !== false) {
                        if ($pos == CClientScript::POS_LOAD || $pos == CClientScript::POS_READY) {
                            $key = substr(md5($this->assets . "/" . $js), 20);
                            if (!Yii::app()->clientScript->isScriptRegistered($key, $pos)) {
                                $tmppath = realpath($this->assetsFull . "/" . $js);
                                $this->renderJsFile($this->assetsFull . "/" . $js);
                                Yii::app()->clientScript->registerScript($key, $this->renderJsFile($this->assetsFull . "/" . $js), $pos);
                            }
                        } else {
                            if (!Yii::app()->clientScript->isScriptFileRegistered($this->assetsFull . "/" . $js, $pos)) {
                                $this->renderJsFile($this->assetsFull . "/" . $js);
                                Yii::app()->clientScript->registerScriptFile($this->assets . "/" . $js, $pos);
                            }
                        }
                    }
                } else {
                    if (strpos($js, ".css") > 0) {
                        Yii::app()->clientScript->registerCssFile($this->assets . "/" . $js);
                    } elseif (strpos($js, ".js") > 0) {
                        Yii::app()->clientScript->registerScriptFile($this->assets . "/" . $js, CClientScript::POS_HEAD);
                    }
                }
            }
        }
    }

    private function renderJsFile($file = "") {
        if ($file != "") {
            $path = realpath($file);
            if ($path !== false) {
                $content = file_get_contents($path);
                ob_start();
                eval('?> ' . $content . ' <?php ');
                $output = ob_get_clean();
                file_put_contents($path, $output);
                return $output;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public function publishFiles($file = "", $return = false) {
        if ($file != "") {
            $controller = $this->getId();
            $realfile = "application.views.{$controller}." . $file;
            $realfile = YiiBase::getPathOfAlias($realfile);
            $path = realpath($realfile . "/");
            if ($path === false) {
                mkdir($realfile);
                $path = realpath($realfile);
            }
            Yii::app()->assetManager->forceCopy = true;
            $assetPath = Yii::app()->assetManager->publish($path);
            if ($return == true) {
                return Yii::app()->assetManager->getPublishedPath($path);
            } else {
                return $assetPath;
            }
        } else {
            throw new CHttpException(500, "La ruta del asset no puede quedar vacia");
        }
    }

    public function menu() {
        $usuario = Yii::app()->user->getState('id_usuario');
        $sitios = Sitios::model()->with(
                        array(
                            'sitiosAsesors' => array(
                                'condition' => 'sitiosAsesors.ID_Asesor = "' . $usuario . '" AND sitiosAsesors.Estado = 1',
                            )
                        )
                )->findAll('Visible = 1');

        $retorno = Sitios::model()->findByPk(6);

        if (sizeof($sitios) > 1) :
            $item = array('label' => 'Retorno', 'url' => $retorno->Controlador . "?id=" . Yii::app()->user->getState('id_usuario')); /* Yii::app()->createUrl($retorno->Controlador, array('id' => $usuario))); */
        else :
            $item = array('label' => 'Salir', 'url' => Yii::app()->createUrl('site/logout'));
        endif;

        return $item;
    }

    public function cierre() {
        $periodo = Cierres::model()->findAll();
        $cierre = array();
        foreach ($periodo as $p) :
            if (strtotime($p->Fecha_inicial) <= strtotime(date('Y-m-d')) && strtotime(date('Y-m-d')) <= strtotime($p->Fecha_final)) :
                $cierre = $p->attributes;
            endif;
        endforeach;
        return $cierre;
    }

    public function estadisticas() {
        $this->renderPartial('/site/estadisticas');
    }

    public function notificaciones() {
        $this->renderPartial('/site/notificaciones');
    }

    public function frases() {
        $frases = Frases::model()->findByAttributes(array('ID_Usuario' => Yii::app()->user->getState('id_usuario'), 'Estado' => 1));
        $texto = "";
        if ($frases != NULL) :
            $texto = $frases->Descripcion;
        else :
            $tt = Frases::model()->findAll('ID_Usuario = 1 AND Estado = 1');
            if ($tt != NULL) :
                foreach ($tt as $t) :
                    $texto = $t->Descripcion;
                endforeach;
            else :
                $texto = 'Sin Mensaje';
            endif;
        endif;

        return $texto;
    }

    /**
     *
     */
    public function contadorEstadoProceso($tipo) {

        $query = '';
        if ($tipo != 0) :
            $query = 'Estado_proceso = ' . $tipo . ' AND ';
        endif;

        $criteria = new CDbCriteria();
        //$criteria->addBetweenCondition('Fecha_creacion', $cierre['Fecha_inicial'], $cierre['Fecha_final']);
        $criteria->addCondition($query . ' Telemercaderista = ' . Yii::app()->user->getState('id_usuario'));
        return Contacto::model()->count($criteria);
    }

    /**
     * Retorna el Ultimo dia del Mes
     * @return type
     */
    function Ultimodia($fecha = '') {

        if (!empty($fecha)) :
            $time = explode('-', $fecha);
            $month = $time[1];
            $year = $time[0];
        else :
            $month = date('m');
            $year = date('Y');
        endif;

        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));

        return date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
    }

    /**
     * Retorna el Primer dia del Mes
     * @return type
     */
    function Primerdia($fecha = '') {

        if (!empty($fecha)) :
            $time = explode('-', $fecha);
            $month = $time[1];
            $year = $time[0];
        else :
            $month = date('m');
            $year = date('Y');
        endif;

        return date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
    }

    /**
     * Retorna el Mes Actual
     * @return string
     */
    function Mes($m = '') {
        $mes = (!empty($m)) ? $m : date("F");
        if ($mes == "January" || $mes == '01')
            $mes = "Enero";
        if ($mes == "February" || $mes == '02')
            $mes = "Febrero";
        if ($mes == "March" || $mes == '03')
            $mes = "Marzo";
        if ($mes == "April" || $mes == '04')
            $mes = "Abril";
        if ($mes == "May" || $mes == '05')
            $mes = "Mayo";
        if ($mes == "June" || $mes == '06')
            $mes = "Junio";
        if ($mes == "July" || $mes == '07')
            $mes = "Julio";
        if ($mes == "August" || $mes == '08')
            $mes = "Agosto";
        if ($mes == "September" || $mes == '09')
            $mes = "Setiembre";
        if ($mes == "October" || $mes == '10')
            $mes = "Octubre";
        if ($mes == "November" || $mes == '11')
            $mes = "Noviembre";
        if ($mes == "December" || $mes == '12')
            $mes = "Diciembre";
        return $mes;
    }

    function cleanspace($cadena) {
        return str_replace(' ', '', $cadena);
    }

    /**
     * Envia el SMS para la Cita Comercial
     * @param type $model
     * @return int
     */
    public function enviarCitaSMS($datos, $idcomercial, $idcontacto, $destino) {

        Yii::import("ext.httpclient.*");

        $cadena = "";
        $int1 = 0;
        $int2 = 0;

        if ($datos['telefono'] != 0 && !empty($datos['telefono'])) :
            $cadena .= $datos['telefono'] . "-";
        endif;

        if ($datos['celular'] != 0 && !empty($datos['celular'])):
            $cadena .= $datos['celular'];
        endif;

        $add = '';

        if ($datos['plan'] == 2) :
            $add .= '*Plan:Instale Ya!';
        endif;

        if ($destino != 0) :

            $time = explode(" ", $datos['datecita']);

            $texto = "Telecita:" . $this->cleanspace($datos['razon']) . "-" . $this->cleanspace($datos['nombre']) .
                    "*Dia:" . $time[0] . "*Hora:" . $time[1] . "*Tel:" . $cadena .
                    "*Dir:" . $this->cleanspace($datos['direccion'] . " " . $add);

            $longitud = strlen($texto);

            Mensajeria::model()->logMensajeria($idcomercial, 'SMS', $texto, $idcontacto);

            if ($longitud <= 150) :
                $int1 = $this->urlsms($texto, $destino);
                $int2 = 1;
                $int3 = 1;
            endif;
            if ($longitud >= 151 && $longitud <= 300) :
                $cadena1 = substr($texto, 0, 150);
                $cadena2 = substr($texto, 151, 300);
                $int1 = $this->urlsms($cadena1, $destino);
                $int2 = $this->urlsms($cadena2, $destino);
                $int3 = 1;
            endif;
            if ($longitud >= 301) :
                $cadena1 = substr($texto, 0, 150);
                $cadena2 = substr($texto, 151, 300);
                $cadena3 = substr($texto, 301, 450);
                $int1 = $this->urlsms($cadena1, $destino);
                $int2 = $this->urlsms($cadena2, $destino);
                $int3 = $this->urlsms($cadena3, $destino);
            endif;
            $ezum = $int1 + $int2 + $int3;
            if ($ezum == 3) :
                return 1;
            else :
                return 0;
            endif;
        else :
            return 2;
        endif;
    }

    public function urlsms($texto, $destino) {
        Yii::import("ext.httpclient.*");
        $message = urlencode($texto);
        $sitios = Sitios::model()->findByPk(13);
        $connection = $sitios->Controlador . "?mensaje=" . $message . "&destino=" . $destino;
        $client = new EHttpClient($connection, array(
            "maxredirects" => 3,
            "timeout" => 10
        ));
        $response = $client->request();

        try {
            if ($response->isSuccessful()) :
                if ($response->getBody() == 'OK') :
                    $mensaje = 1;
                else :
//                    $mensaje = "No se envio el SMS Intentelo de Nuevo";
                    $mensaje = 0;
                endif;
            else :
//                $mensaje = "Error con el Servidor Intentelo mas Tarde";
                $mensaje = 0;
            endif;
        } catch (Exception $e) {
            $mensaje = $e;
        }

        return $mensaje;
    }

    /**
     * Enviar Contacto a 4D despues de haberlo Asignado a el Asesor
     * @param type $id
     * @throws EHttpClientException
     */
    public function sendContacto($id, $regional) {
        if ($regional == 5001) :
            $sitio = Sitios::model()->findByPk(10);
        else :
            $sitio = Sitios::model()->findByPk(9);
        endif;

        Yii::import("ext.httpclient.*");
        $url = $sitio->Controlador;
        $c = Contacto::model()->findByPk($id);
        $client = new EHttpClient($url);
        $arr = array();
        $time = explode("-", $c->Fecha_creacion);
        $arr['ID'] = $c->ID;
        $arr['Telemercaderista'] = $c->Telemercaderista;
        $arr['Tipo'] = $c->Tipo;
        $arr['Razon_social'] = $c->Razon_social;
        $arr['Nit'] = $c->Nit;
        $arr['Nombre_completo'] = $c->Nombre_completo;
        $arr['Direccion'] = $c->Direccion;
        $arr['Direccion_encargado'] = $c->Direccion_encargado;
        $arr['Telefono'] = $c->Telefono;
        $arr['Celular'] = $c->Celular;
        $arr['Email'] = $c->Email;
        $arr['Cliente_actual'] = $c->Cliente_actual;
        $arr['Ciudad'] = $regional;
        $arr['Agencia'] = $c->Agencia;
        $arr['Asesor'] = $c->Asesor;
        $arr['Barrio'] = $c->Barrio;
        $arr['Persona_contacto'] = $c->Persona_contacto;
        $arr['Medio_contacto'] = $c->Medio_contacto;
        $arr['Documento'] = $c->Documento;
        $arr['Digito_verificacion'] = $c->Digito_verificacion;
        $arr['Tipodoc'] = $c->Tipodoc;
        $arr['Observaciones'] = $c->Observaciones;
        $arr['Contrato'] = $c->Contrato;
        $arr['Estado'] = $c->Estado;
        $arr['Busqueda'] = $c->Busqueda;
        $arr['Fecha_creacion'] = $time[1] . "/" . $time[2] . "/" . $time[0];
        $arr['Hora_creacion'] = $c->Hora_creacion;
        $arr['Tipo_cliente'] = $c->Tipo_cliente;
        $arr['Segmentacion'] = $c->Segmentacion;
        $arr['Producto_interes'] = "Monitoreo";
        $arr['Actividad'] = $c->Actividad;
        $arr['Servicio'] = "Monitoreo";
        $arr['pruebas'] = $c->pruebas;
        $arr['Consecutivo4D'] = $c->Consecutivo4D;
        $arr['Bloqueado'] = $c->Bloqueado;
        $arr['Aprobado4D'] = $c->Aprobado4D;
        $arr['Estado_proceso'] = $c->estadoProceso->Nombre;
        $arr['ID_cierre'] = $c->ID_cierre;
        $arr['Tipo_contacto'] = $c->tipoContacto->Descripcion;
        $arr['Tiene_equipos'] = $c->Tiene_equipos;
        $arr['Clase_vehiculo'] = $c->Clase_vehiculo;
        $arr['Cantidad_vehiculos'] = $c->Cantidad_vehiculos;
        $client->setParameterPost('info', json_encode($arr));
        $response = $client->request('POST');

        try {
            if ($response->isSuccessful()) :
                return true;
            else :
                return false;
            endif;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Genera la Respuesta para el Cliente segun su Seleccion
     * @param String $email Email del Cliente
     * @param int $opcion Tipo de Respuesta Si : 1 ,  No : 2
     * @return boolean Respuesta del  Envio
     */
    public function sendMail($datos, $idcomercial, $idcontacto) {

        Yii::import('ext.phpmailer.JPhpMailer');
        $mail = new JPhpMailer();
        $mail->isSMTP();                                                  // Set mailer to use SMTP
        $mail->Host = 'email-smtp.us-west-2.amazonaws.com';               // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                           // Enable SMTP authentication
        $mail->Username = 'AKIAIURMRPOHPU32Q7DQ';                         // SMTP username
        $mail->Password = ' AkmTrrcl32b0FIoKrE6/moNqK1fU0jg+eD3CFNKACxEp';                                     // SMTP password
        $mail->SMTPSecure = 'tls';                                        // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 2587;                                               // TCP port to connect to
        $mail->SetFrom('telemensajes@telesentinel.com', 'TELESENTINEL');
        $mail->AddAddress($datos['emailasesor'], 'Funcionario');  // Add a recipient
        $mail->Subject = 'Telesentinel - Proxima Cita!';

        $mail->AltBody = 'Este Mensaje es de Uso Interno de la Compañia';

        $html = '   <font face="arial" size=4 color=blue><p>
                    <h3><b>Estimado Funcionario.</b></h3><br>
                    Presenta Cita para el dia ' . $datos['datecita'] . '<br>
                    <ul>
                        <li>*) Razon Social    : ' . $datos['razon'] . '</li>
                        <li>*) Nombre Completo : ' . $datos['nombre'] . '</li>
                        <li>*) Telefonos       : ' . $datos['telefono'] . ' - ' . $datos['celular'] . '</li>
                        <li>*) Direccion       : ' . $datos['direccion'] . '</li>
                    </ul>
                    </p></font>';

        $html .= '<br>'
                . '<img src="http://www.telesentinel.com.mx/images2/firma2.jpg" class="block " width="180" height="280"  style="border: 0px; transform-origin: left top 0px; display: block;" alt="">'
                . '<br><b>Departamento Comercial </b><br>'
                . 'Tel.: (57-1) 2888788 Ext.: 1501 <br>'
                . '|Brasil|Colombia|Inglaterra|Israel|Mexico|';

        $mail->MsgHTML(utf8_decode($html));

        Mensajeria::model()->logMensajeria($idcomercial, 'EMAIL', $html, $idcontacto);

        try {
            if ($mail->send()) :
                return 0;
            else :
                return 1;
            endif;
        } catch (Exception $e) {
            return 1;
        }
    }

    /**
     * Genera la Respuesta para el Cliente segun su Seleccion
     * @param String email Email del Cliente
     * @param int opcion Tipo de Respuesta Si : 1 ,  No : 2
     * @return boolean Respuesta del  Envio
     */
    public function sendMailReenvio($html, $emailasesor) {

        Yii::import('ext.phpmailer.JPhpMailer');
        $mail = new JPhpMailer();
        $mail->isSMTP();                                                  // Set mailer to use SMTP
        $mail->Host = 'email-smtp.us-west-2.amazonaws.com';               // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                           // Enable SMTP authentication
        $mail->Username = 'AKIAIURMRPOHPU32Q7DQ';                         // SMTP username
        $mail->Password = ' AkmTrrcl32b0FIoKrE6/moNqK1fU0jg+eD3CFNKACxEp';                                     // SMTP password
        $mail->SMTPSecure = 'tls';                                        // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 2587;                                               // TCP port to connect to
        $mail->SetFrom('telemensajes@telesentinel.com', 'TELESENTINEL');
        $mail->AltBody = 'Este Mensaje es de Uso Interno de la Compañia';
        $mail->AddAddress($emailasesor, 'Funcionario');  // Add a recipient
        $mail->Subject = 'Telesentinel - Proxima Cita!';

        $mail->MsgHTML(urlencode(utf8_decode($html)));

        try {
            if ($mail->send()) :
                return 0;
            else :
                return 1;
            endif;
        } catch (Exception $e) {
            return 1;
        }
    }

    public function getRegionalesXNivel() {

        $reg = RegionalAsesor::model()->findAll('ID_Asesor = ' . Yii::app()->user->getState('id_usuario') . ' AND Estado = 1');
        $arr_aux = array();
        foreach ($reg as $r) :
            $arr_aux[] = $r->ID_Regional;
        endforeach;
        $reglist = array();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('ID_Regional', $arr_aux);
        $regionales = Regional::model()->findAll($criteria);
        if (count($regionales) > 0) {
            foreach ($regionales as $regional) {
                $reglist[] = $regional;
            }
        }
        return $reglist;
    }

    /**
     * Genera la Respuesta para el Cliente segun su Seleccion
     * @param String $email Email del Cliente
     * @param int $opcion Tipo de Respuesta Si : 1 ,  No : 2
     * @return boolean Respuesta del  Envio
     */
    public function sendMailPro($datos) {

        Yii::import('ext.phpmailer.JPhpMailer');
        $mail = new JPhpMailer();
        $mail->isSMTP();                                                  // Set mailer to use SMTP
        $mail->Host = 'email-smtp.us-west-2.amazonaws.com';               // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                           // Enable SMTP authentication
        $mail->Username = 'AKIAIURMRPOHPU32Q7DQ';                         // SMTP username
        $mail->Password = ' AkmTrrcl32b0FIoKrE6/moNqK1fU0jg+eD3CFNKACxEp';                                     // SMTP password
        $mail->SMTPSecure = 'tls';                                        // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 2587;                                               // TCP port to connect to
        $mail->SetFrom('telemensajes@telesentinel.com', 'TELESENTINEL');
        $mail->AddAddress('todosavender@telesentinel.com', 'Telemercadeo');  // Add a recipient
        $mail->Subject = 'Todos a Vender!';

        $mail->AltBody = 'Este Mensaje es de Uso Interno de la Compañia';

        $html = '   <font face="arial" size=4><p>
                    <h3><b>Registro Referido por : ' . $datos['quien'] . '</b></h3><br>
                    <ul>
                        <li>*) Razon Social    : ' . $datos['razon'] . '</li>
                        <li>*) Nombre Completo : ' . $datos['nombre'] . '</li>
                        <li>*) Telefonos       : ' . $datos['telefono'] . ' - ' . $datos['celular'] . '</li>
                        <li>*) Direccion       : ' . $datos['direccion'] . '</li>
                        <li>*) Producto de Interes  : ' . $datos['producto'] . '</li>
                        <li>*) Ref. Origen     : ' . $datos['origen'] . '</li>
                    </ul>
                    <br />
                    <b>NOTA : Si es Cliente Activo verifique la Razon Social o el Contrato</b>
                    </p>
                    </font>';

        $html .= '<br>'
                . '<img src="http://www.telesentinel.com.mx/images2/firma2.jpg" class="block " width="180" height="280"  style="border: 0px; transform-origin: left top 0px; display: block;" alt="">'
                . '<br><b>Departamento Comercial </b><br>'
                . 'Tel.: (57-1) 2888788 Ext.: 1501 <br>'
                . '|Brasil|Colombia|Inglaterra|Israel|Mexico|';

        $mail->MsgHTML(utf8_decode($html));

        try {
            if ($mail->send()) :
                return 0;
            else :
                return 1;
            endif;
        } catch (Exception $e) {
            return 1;
        }
    }

    /**
     * Envia el SMS para la Cita Comercial
     * @param type $model
     * @return int
     */
    public function enviarNotificacionSMS($datos) {

        $sms = AsesorOportunidad::model()->teleSms();
        if ($sms != NULL) :

            foreach ($sms as $s) :
                $int1 = 0;
                $int2 = 0;
                $int3 = 0;

                $texto = 'TeleReg:' . $this->cleanspace($datos['nombre']) . '-*Tel:' . $this->cleanspace($datos['telefono']) . '-*@:' . $this->cleanspace($datos['email']) . '-*Ciu:' . $this->cleanspace($datos['ciudad']) . '-*Org:' . $this->cleanspace($datos['origen']);
                $longitud = strlen($texto);
                if ($longitud <= 150) :
                    $int1 = $this->urlsms($texto, $s);
                    $int2 = 1;
                    $int3 = 1;
                endif;
                if ($longitud >= 151 && $longitud <= 300) :
                    $cadena1 = substr($texto, 0, 150);
                    $cadena2 = substr($texto, 151, 300);
                    $int1 = $this->urlsms($cadena1, $s);
                    $int2 = $this->urlsms($cadena2, $s);
                    $int3 = 1;
                endif;
                if ($longitud >= 301) :
                    $cadena1 = substr($texto, 0, 150);
                    $cadena2 = substr($texto, 151, 300);
                    $cadena3 = substr($texto, 301, 450);
                    $int1 = $this->urlsms($cadena1, $s);
                    $int2 = $this->urlsms($cadena2, $s);
                    $int3 = $this->urlsms($cadena3, $s);
                endif;
                $ezum = $int1 + $int2 + $int3;
                if ($ezum == 3) :
                    return 1;
                else :
                    return 0;
                endif;
            endforeach;
        endif;
    }

    public function crearMailChimp($email, $datos) {
        Yii::import('ext.mailchimp.*');
        $myObject = new MailChimp();
        $options = array(
            'email_address' => $email,
            'status' => 'subscribed',
            'merge_fields' => $datos
        );

        $myObject->post("lists/7a79313e50/members", $options);
    }

    public function actualizarMailChimp($email, $datos, $md5 = false) {
        Yii::import('ext.mailchimp.*');
        $myObject = new MailChimp();
        $options = array(
            'merge_fields' => $datos
        );

        if ($md5) :
            $myObject->patch("lists/7a79313e50/members/" . $email, $options);
        else :
            $myObject->patch("lists/7a79313e50/members/" . md5($email), $options);
        endif;
    }

    public function IntervaloSemanal($intervalo = '') {

        if (!empty($intervalo)) :
            $dd = explode("-", $intervalo);
            $year = $dd[0];
            $month = $dd[1];
            $day = $dd[2];
        else :
            $year = date('Y');
            $month = date('m');
            $day = date('d');
        endif;

        # Obtenemos el día de la semana de la fecha dada
        $diaSemana = date("w", mktime(0, 0, 0, $month, $day, $year));

        # el 0 equivale al domingo...
        if ($diaSemana == 0) :
            $diaSemana = 7;
        endif;

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $primerDia = date("Y-m-d", mktime(0, 0, 0, $month, $day - $diaSemana + 1, $year));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $ultimoDia = date("Y-m-d", mktime(0, 0, 0, $month, $day + (7 - $diaSemana), $year));

        return array($primerDia, $ultimoDia);
    }

    public function totalTipos($tipo, $fechainicial, $fechafinal, $ciudad) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        if (!empty($ciudad)) :
            if ($ciudad[0] == '11001') :
                $ciu = array('5001', '68001', '76001');
                $criteria->addNotInCondition('Ciudad', $ciu);
            else :
                $criteria->addInCondition('Ciudad', $ciudad);
            endif;
        endif;
        if (!empty($fechainicial) && !empty($fechafinal)) :
            $criteria->addBetweenCondition('Fecha_creacion', $fechainicial, $fechafinal);
        else :
            $criteria->addBetweenCondition('Fecha_creacion', Contacto::model()->_data_first_month_day(), Contacto::model()->_data_last_month_day());
        endif;
        return Contacto::model()->count($criteria);
    }
    
     /**
     * 
     * @param type $fechainicial
     * @param type $fechafinal
     * @return type
     */
    public function getConTipFecGeneral($fechainicial, $fechafinal, $estados, $ciudad) {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('Id_Estadoweb', $estados);
        if (sizeof($ciudad) > 0) :
            if ($ciudad[0] == '11001') :
                $ciu = array('5001', '68001', '76001');
                $criteria->addNotInCondition('Ciudad', $ciu);
            else :
                $criteria->addInCondition('Ciudad', $ciudad);
            endif;
        endif;
        $criteria->addBetweenCondition('Fecha_creacion', $fechainicial, $fechafinal);
        return Contacto::model()->count($criteria);
    }

}
