<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ActiveRecord extends CActiveRecord {

    public $_attributes;
    public $attr;

//Metodo para las conexion es de los modelos con 4D//
    public function conexion($ruta = "", $datos = array(), $tipo = "GET") {
        $results = array(
            "status" => true,
            "message" => ""
        );
        Yii::import("ext.httpclient.*");
        if (!empty($ruta) && count($datos) > 0) {
            $client = new EHttpClient($ruta, array(
                "maxredirects" => 3,
                "timeout" => 50,
            ));
            try {
                if ($tipo == "POST") {
                    $client->setParameterPost($datos);
                    $response = $client->request("POST");
                } else {
                    $client->setParameterGet($datos);
                    $response = $client->request("GET");
                }
                if ($response->isSuccessful()) {
                    $results["status"] = true;
                    $results["message"] = $response->getBody();
                } else {
                    Yii::log($response->getRawBody(), "error");
                    $results["status"] = false;
                    $results["message"] = "Error al conectarse con el servidor, comuniquese con soporte tecnico";
                }
            } catch (EHttpClientException $e) {
                Yii::log($e->getMessage(), "error");
                $results["status"] = false;
                $results["message"] = "No hay conexion con el servidor, intentelo mas tarde";
            }
        } else {
            $results["status"] = false;
            $results["message"] = "Hacen falta parametros";
        }
        return $results;
    }

    ///////////////////////PARA GUARDAR CAMBIOS X TABLA////////////////////////

    protected function afterFind() {
        parent::afterFind();
        if (!empty($this->attr)) {
            if (!is_array($this->attr)) {
                if ($this->attr == "*") {
                    $this->_attributes = $this->attributes;
                }
            } else {
                $this->_attributes = array_intersect_key($this->attributes, array_flip($this->attr));
            }
        }
        return true;
    }

    protected function afterSave() {
        parent::afterSave();
        $ID = $this->ID;
        if (!empty($this->_attributes)) {
            $oldrecord = $this->_attributes;
            foreach ($this->_attributes as $name => $attribute) {
                if ($this->attributes[$name] != $attribute) {
                    $this->crearLog($this, $oldrecord, $name);
                }
            }
        }
        return true;
    }

    private function crearLog($newmodel, $oldmodel, $field) {
        date_default_timezone_set('America/Bogota');
        $usuario = (!empty(app()->user->getState("id_usuario")) ? app()->user->getState("id_usuario") : -99999);
        $log = new Logs;
        $log->ID_Tabla = $newmodel->ID;
        $log->Fecha = date("Y-m-d");
        $log->Hora = date("H:i:s");
        $log->Tabla = $this->tableName();
        $log->Usuario = $usuario;
        $log->Campo = $field;
        $log->Valor_anterior = $oldmodel[$field];
        $log->Nuevo_valor = $newmodel->attributes[$field];
        $log->save();
    }

    public function getLog($campo = "") {
        $logs = Logs::model()->findAllByAttributes(array(
            "Tabla" => $this->tableName(),
            "ID" => $this->ID
        ));
    }

    //////////////////////ENVIAR SMS////////////////////////////

    public function enviarSMS($cel = null, $msg = "") {
        if (!empty($cel)) {
            $sitio = Sitios::model()->findByPk(13);
            if ($sitio != null) {
                $ruta = $sitio->Controlador;
                $datos = array(
                    "destino" => $cel,
                    "mensaje" => base64_encode($msg),
                    "base64" => 1
                );
                $result = $this->conexion($ruta, $datos, "POST");
                if ($result ["status"] == false) {
                    Yii::log("Error al intentar enviar el sms, el sistema devolvio " . $result["message"]);
                }
            } else {
                Yii::log("No hay registro en 'Sitios' para enviar SMS (Married)", "error");
            }
        }
    }

}
