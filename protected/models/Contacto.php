<?php

/**
 * This is the model class for table "Contacto".
 *
 * The followings are the available columns in table 'Contacto':
 * @property integer $ID
 * @property integer $Telemercaderista
 * @property integer $Tipo
 * @property string $Razon_social
 * @property string $Nit
 * @property string $Nombre_completo
 * @property string $Direccion
 * @property string $Direccion_encargado
 * @property string $Telefono
 * @property string $Celular
 * @property string $Email
 * @property integer $Cliente_actual
 * @property integer $Ciudad
 * @property integer $Agencia
 * @property integer $Asesor
 * @property string $Barrio
 * @property string $Persona_contacto
 * @property string $Medio_contacto
 * @property string $Documento
 * @property integer $Digito_verificacion
 * @property string $Tipodoc
 * @property string $Observaciones
 * @property integer $Contrato
 * @property integer $Estado
 * @property string $Busqueda
 * @property string $Fecha_creacion
 * @property string $Hora_creacion
 * @property integer $Tipo_cliente
 * @property string $Segmentacion
 * @property integer $Producto_interes
 * @property string $Actividad
 * @property string $Servicio
 * @property string $pruebas
 * @property integer $Consecutivo4D
 * @property integer $Bloqueado
 * @property integer $Aprobado4D
 * @property integer $Estado_proceso
 * @property integer $ID_cierre
 * @property integer $Tipo_contacto
 * @property integer $Tiene_equipos
 * @property string $Clase_vehiculo
 * @property integer $Cantidad_vehiculos
 * @property integer $TipoDescartado
 * @property integer $Sincronizado
 * @property integer $ID_plan
 * @property integer $Oportunidad
 * @property String $Fecha_Ultgestion
 * @property String $Fecha_Ultagenda
 * @property String $Decision_compra
 * @property integer $Horizontal
 *
 * The followings are the available model relations:
 * @property Agendados[] $agendadoses
 * @property Asesor $telemercaderista
 * @property TipoContacto $tipoContacto
 * @property Planes $iDPlan
 * @property ProductoInteres $productoInteres
 * @property Agencia $agencia
 * @property Asesor $asesor
 * @property Ciudad $ciudad
 * @property TipoCliente $tipoCliente
 * @property Cotizacion[] $cotizacions
 * @property GestionComercial[] $gestionComercials
 * @property GrupoCotizacion[] $grupoCotizacions
 * @property HistoricoGestion[] $historicoGestions
 * @property Mensajeria[] $mensajerias
 * @property PersonaContacto[] $personaContactos
 * @property ServicioInteres[] $servicioInteres
 * @property RuteroComercial[] $ruteroComercials
 * @property DetalleCalendario[] $detalleCalendarios
 */
class Contacto extends CActiveRecord {

    public $Contador;
    public $EstadoProceso;
    public $MedioContacto;
    public $tipCont = 0;
    public $searchasesor;
    public $searchagencia;
    public $searchciudad;
    public $searchtipocontacto;
    public $tipDesc = 0;
    public $counter;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Contacto';
    }

    public function getEstado() {
        return $this->EstadoProceso;
    }

    public function setEstado($estado) {
        $this->EstadoProceso = $estado;
    }

    public function setMedio($medio) {
        $this->MedioContacto = $medio;
    }

    public function getMedio() {
        return $this->MedioContacto;
    }

    public function getDescartado() {
        return $this->tipDesc;
    }

    public function setDescartado($opcion) {
        $this->tipDesc = $opcion;
    }

    public function getContacto() {
        return $this->tipCont;
    }

    public function setContacto($opcion) {
        $this->tipCont = $opcion;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Razon_social, Ciudad', 'required'),
            array('Telemercaderista, Tipo, Cliente_actual, Ciudad, Agencia, Asesor, Digito_verificacion, Contrato, Estado, Tipo_cliente, Producto_interes, Consecutivo4D, Bloqueado, Aprobado4D, Estado_proceso, ID_cierre, Tipo_contacto, Tiene_equipos, Cantidad_vehiculos, TipoDescartado, Sincronizado, ID_plan, Oportunidad, Horizontal', 'numerical', 'integerOnly' => true),
            array('Razon_social, Nombre_completo', 'length', 'max' => 500),
            array('Nit, Direccion_encargado, Clase_vehiculo, Decision_compra', 'length', 'max' => 100),
            array('Telefono', 'length', 'max' => 10),
            array('Celular', 'length', 'max' => 14),
            array('Email, Persona_contacto, Medio_contacto, Actividad', 'length', 'max' => 200),
            array('Barrio, Tipodoc', 'length', 'max' => 45),
            array('Documento, Segmentacion', 'length', 'max' => 20),
            array('Servicio', 'length', 'max' => 150),
            array('Direccion, Observaciones, Busqueda, Fecha_creacion, Fecha_Ultgestion, Hora_creacion, pruebas, Decision_compra', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Telemercaderista, Tipo, Razon_social, Nit, Nombre_completo, Direccion, Direccion_encargado, '
                . 'Telefono, Celular, Email, Cliente_actual, Ciudad, Agencia, Asesor, Barrio, Persona_contacto, Medio_contacto, '
                . 'Documento, Digito_verificacion, Tipodoc, Observaciones, Contrato, Estado, Busqueda, Fecha_creacion, Hora_creacion, Fecha_Ultgestion, Fecha_Ultagenda, '
                . 'Tipo_cliente, Segmentacion, Producto_interes, Actividad, Servicio, pruebas, Consecutivo4D, Bloqueado,'
                . 'Aprobado4D, ID_cierre, Tipo_contacto, Tiene_equipos, Clase_vehiculo, Cantidad_vehiculos, searchtipocontacto, '
                . 'searchciudad, searchagencia, searchasesor, ID_plan, Oportunidad, Decision_compra', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'agendadoses' => array(self::HAS_MANY, 'Agendados', 'ID_Contacto'),
            'telemercaderista' => array(self::BELONGS_TO, 'Asesor', 'Telemercaderista'),
            'tipoContacto' => array(self::BELONGS_TO, 'TipoContacto', 'Tipo_contacto'),
            'estadoProceso' => array(self::BELONGS_TO, 'EstadoVenta', 'Estado_proceso'),
            'iDPlan' => array(self::BELONGS_TO, 'Planes', 'ID_plan'),
            'productoInteres' => array(self::BELONGS_TO, 'ProductoInteres', 'Producto_interes'),
            'agencia' => array(self::BELONGS_TO, 'Agencia', 'Agencia'),
            'asesor' => array(self::BELONGS_TO, 'Asesor', 'Asesor'),
            'ciudad' => array(self::BELONGS_TO, 'Ciudad', 'Ciudad'),
            'tipoCliente' => array(self::BELONGS_TO, 'TipoCliente', 'Tipo_cliente'),
            'cotizacions' => array(self::HAS_MANY, 'Cotizacion', 'Contacto'),
            'gestionComercials' => array(self::HAS_MANY, 'GestionComercial', 'ID_Contacto'),
            'grupoCotizacions' => array(self::HAS_MANY, 'GrupoCotizacion', 'ID_Contacto'),
            'historicoGestions' => array(self::HAS_MANY, 'HistoricoGestion', 'ID_Contacto'),
            'mensajerias' => array(self::HAS_MANY, 'Mensajeria', 'ID_Contacto'),
            'personaContactos' => array(self::HAS_MANY, 'PersonaContacto', 'Contacto'),
            'servicioInteres' => array(self::HAS_MANY, 'ServicioInteres', 'ID_Contacto'),
            'ruteroComercials' => array(self::HAS_MANY, 'Rutero', 'ID_Contacto'),
            'detalleCalendarios' => array(self::HAS_MANY, 'DetalleCalendario', 'ID_Contacto'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Telemercaderista' => 'Telemercaderista',
            'Tipo' => 'Tipo',
            'Razon_social' => 'Razon Social',
            'Nit' => 'Nit',
            'Nombre_completo' => 'Nombre Completo',
            'Direccion' => 'Direccion',
            'Direccion_encargado' => 'Direccion Encargado',
            'Telefono' => 'Telefono',
            'Celular' => 'Celular',
            'Email' => 'Email',
            'Cliente_actual' => 'Cliente Actual',
            'Ciudad' => 'Ciudad',
            'Agencia' => 'Agencia',
            'Asesor' => 'Asesor',
            'Barrio' => 'Barrio',
            'Persona_contacto' => 'Persona Contacto',
            'Medio_contacto' => 'Medio Contacto',
            'Documento' => 'Documento',
            'Digito_verificacion' => 'Digito Verificacion',
            'Tipodoc' => 'Tipodoc',
            'Observaciones' => 'Observaciones',
            'Contrato' => '# Contrato Referido',
            'Estado' => 'Estado',
            'Busqueda' => 'Busqueda',
            'Fecha_creacion' => 'Fecha Creacion',
            'Fecha_Ultgestion' => 'Fecha Ult Gestion',
            'Hora_creacion' => 'Hora Creacion',
            'Tipo_cliente' => 'Tipo Cliente',
            'Segmentacion' => 'Segmentacion',
            'Producto_interes' => 'Producto Interes',
            'Actividad' => 'Actividad',
            'Servicio' => 'Servicio',
            'pruebas' => 'Pruebas',
            'Consecutivo4D' => 'Consecutivo4 D',
            'Bloqueado' => 'Bloqueado',
            'Aprobado4D' => 'Aprobado4 D',
            'Estado_proceso' => 'Estado Proceso',
            'ID_cierre' => 'Id Cierre',
            'Tipo_contacto' => 'Tipo Contacto',
            'Tiene_equipos' => 'Tiene Equipos',
            'Clase_vehiculo' => 'Clase Vehiculo',
            'Cantidad_vehiculos' => 'Cantidad Vehiculos',
            'searchasesor' => 'Funcionario',
            'searchagencia' => 'Agencia',
            'searchciudad' => 'Ciudad',
            'searchtipocontacto' => 'Tipo Contacto',
            'TipoDescartado' => 'Tipo Descartado',
            'Sincronizado' => 'Sincronizado',
            'ID_plan' => 'Planes de Interes',
            'Oportunidad' => 'Oportunidad Triangulo',
            'Decision_compra' => 'Decision de Compra',
            'Horizontal' => 'Propiedad Horizontal'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('t.ID', $this->ID);
        $criteria->compare('Telemercaderista', $this->Telemercaderista);
        $criteria->compare('Tipo', $this->Tipo);
        $criteria->compare('t.Razon_social', $this->Razon_social, true);
        $criteria->compare('t.Nit', $this->Nit, true);
        $criteria->compare('Nombre_completo', $this->Nombre_completo, true);
        $criteria->compare('t.Direccion', $this->Direccion, true);
        $criteria->compare('t.Direccion_encargado', $this->Direccion_encargado, true);
        $criteria->compare('t.Telefono', $this->Telefono, true);
        $criteria->compare('t.Celular', $this->Celular, true);
        $criteria->compare('t.Email', $this->Email, true);
        $criteria->compare('Cliente_actual', $this->Cliente_actual);
        $criteria->compare('Ciudad', $this->Ciudad);
        $criteria->compare('Agencia', $this->Agencia);
        $criteria->compare('Asesor', $this->Asesor);
        $criteria->compare('Barrio', $this->Barrio, true);
        $criteria->compare('Persona_contacto', $this->Persona_contacto, true);
        $criteria->compare('Medio_contacto', $this->Medio_contacto, true);
        $criteria->compare('Documento', $this->Documento, true);
        $criteria->compare('Digito_verificacion', $this->Digito_verificacion);
        $criteria->compare('Tipodoc', $this->Tipodoc, true);
        $criteria->compare('Observaciones', $this->Observaciones, true);
        $criteria->compare('t.Contrato', $this->Contrato);
        $criteria->compare('Estado', $this->Estado);
        $criteria->compare('Busqueda', $this->Busqueda, true);
        $criteria->compare('t.Fecha_creacion', $this->Fecha_creacion, true);
        $criteria->compare('Hora_creacion', $this->Hora_creacion, true);
        $criteria->compare('Tipo_cliente', $this->Tipo_cliente);
        $criteria->compare('Segmentacion', $this->Segmentacion, true);
        $criteria->compare('Producto_interes', $this->Producto_interes);
        $criteria->compare('Actividad', $this->Actividad, true);
        $criteria->compare('Servicio', $this->Servicio, true);
        $criteria->compare('pruebas', $this->pruebas, true);
        $criteria->compare('Consecutivo4D', $this->Consecutivo4D);
        $criteria->compare('Bloqueado', $this->Bloqueado);
        $criteria->compare('Aprobado4D', $this->Aprobado4D);
        $criteria->compare('Estado_proceso', $this->Estado_proceso);
        $criteria->compare('ID_cierre', $this->ID_cierre);
        $criteria->compare('Tipo_contacto', $this->Tipo_contacto);
        $criteria->compare('Tiene_equipos', $this->Tiene_equipos);
        $criteria->compare('Clase_vehiculo', $this->Clase_vehiculo, true);
        $criteria->compare('Cantidad_vehiculos', $this->Cantidad_vehiculos);
        $criteria->compare('asesor.Nombre', $this->searchasesor, true);
        $criteria->compare('agencia.Nombre', $this->searchagencia, true);
        $criteria->compare('ciudad.Nombre', $this->searchciudad, true);
        $criteria->compare('tipoContacto.Descripcion', $this->searchtipocontacto, true);
        $criteria->compare('TipoDescartado', $this->TipoDescartado, true);

        $criteria->with = array(
            'asesor' => array('Nombre LIKE "%' . $this->searchasesor . '%"'),
            'agencia' => array('Nombre LIKE "%' . $this->searchagencia . '%"'),
            'tipoContacto' => array('Descripcion LIKE "%' . $this->searchtipocontacto . '%"'),
            'ciudad' => array('Nombre LIKE "%' . $this->searchciudad . '%"'),
        );

        if ($this->getEstado() == 0) :
            $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Estado_proceso != 19 ');
        else :

            $criteria->addCondition('Telemercaderista = ' . Yii::app()->user->getState('id_usuario') . ' AND Estado_proceso = ' . $this->getEstado());

            if ($this->tipCont != 0) :
                $criteria->addCondition('Tipo_contacto = ' . $this->getContacto());
            endif;

            if ($this->tipDesc != 0) :
                $criteria->addCondition('TipoDescartado = ' . $this->getDescartado());
            endif;

        endif;

        if ($this->getMedio() == 'OPORTUNIDAD TRIANGULO') :
            $criteria->addCondition('Medio_contacto = "' . $this->getMedio() . '"');
        endif;

        $criteria->order = 't.Fecha_creacion DESC, t.Hora_creacion DESC';

        return new CActiveDataProvider(
                $this, array(
            'criteria' => $criteria,
            "pagination" => array(
                'pageSize' => 10
            )
                )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Contacto the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Temporalidad Semanal
     * @param type $dias
     * @param type $fecha
     * @return type
     */
    public function getTemporalidadsemanal($dias, $fechainicial, $fechafinal, $tipo = 'diario') {
        date_default_timezone_set('America/Bogota');
        $arr_aux = array();
        $intervaloinicial = date('Y-m-d', strtotime($dias, strtotime($fechainicial)));
        $intervalofinal = date('Y-m-d', strtotime($dias, strtotime($fechafinal)));
        $diames = date_format(date_create($intervalofinal), 'Y-m-d');
        $registros = $this->temporalidadContacto($intervaloinicial, $intervalofinal, $tipo);
        $total = 0;
        foreach ($registros as $c) :
            $total += $c->Contador;
        endforeach;

        $arr_aux['registros'] = $registros;
        $arr_aux['total'] = $total;
        $arr_aux['fecha'] = $diames;

        return $arr_aux;
    }

    /**
     * Retorna los Registros de los Contactos en la Fecha Asignada por Tipos de Contacto
     * @param type $fecha
     * @param type $fecha2
     * @return type
     */
    public function temporalidadContacto($fechainicial, $fechafinal, $tipo = '') {
        $criteria = new CDbCriteria();
        $criteria->select = 'Tipo_contacto, COUNT(*) as Contador';
        if ($tipo == 'diario' || $tipo == 'actual') :
            $criteria->condition = 'Fecha_creacion = "' . $fechafinal . '" ';
        else :
            $criteria->condition = 'Fecha_creacion BETWEEN "' . $fechainicial . '" AND "' . $fechafinal . '"';
        endif;
        $criteria->group = 'Tipo_contacto';
        $criteria->order = 'Tipo_contacto DESC';
        return Contacto::model()->findAll($criteria);
    }

    /**
     * Retorna el Mes Actual
     * @return string
     */
    public function getMes($m = '') {
        $month = explode('-', $m);
        $mes = (!empty($month[0])) ? $month[0] : date("F");
        if ($mes == "Jan" || $mes == '01')
            $mes = "Ene " . $month[1];
        if ($mes == "Feb" || $mes == '02')
            $mes = "Feb " . $month[1];
        if ($mes == "Mar" || $mes == '03')
            $mes = "Mar " . $month[1];
        if ($mes == "Apr" || $mes == '04')
            $mes = "Abr " . $month[1];
        if ($mes == "May" || $mes == '05')
            $mes = "May " . $month[1];
        if ($mes == "Jun" || $mes == '06')
            $mes = "Jun " . $month[1];
        if ($mes == "Jul" || $mes == '07')
            $mes = "Jul " . $month[1];
        if ($mes == "Aug" || $mes == '08')
            $mes = "Ago " . $month[1];
        if ($mes == "Sep" || $mes == '09')
            $mes = "Sep " . $month[1];
        if ($mes == "Oct" || $mes == '10')
            $mes = "Oct " . $month[1];
        if ($mes == "Nov" || $mes == '11')
            $mes = "Nov " . $month[1];
        if ($mes == "Dec" || $mes == '12')
            $mes = "Dic " . $month[1];
        return $mes;
    }

    /**
     * Retorna el dia de la Semana
     */
    public function getSemanario($semana) {
        $semanario = $semana;

        switch ($semanario) :
            case "Mon": $dia_esp = 'Lunes';
                break;
            case "Tue": $dia_esp = 'Martes';
                break;
            case "Wed": $dia_esp = 'Miercoles';
                break;
            case "Thu": $dia_esp = 'Jueves';
                break;
            case "Fri": $dia_esp = 'Viernes';
                break;
            case "Sat": $dia_esp = 'Sabado';
                break;
            case "Sun": $dia_esp = 'Domingo';
                break;
        endswitch;

        return $dia_esp;
    }

    /**
     * 
     * @param type $fecha
     * @param type $fecha2
     */
    public function registroContactos($fecha, $fecha2) {
        $matriz = array();

        $contactos = Yii::app()->db->createCommand()
                ->select('r.ID_Regional as Ciudad, c.Tipo_contacto as Descripcion, COUNT(*) as Total')
                ->from('Contacto c, regional_ciudad r')
                ->where('c.Fecha_creacion BETWEEN :fecha AND :fecha2 AND c.Ciudad = r.ID_Ciudad', array(':fecha' => $fecha, ':fecha2' => $fecha2))
                ->group('r.ID_Regional, c.Tipo_contacto')
                ->order('r.ID_Regional ASC')
                ->queryAll();

        foreach ($contactos as $c):
            $matriz[$c['Ciudad']][$c['Descripcion']] = $c['Total'];
        endforeach;

        return $matriz;
    }

    /**
     * Carga las Regionales
     * @return Objeto 
     */
    public function getTiposContacto($tipo = '') {
        if (!empty($tipo)) :
            return TipoContacto::model()->findByPk($tipo);
        else :
            return TipoContacto::model()->findAll();
        endif;
    }

    /**
     * Carga las Regionales
     * @return Objeto 
     */
    public function getRegionales() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('Estado = 1');
        $data = Regional::model()->findAll($criteria);
        return $data;
    }

    /**
     * Carga las Regionales
     * @return Objeto 
     */
    public function getEtapasContacto() {
        return EstadoVenta::model()->findAll();
    }

    /**
     * Retorna los Registros de los Contactos en la Fecha Asignada por Etapas
     * @param type $fecha
     * @param type $fecha2
     * @return type
     */
    public function etapasContacto($fecha, $fecha2, $tipo = '') {
        $criteria = new CDbCriteria();
        $criteria->select = 'Estado_proceso, COUNT(*) as Contador';
        $criteria->condition = 'Fecha_creacion BETWEEN "' . $fecha . '" AND "' . $fecha2 . '"';
        if (!empty($tipo)) :
            $criteria->addCondition('Tipo_contacto = "' . $tipo . '"');
        endif;
        $criteria->group = 'Estado_proceso';
        $criteria->order = 'Contador DESC';
        return Contacto::model()->findAll($criteria);
    }

    /**
     * 
     * @param type $fecha
     * @param type $fecha2
     */
    public function registroEtapasContactos($fecha, $fecha2, $tipo = '') {
        $matriz = array();

        if (!empty($tipo)) :
            $contactos = Yii::app()->db->createCommand()
                    ->select('r.ID_Regional as Ciudad, c.Estado_proceso as Descripcion, COUNT(*) as Total')
                    ->from('Contacto c, regional_ciudad r')
                    ->where('c.Fecha_creacion BETWEEN :fecha AND :fecha2 AND c.Ciudad = r.ID_Ciudad AND Tipo_contacto = :tipo', array(':fecha' => $fecha, ':fecha2' => $fecha2, ':tipo' => $tipo))
                    ->group('r.ID_Regional, c.Estado_proceso')
                    ->order('r.ID_Regional ASC')
                    ->queryAll();
        else :
            $contactos = Yii::app()->db->createCommand()
                    ->select('r.ID_Regional as Ciudad, c.Estado_proceso as Descripcion, COUNT(*) as Total')
                    ->from('Contacto c, regional_ciudad r')
                    ->where('c.Fecha_creacion BETWEEN :fecha AND :fecha2 AND c.Ciudad = r.ID_Ciudad', array(':fecha' => $fecha, ':fecha2' => $fecha2))
                    ->group('r.ID_Regional, c.Estado_proceso')
                    ->order('r.ID_Regional ASC')
                    ->queryAll();
        endif;
        foreach ($contactos as $c):
            $matriz[$c['Ciudad']][$c['Descripcion']] = $c['Total'];
        endforeach;
        return $matriz;
    }

    /**
     * Retorna los Registros de los Contactos en la Fecha Asignada por Asesor
     * @param type $fecha
     * @param type $fecha2
     * @return type
     */
    public function asesoresContacto($fecha, $fecha2) {
        $criteria = new CDbCriteria();
        $criteria->select = 'Telemercaderista, COUNT(*) as Contador';
        $criteria->addBetweenCondition('Fecha_creacion', $fecha, $fecha2);
        $criteria->group = 'Telemercaderista';
        $criteria->order = 'Contador DESC';
        return Contacto::model()->findAll($criteria);
    }

    /**
     * 
     * @param type $fecha
     * @param type $fecha2
     */
    public function registroAsesoresContactos($fecha, $fecha2) {
        $matriz = array();

        $contactos = Yii::app()->db->createCommand()
                ->select('r.ID_Regional as Ciudad, c.Telemercaderista as Descripcion, COUNT(*) as Total')
                ->from('Contacto c, regional_ciudad r')
                ->where('c.Fecha_creacion BETWEEN :fecha AND :fecha2 AND c.Ciudad = r.ID_Ciudad', array(':fecha' => $fecha, ':fecha2' => $fecha2))
                ->group('r.ID_Regional, c.Telemercaderista')
                ->order('r.ID_Regional ASC')
                ->queryAll();

        foreach ($contactos as $c):
            $matriz[$c['Ciudad']][$c['Descripcion']] = $c['Total'];
        endforeach;

        return $matriz;
    }

    /**
     * Retorna los Registros de los Contactos en la Fecha Asignada por Tipos de Contacto
     * @param type $fecha
     * @param type $fecha2
     * @return type
     */
    public function temporalidadDiariaContacto($fecha) {
        $criteria = new CDbCriteria();
        $criteria->select = 'Tipo_contacto, COUNT(*) as Contador';
        $criteria->condition = 'Fecha_creacion = "' . $fecha . '"';
        $criteria->group = 'Tipo_contacto';
        $criteria->order = 'Contador DESC';
        return Contacto::model()->findAll($criteria);
    }

    public function campaingDigitalMedio($fechainicial, $fechafinal, $medios, $regional = '') {
        $listmed = new CDbCriteria();
        $listmed->select = 'COUNT(*) as Contador, Medio_contacto, Fecha_creacion';
        $listmed->addBetweenCondition("Fecha_creacion", $fechainicial, $fechafinal);
        $listmed->addInCondition('Medio_contacto', $medios);
        $listmed->addInCondition('Tipo_contacto', array(3, 5));
        if (!empty($regional)) :
            $listmed->addInCondition('Ciudad', $regional);
        endif;
        $listmed->group = 'Medio_contacto, Fecha_creacion';
        $listmed->order = 'Medio_contacto ASC, Fecha_creacion ASC';
        return Contacto::model()->findAll($listmed);
    }

    public function campaingDigitalServicio($fechainicial, $fechafinal, $servicios, $regional = '') {
        $listser = new CDbCriteria();
        $listser->select = 'COUNT(*) as Contador, Servicio, Fecha_creacion';
        $listser->addBetweenCondition("Fecha_creacion", $fechainicial, $fechafinal);
        $listser->addInCondition('Servicio', $servicios);
        $listser->addInCondition('Tipo_contacto', array(3, 5));
        if (!empty($regional)) :
            $listser->addInCondition('Ciudad', $regional);
        endif;
        $listser->group = 'Servicio, Fecha_creacion';
        $listser->order = 'Servicio ASC, Fecha_creacion ASC';
        return Contacto::model()->findAll($listser);
    }

    public function campaingDigitalTipo($fechainicial, $fechafinal, $tipocontacto, $regional = '') {
        $listtip = new CDbCriteria();
        $listtip->select = 'COUNT(*) as Contador, Tipo_contacto, Fecha_creacion';
        $listtip->addBetweenCondition("Fecha_creacion", $fechainicial, $fechafinal);
        $listtip->addInCondition('Tipo_contacto', $tipocontacto);
        if (!empty($regional)) :
            $listtip->addInCondition('Ciudad', $regional);
        endif;
        $listtip->group = 'Tipo_contacto, Fecha_creacion';
        $listtip->order = 'Tipo_contacto DESC, Fecha_creacion ASC';
        return Contacto::model()->findAll($listtip);
    }

    public function campaingDigitalLlgo($fecha, $regional = '') {
        $listtip = new CDbCriteria();
        $listtip->select = 'COUNT(*) as Contador, Fecha_creacion';
        $listtip->addCondition("Fecha_creacion = '" . $fecha . "' AND Tipo_contacto = 1 AND Medio_contacto = 'GOOGLE'");
        if (!empty($regional)) :
            $listtip->addInCondition('Ciudad', $regional);
        endif;
        return Contacto::model()->findAll($listtip);
    }

    public function campaingDigitalEstado($fechainicial, $fechafinal, $estadoproceso, $tipo, $regional = '') {
        $listtip = new CDbCriteria();
        $listtip->select = 'COUNT(*) as Contador, "' . $tipo . '" as Estado, Fecha_creacion';
        $listtip->addBetweenCondition("Fecha_creacion", $fechainicial, $fechafinal);
        $listtip->addInCondition('Estado_proceso', $estadoproceso);
        $listtip->addInCondition('Tipo_contacto', array(1, 3, 5));
        if (!empty($regional)) :
            $listtip->addInCondition('Ciudad', $regional);
        endif;
        $listtip->group = 'Fecha_creacion';
        $listtip->order = 'Fecha_creacion ASC';
        return Contacto::model()->findAll($listtip);
    }

    public function detalladoTipo($primerdia, $tipo, $temporalidad, $ultimodia) {
        $criteria = new CDbCriteria();

        $criteria->select = 'Medio_contacto, COUNT(*) as Contador';
        $criteria->group = 'Medio_contacto';

        if ($temporalidad == 'semanal') :
            $criteria->addCondition('Fecha_creacion ="' . $primerdia . '"');
        else :
            $criteria->addBetweenCondition('Fecha_creacion', $primerdia, $ultimodia);
        endif;

        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        $criteria->order = 'Contador DESC';

        return Contacto::model()->findAll($criteria);
    }

    public function detalladoEstapa($primerdia, $ultimodia, $estado, $idtipo) {
        $criteria = new CDbCriteria();
        $criteria->select = 'Telemercaderista, COUNT(*) as Contador';
        if ($idtipo > 0) :
            $criteria->addCondition('Tipo_contacto = ' . $idtipo);
        endif;
        $criteria->addBetweenCondition('Fecha_creacion', $primerdia, $ultimodia);
        $criteria->addCondition('Estado_proceso = ' . $estado);
        $criteria->group = 'Telemercaderista';
        $criteria->order = 'Contador DESC';
        return Contacto::model()->findAll($criteria);
    }

    public function detalladoAsesor($asesor, $primerdia, $ultimodia) {
        $criteria = new CDbCriteria();
        $criteria->select = 'Tipo_contacto, count(*) as Contador';
        $criteria->addBetweenCondition('Fecha_creacion', $primerdia, $ultimodia);
        $criteria->addCondition('Telemercaderista = ' . $asesor);
        $criteria->group = 'Tipo_contacto';
        $criteria->order = 'Contador DESC';
        return Contacto::model()->findAll($criteria);
    }

    public function detalladoAsesortipo($asesor, $tipo, $primerdia, $ultimodia) {
        $criteria = new CDbCriteria();
        $criteria->select = 'Asesor, count(*) as Contador';
        $criteria->addBetweenCondition('Fecha_creacion', $primerdia, $ultimodia);
        $criteria->addCondition('Telemercaderista = ' . $asesor . ' AND Tipo_contacto = ' . $tipo);
        $criteria->group = 'Asesor';
        $criteria->order = 'Contador DESC';
        return Contacto::model()->findAll($criteria);
    }

    public function getMedioInfo($primerDia, $ultimoDia, $tipoContacto, $regional = '') {

        $criteria = new CDbCriteria();

        if ($tipoContacto == 3) :
            $criteria->select = 'COUNT(*) as Contador, Medio_contacto, Fecha_creacion';
            $criteria->addCondition('Medio_contacto != "" AND Tipo_contacto = ' . $tipoContacto);
            $criteria->addBetweenCondition('Fecha_creacion', $primerDia, $ultimoDia);
            $criteria->group = 'Medio_contacto, Fecha_creacion';
            $criteria->order = 'Medio_contacto ASC, Fecha_creacion ASC';
        else :
            $criteria->select = 'Fecha_creacion, COUNT(*) as Contador';
            $criteria->addCondition('Tipo_contacto = ' . $tipoContacto);
            $criteria->addBetweenCondition('Fecha_creacion', $primerDia, $ultimoDia);
            $criteria->group = 'Fecha_creacion';
            $criteria->order = 'Fecha_creacion ASC';
        endif;

        if (!empty($regional)) :
            $criteria->addCondition('Ciudad = ' . $regional);
        endif;

        return Contacto::model()->findAll($criteria);
    }

    public function getEstadosInfo($primerDia, $ultimoDia, $tipoContacto, $regional = '') {

        $criteria = new CDbCriteria();

        if ($tipoContacto == 3) :
            $criteria->select = 'COUNT(*) as Contador, Medio_contacto, Fecha_creacion, Id_Estadoweb, TipoDescartado';
            $criteria->addCondition('Medio_contacto != "" AND Tipo_contacto = ' . $tipoContacto);
            $criteria->addBetweenCondition('Fecha_creacion', $primerDia, $ultimoDia);
            $criteria->group = 'Medio_contacto, Id_Estadoweb';
            $criteria->order = 'Medio_contacto ASC, Fecha_creacion ASC';
        else :
            $criteria->select = 'Id_Estadoweb, Fecha_creacion, COUNT(*) as Contador, TipoDescartado';
            $criteria->addCondition('Tipo_contacto = ' . $tipoContacto);
            $criteria->addBetweenCondition('Fecha_creacion', $primerDia, $ultimoDia);
            $criteria->group = 'Fecha_creacion';
            $criteria->order = 'Fecha_creacion ASC';
        endif;

        if (!empty($regional)) :
            $criteria->addCondition('Ciudad = ' . $regional);
        endif;

        return Contacto::model()->findAll($criteria);
    }

    /* Por Fecha */

    public function getDetalleinforme($medio, $fecha, $tipo) {
        $criteria = new CDbCriteria();
        if ($tipo == 3) :
            $criteria->addCondition('Medio_contacto = "' . $medio . '"');
        endif;
        $criteria->addCondition('Fecha_creacion = "' . $fecha . '"');
        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        return Contacto::model()->findAll($criteria);
    }

    /* Por Estado */

    public function getDetalleestado($medio, $fecha1, $fecha2, $tipo, $estado) {
        $criteria = new CDbCriteria();
        if ($tipo == 3) :
            $criteria->addCondition('Medio_contacto = "' . $medio . '"');
        endif;
        $criteria->addInCondition('ID_Estadoweb', json_decode($estado));
        $criteria->addBetweenCondition('Fecha_creacion', $fecha1, $fecha2);
        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        return Contacto::model()->findAll($criteria);
    }

    public function ultReg($primerdia, $ultimodia, $tipo) {
        /* Ultimos 5 Leads */
        $criteria = new CDbCriteria();
        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        $criteria->addBetweenCondition('Fecha_creacion', $primerdia, $ultimodia);
        $criteria->order = 'ID DESC';
        $criteria->limit = 5;
        return Contacto::model()->findAll($criteria);
    }

    function getFechas($inicio, $fin, $intervalo) {
        $_inicio = new DateTime($inicio);
        $_intervalo = new DateInterval($intervalo);
        $_fin = new DateTime($fin);
        $_fin->modify('+1 day');


        $fechas = new DatePeriod($_inicio, $_intervalo, $_fin);
        $ret = array();

        foreach ($fechas as $fecha) {
            $ret[] = $fecha->format('Y-m-d');
        }

        return $ret;
    }

    function _data_last_month_day() {
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));
        return date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
    }

    /** Actual month first day * */
    function _data_first_month_day() {
        $month = date('m');
        $year = date('Y');
        return date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
    }

    function totalCampania($dia, $tipo, $medio = array(), $ciudad) {
        $criteria = new CDbCriteria();
        
        if ($medio != NULL) :
            if (sizeof($medio) > 0) :
                $criteria->addInCondition('Medio_contacto', $medio);
            endif;
        endif;

        if (sizeof($ciudad) > 0) :
            if ($ciudad[0] == '11001') :
                $ciu = array('5001', '68001', '76001');
                $criteria->addNotInCondition('Ciudad', $ciu);
            else :
                $criteria->addInCondition('Ciudad', $ciudad);
            endif;
        endif;
        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        $criteria->addCondition('Fecha_creacion = "' . $dia . '"');
        $total = Contacto::model()->count($criteria);

        if ($total > 0) :
            return (int) $total;
        else :
            return 0;
        endif;
    }

    /**
     * 
     * @param type $fechainicial
     * @param type $fechafinal
     * @param type $tipo
     * @param type $estados
     * @return type
     */
    public function getConTipFec($fechainicial, $fechafinal, $tipo, $estado, $ciudad, $medios) {
        $criteria = new CDbCriteria();

        if (sizeof($ciudad) > 0) :
            if ($ciudad[0] == '11001') :
                $ciu = array('5001', '68001', '76001');
                $criteria->addNotInCondition('Ciudad', $ciu);
            else :
                $criteria->addInCondition('Ciudad', $ciudad);
            endif;
        endif;

        if (!empty($medios)) :
            $criteria->addInCondition('Medio_contacto', $medios);
        endif;

        if (!empty($estado)) :
            $criteria->addInCondition('ID_Estadoweb', $estado);
        endif;

        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        $criteria->addBetweenCondition('Fecha_creacion', $fechainicial, $fechafinal);
        $total = Contacto::model()->count($criteria);

        if ($total > 0) :
            return (int) $total;
        else :
            return 0;
        endif;
    }

    /**
     * 
     * @param type $fechainicial
     * @param type $fechafinal
     * @param type $tipo
     * @param type $estados
     * @return type
     */
    public function getDescartados($fechainicial, $fechafinal, $ciudad) {
        $criteria = new CDbCriteria();
        $criteria->select = 'Id_Estadoweb, COUNT(*) as Contador';
        if (sizeof($ciudad) > 0) :
            if ($ciudad[0] == '11001') :
                $ciu = array('5001', '68001', '76001');
                $criteria->addNotInCondition('Ciudad', $ciu);
            else :
                $criteria->addInCondition('Ciudad', $ciudad);
            endif;
        endif;
        $criteria->addInCondition('Id_Estadoweb', array(2, 3, 4, 6, 7, 15, 16, 17, 18, 20));
        $criteria->addBetweenCondition('Fecha_creacion', $fechainicial, $fechafinal);
        $criteria->group = 'Id_Estadoweb';
        return Contacto::model()->findAll($criteria);
    }

    function estadosRegistrosLlamada($medio, $tipo, $fechainicial, $fechafinal, $ciudad, $estado) {
        $criteria = new CDbCriteria();
        if (sizeof($medio) > 0) :
            $criteria->addInCondition('Medio_contacto', $medio);
        endif;
        if (sizeof($ciudad) > 0) :
            if ($ciudad[0] == '11001') :
                $ciu = array('5001', '68001', '76001');
                $criteria->addNotInCondition('Ciudad', $ciu);
            else :
                $criteria->addInCondition('Ciudad', $ciudad);
            endif;
        endif;
        $criteria->addInCondition('ID_Estadoweb', $estado);
        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        $criteria->addBetweenCondition('Fecha_creacion', $fechainicial, $fechafinal);
        $total = Contacto::model()->count($criteria);

        if ($total > 0) :
            return (int) $total;
        else :
            return 0;
        endif;
    }

    //statusTipos
    //statusTiposAsignado
    //statusTiposAsignadoComercial
    //estadosRegistrosLlamada
    //
    //estadoRegistroDetallado
    //EstadoRegistroTelemercadeo
    function estadoRegistroDetallado($tipo, $fechainicial, $fechafinal, $ciudad, $estado = array(), $medios = array(), $telemercadeo = '') {
        $criteria = new CDbCriteria();
        if (sizeof($ciudad) > 0) :
            if ($ciudad[0] == '11001') :
                $ciu = array('5001', '68001', '76001');
                $criteria->addNotInCondition('Ciudad', $ciu);
            else :
                $criteria->addInCondition('Ciudad', $ciudad);
            endif;
        endif;

        if (!empty($medios) && !in_array('all', $medios)) :
            $criteria->addInCondition('Medio_contacto', $medios);
        endif;

        if (!empty($telemercadeo)) :
            $criteria->addCondition('Telemercaderista = ' . $telemercadeo);
        endif;

        if (!empty($estado)) :
            $criteria->addInCondition('ID_Estadoweb', $estado);
        endif;

        $criteria->addCondition('Tipo_contacto = ' . $tipo);
        $criteria->addBetweenCondition('Fecha_creacion', $fechainicial, $fechafinal);

        $total = Contacto::model()->count($criteria);

        if ($total > 0) :
            return (int) $total;
        else :
            return 0;
        endif;
    }

}
