<?php

/**
 * This is the model class for table "Historico_gestion".
 *
 * The followings are the available columns in table 'Historico_gestion':
 * @property double $ID
 * @property integer $ID_Contacto
 * @property integer $ID_Seguimiento
 * @property integer $ID_Asesor
 * @property integer $ID_Accion
 * @property string $Fecha
 * @property string $Hora
 * @property string $Memo
 * @property integer $ID_Comercial
 * @property integer $Puntualidad
 * @property integer $Veracidad
 *
 * The followings are the available model relations:
 * @property Contacto $iDContacto
 * @property Asesor $iDAsesor
 * @property AccionGestion $iDAccion
 * @property EstadoVenta $iDSeguimiento
 */
class HistoricoGestion extends CActiveRecord {
    public $Contador;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Historico_gestion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ID_Contacto, ID_Seguimiento, ID_Asesor, ID_Accion, Fecha, Hora, Memo', 'required'),
            array('ID_Contacto, ID_Seguimiento, ID_Asesor, ID_Accion, ID_Comercial, Puntualidad, Veracidad', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, ID_Contacto, ID_Seguimiento, ID_Asesor, ID_Accion, Fecha, Hora, Memo, ID_Comercial, Puntualidad, Veracidad', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDContacto' => array(self::BELONGS_TO, 'Contacto', 'ID_Contacto'),
            'iDAsesor' => array(self::BELONGS_TO, 'Asesor', 'ID_Asesor'),
            'iDAccion' => array(self::BELONGS_TO, 'AccionGestion', 'ID_Accion'),
            'iDSeguimiento' => array(self::BELONGS_TO, 'EstadoVenta', 'ID_Seguimiento'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'ID_Contacto' => 'Id Contacto',
            'ID_Seguimiento' => 'Id Seguimiento',
            'ID_Asesor' => 'Id Asesor',
            'ID_Accion' => 'Id Accion',
            'Fecha' => 'Fecha',
            'Hora' => 'Hora',
            'Memo' => 'Memo',
            'ID_Comercial' => 'Id Comercial',
            'Puntualidad' => 'Puntualidad',
            'Veracidad' => 'Veracidad',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('ID_Contacto', $this->ID_Contacto);
        $criteria->compare('ID_Seguimiento', $this->ID_Seguimiento);
        $criteria->compare('ID_Asesor', $this->ID_Asesor);
        $criteria->compare('ID_Accion', $this->ID_Accion);
        $criteria->compare('Fecha', $this->Fecha, true);
        $criteria->compare('Hora', $this->Hora, true);
        $criteria->compare('Memo', $this->Memo, true);
        $criteria->compare('ID_Comercial', $this->ID_Comercial);
        $criteria->compare('Puntualidad', $this->Puntualidad);
        $criteria->compare('Veracidad', $this->Veracidad);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return HistoricoGestion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function detalladoGestiones($primerdia, $ultimodia, $asesor) {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('Fecha', $primerdia, $ultimodia);
        $criteria->addCondition('ID_Asesor = ' . $asesor);
        $criteria->group = 'ID_Contacto';
        $criteria->order = 'Fecha DESC';
        return HistoricoGestion::model()->findAll($criteria);
    }
}