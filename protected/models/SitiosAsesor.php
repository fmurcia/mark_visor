<?php

/**
 * This is the model class for table "sitios_asesor".
 *
 * The followings are the available columns in table 'sitios_asesor':
 * @property integer $ID
 * @property integer $ID_Asesor
 * @property integer $ID_Sitio
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Sitios $iDSitio
 * @property Asesor $iDAsesor
 */
class SitiosAsesor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sitios_asesor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_Asesor, ID_Sitio', 'required'),
			array('ID_Asesor, ID_Sitio, Estado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, ID_Asesor, ID_Sitio, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDSitio' => array(self::BELONGS_TO, 'Sitios', 'ID_Sitio'),
			'iDAsesor' => array(self::BELONGS_TO, 'Asesor', 'ID_Asesor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'ID_Asesor' => 'Id Asesor',
			'ID_Sitio' => 'Id Sitio',
			'Estado' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('ID_Asesor',$this->ID_Asesor);
		$criteria->compare('ID_Sitio',$this->ID_Sitio);
		$criteria->compare('Estado',$this->Estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SitiosAsesor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
