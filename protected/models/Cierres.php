<?php

/**
 * This is the model class for table "Cierres".
 *
 * The followings are the available columns in table 'Cierres':
 * @property integer $ID
 * @property string $Fecha_inicial
 * @property string $Hora_inicial
 * @property string $Fecha_final
 * @property string $Hora_final
 * @property string $Mes
 * @property string $Año
 * @property integer $Estado
 */
class Cierres extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Cierres';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Estado', 'numerical', 'integerOnly' => true),
            array('Mes, Año', 'length', 'max' => 45),
            array('Fecha_inicial, Hora_inicial, Fecha_final, Hora_final', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, Fecha_inicial, Hora_inicial, Fecha_final, Hora_final, Mes, Año, Estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'Fecha_inicial' => 'Fecha Inicial',
            'Hora_inicial' => 'Hora Inicial',
            'Fecha_final' => 'Fecha Final',
            'Hora_final' => 'Hora Final',
            'Mes' => 'Mes',
            'Año' => 'Año',
            'Estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('Fecha_inicial', $this->Fecha_inicial, true);
        $criteria->compare('Hora_inicial', $this->Hora_inicial, true);
        $criteria->compare('Fecha_final', $this->Fecha_final, true);
        $criteria->compare('Hora_final', $this->Hora_final, true);
        $criteria->compare('Mes', $this->Mes, true);
        $criteria->compare('Año', $this->Año, true);
        $criteria->compare('Estado', $this->Estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cierres the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    function check_in_range($start_date, $end_date, $evaluame) {
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($evaluame);
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    function getCierre($origen, $nombre = false) {
        $criterio = new CDbCriteria();
        if (is_array($origen)) {
            if (isset($origen["ID"])) {
                $criterio->addCondition("ID='" . $origen["ID"] . "'");
            }
        } else {
            $criterio->addCondition("Fecha_inicial<='" . $origen . "'");
            $criterio->addCondition("Fecha_final>='" . $origen . "'");
        }
        $cierre = Cierres::model()->find($criterio);
        if (!$nombre) {
            if ($cierre != null) {
                return $cierre->ID;
            }
            return 0;
        } else {
            if ($cierre != null) {
                return substr($cierre->Mes, 0, 3) . " de " . $cierre->Año;
            }
            return "";
        }
    }

    function getCierres() {
        return CHtml::listData(Cierres::model()->findAll(), "ID", function($data) {
                    return $data->Mes . " de " . $data->Año . "";
                }, "Año");
    }
}
