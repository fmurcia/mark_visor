<?php

require_once dirname(__FILE__) . "/Slim.php";
Yii::registerAutoloader("SLIM_autoload");

class yiislim extends CApplicationComponent {

    public $slim;

    public function init() {
        if ($this->slim === null)
            $slim = new Slim\Slim();
            $this->slim = $slim->getInstance();
        return $this->slim;
    }
}