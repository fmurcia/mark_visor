<?php

$ttlvenreg = $this->unidadNegocioAgencia('Venta');

print_r($ttlvenreg);

$criteria = new CDbCriteria();
$criteria->addCondition('Tipo = 1');
$criteria->order = 'ID ASC';
$servicios = Servicios::model()->findAll($criteria);

$ultcli = 0;
$arr_view = array();
$totalven = 0;

foreach ($ttlvenreg as $r) :
    if ($r->AgenciaCliente > 0) :
        $arr_view[$r->AgenciaCliente]['Regional'] = $r->Regional;
        $arr_view[$r->AgenciaCliente]['Agencia'] = strtoupper($r->Agencia);
        if ($r->AgenciaCliente == $ultcli) :
            foreach ($servicios as $s) :
                if ($s->ID == $r->CodigoServicioDetalle) :
                    $arr_view[$r->AgenciaCliente][$s->ID] += $r->Total;
                    $totalven += $r->Total;
                endif;
            endforeach;
        else :
            foreach ($servicios as $s) :
                if ($s->ID == $r->CodigoServicioDetalle) :
                    $arr_view[$r->AgenciaCliente][$s->ID] = $r->Total;
                    $totalven = $r->Total;
                else :
                    $arr_view[$r->AgenciaCliente][$s->ID] = 0;
                endif;
            endforeach;
        endif;

        $arr_view[$r->AgenciaCliente]['Total'] = $totalven;
        $ultcli = $r->AgenciaCliente;
    endif;
endforeach;

$totalgeneral = 0;
?>
<div class="row">  
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>MOVIMIENTO AGENCIAS <small>Actividad General</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="penvesagns">
                <table class="table editabv">
                    <thead>
                        <tr>
                            <th>REGIONAL</th>
                            <th>AGENCIA</th>
                            <?php foreach ($servicios as $s) : ?>
                                <th><?= strtoupper($s->Nombre) ?></th>
                            <?php endforeach; ?>
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($arr_view as $ar => $key) : ?>   
                            <tr>
                                <td><?= strtoupper($key['Regional']) ?></td>
                                <td><?= strtoupper($key['Agencia']) ?></td>
                                <?php foreach ($servicios as $s) : ?>
                                    <td>$<?= number_format($key[$s->ID]) ?></td>
                                    <?php
                                    $total[$s->ID][] = $key[$s->ID];
                                endforeach;
                                ?>
                                <td>$<?= number_format($key['Total']) ?></td>
                            </tr>
                            <?php
                            $totalgeneral += $key['Total'];
                        endforeach;
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td style="text-align: center">TOTALES : </td>
                            <?php
                            foreach ($servicios as $s) :
                                if (isset($total[$s->ID])) :
                                    ?>
                                    <td>$<?= number_format(array_sum($total[$s->ID])) ?></td>
                                    <?php
                                endif;
                            endforeach;
                            ?>
                            <td>$<?= number_format($totalgeneral) ?></td>
                        </tr>
                    </tfoot>
                </table> 
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12" id="Ventasagnd"></div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>