<div class="col-12">
    <br>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close"  style="color: black"></i></span>
    </button>
</div>
<div class="modal-header">
    <b><h4 class="modal-title"><?= $texto ?></h4></b>
</div>
<div class="modal-body">
    <div class="esquina_superior_derecha_simple_comercial"><h1><?= $ttcon ?></h1></div>
    <table class="table table-striped">
        <tr>
            <?php
            $total = $ttcon;
            ?>
            <th style="font-size: 20pt; text-align:center" colspan="3"><?= $total ?></th>  
        </tr>
        <tr>
            <th style="text-align: center">DESCRIPCION</th>
            <th style="text-align: center" colspan="2">CANTIDAD</th>
        </tr>
        <?php
        $arr_grafica = array();
        $i = 0;

        $arr_data = array();
        foreach ($arr_estado as $es) :
            $et = EstadosWeb::model()->findByPk($es);
            $arr_grafica[$i]['name'] = $et->Descripcion;
            $arr_grafica[$i]['y'] = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, array($es), $leads);

            $arr_data[$i]['nombre'] = $et->Descripcion;
            $arr_data[$i]['orden'] = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, array($es), $leads);
            $arr_data[$i]['promedio'] = number_format((Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, array($es), $leads) / (($total > 0) ? $total : 1) ) * 100);
            $i++;
        endforeach;

        uasort($arr_data, 'sort_by_orden');

        foreach ($arr_data as $es) :
            ?>
            <tr>
                <td><?= $es['nombre'] ?></td>
                <th style="text-align: right"><?= $es['orden'] ?></th>
                <th style="text-align: right"><?= $es['promedio'] ?>%</th>
            </tr>   
            <?php
        endforeach;
        ?>
    </table>

    <div id="container_detalle" style="width: 550px; height: 300px"></div>

    <script>
        // Build the chart
        Highcharts.chart('container_detalle', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Reporte'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        },
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                    name: 'Items',
                    data: <?= json_encode($arr_grafica) ?>
                }]
        });
    </script>
    <div class="clearfix"></div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
</div>

<?php

function sort_by_orden($a, $b) {
    return $b['orden'] - $a['orden'];
}
?>