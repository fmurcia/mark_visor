function comparacion(obj) {
    if (obj) {
        $('.comparacion').fadeIn('fast');
    } else {
        $('.comparacion').fadeOut('fast');
    }
}

function subcampanas(item) {
    $('.subcampania').fadeOut('fast');
    if (item == 'Facebook') {
        $('.subcampania').html('<select class="form-control" onchange="subcampana(this.value)" id="subcampania" name="subcampania"><option value="all">..Todas..</option><option value="facebook">Facebook_Pago</option><option value="facebooks">Facebook_Seo</option><option value="instagramp">instagram_pago</option><option value="instagrams">instagram_seo</option></select>');
    }
    if (item == 'Adwords') {
        $('.subcampania').html('<select class="form-control" onchange="subcampana(this.value)" id="subcampania" name="subcampania"><option value="all">..Todas..</option><option value="Adwords">adwords_busqueda</option><option value="Display">adwords_display</option><option value="video">adwords_video</option></select>');
    }
    if (item == 'registroseo') {
        $('.subcampania').html('<select class="form-control" onchange="subcampana(this.value)" id="subcampania" name="subcampania"><option value="all">..Todas..</option><option value="registroseo">seo</option></select>');
    }
    $('.subcampania').fadeIn('fast');
    /*PROCESO*/
    renderizada();
}

function subcampana(item) {
    /*PROCESO*/
    console.log(item); 
    renderizada();
}


function regional() {
    /*PROCESO*/
    renderizada();
}

function renderizada() {
    $("#modalAlerta").modal('show');
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('llamada/qryrender')?>",
        data: {
            regional: $('#regional').val(),
            campania: $('#campania').val(),
            subcampania: $('#subcampania').val(),
            fecha_rango1: $('#rango_fecha1').val(),
            fecha_rango2: $('#rango_fecha2').val(),
            fecha_rango3: $('#rango_fecha3').val(),
            fecha_rango4: $('#rango_fecha4').val(),
            comparacion: $('#toggle-event').prop('checked')
        },
        beforeSend: function () {
            $('.contenidofiltrado').fadeOut('fast');
            $('#modalwait').fadeIn('fast');
        },
        success: function (data) {
            //contadores();
            $("#modalAlerta").modal('hide');
            $('.contenidofiltrado').html(data);
            $('.contenidofiltrado').fadeIn('fast');
        }
    });
}

function detalleEstado(texto){
    $("#modalDetallado").modal('show');
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('llamada/detallado')?>",
        data : {
            texto : texto,
            tipo : $('#tipocontacto').val(),
            totalregistrado : $('#totalregistrado').val()
        },
        beforeSend: function () {
            $('#modalwaitdetallado').fadeIn('fast');
        },
        success: function (respuesta) {
            $("#modalwaitdetallado").fadeOut('fast');
            $("#modalcuerpodetallado").html(respuesta);
        }
    });
}

function detalleEstadoComercial(texto){
    $("#modalDetallado").modal('show');
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('llamada/detcomercial')?>",
        data : {
            texto : texto,
            tipo : $('#tipocontacto').val(),
            totalregistrado : $('#totalregistrado').val()
        },
        beforeSend: function () {
            $('#modalwaitdetallado').fadeIn('fast');
        },
        success: function (respuesta) {
            $("#modalwaitdetallado").fadeOut('fast');
            $("#modalcuerpodetallado").html(respuesta);
        }
    });
}