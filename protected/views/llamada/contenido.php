<div class="row">
    <div class="col-xl-12 col-md-12 mb-12"> 
        <div class="card border-left-danger shadow h-100">
            <table class="table autoajuste">
                <tr>
                    <td class="mdaltwo mr-2 clsPend" data-id="1">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-connectdevelop fa-2x"></i><br>Leads Pendientes</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['nuevos'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['nuevos2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsDupl mr-2" data-id="2">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-files-o fa-2x"></i><br>Duplicados</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['duplicados'] ?></div>
                    </td>
                    <td class="mdaltwo clsErra mr-2" data-id="3">
                        <br>    
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-times fa-2x"></i><br>Errados</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['errados'] ?></div>
                    </td>
                    <td class="mdaltwo clsDesc mr-2" data-id="4">    
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-trash fa-2x"></i><br>Descartados</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['descartados'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['descartados2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsSegu mr-2" data-id="5">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-edit fa-2x"></i><br>En Gestion</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['seguimiento'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['seguimiento2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsCita mr-2" data-id="6">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-calendar fa-2x"></i><br>Citas</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['cita'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['cita2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsDesd mr-2" data-id="7">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-times-circle fa-2x"></i><br>Descartados DC</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['descartadosdc'] ?></div>                
                    </td>
                    <td class="mdaltwo clsCoti mr-2" data-id="8">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-dollar fa-2x"></i><br>Cotizaciones</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['cotizados'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['cotizados2'] ?></div>
                    </td>
                    <td class="mdaltwo clsVent mr-2" data-id="9">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-users fa-2x"></i><br>Ventas</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['venta'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['venta2'] ?> </label></div>
                    </td>                    
                    <td class="mr-2" style="font-size: 70pt">
                        <?= $totalregistrado ?><label style="display:<?= $oculto ?>"> / <?= $totalregistrado2 ?></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <?php
    if ($comparacion == 'true') :
        $div = 'col-xl-6 col-md-5 mb-6';
    else :
        $div = 'col-xl-12 col-md-12 mb-12';
    endif;
    ?>
    <div class="col-xl-12 col-md-12 mb-12">
        <div class="<?= $div ?> card border-left-danger shadow h-100 py-2" id="container"  style="min-width: 310px; height: 330px; margin: 0 auto;"></div>
        <div class="<?= $div ?> card border-left-danger shadow h-100 py-2" id="container2"  style="min-width: 310px; height: 330px; margin: 0 auto; background: #FF8316; display: <?= $graficaoculta ?>"></div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
        <div class="card border-left-danger shadow h-100 py-2">
            <table class="table">
                <tr class="">
                    <th colspan="7" style="text-align:center">INFORME DE CAMPAÑAS</th>
                </tr>
                <tr class="info">
                    <th colspan="2" style="text-align:center">ESTADO</th><th style="text-align: center" colspan="20"><?= $primerdia . ' / ' . $segundodia ?><label style="display:<?= $oculto ?>"> / <?= $tercerdia . ' / ' . $cuartodia ?> </label></th>
                </tr>
                <tr class="">
                    <th>CAMPAÑA</th>
                    <th colspan="2" style="text-align:center; background: #9dd4d0">DIGITAL</th>
                    <th colspan="2" style="text-align:center; background: #d36b7c">TRADICIONAL</th>
                    <th colspan="2" style="text-align:center; background: #f3f1c0">TOTAL</th>
                </tr>
                <tr class="">
                    <th>TOTAL REGISTROS</th>
                    <td class="h1" style="text-align:center; background: #9dd4d0"><?= $totaldigital ?><label style="display:<?= $oculto ?>"> / <?= $totaldigital2 ?></label></td>
                    <td class="h1" style="text-align:center; background: #9dd4d0"><?= number_format($totaldigital / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format($totaldigital2 / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                    <td class="h1" style="text-align:center; background: #d36b7c"><?= $totaltradicional ?><label style="display:<?= $oculto ?>"> / <?= $totaltradicional2 ?></label></td>
                    <td class="h1" style="text-align:center; background: #d36b7c"><?= number_format($totaltradicional / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> /<?= number_format($totaltradicional / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                    <th class="h1" colspan="2" style="text-align:center; background: #f3f1c0"><?= $totalregistrado ?><label style="display:<?= $oculto ?>"> / <?= $totalregistrado2 ?></label></th>
                </tr>

                <tr class="info"><td colspan="22"></td></tr>
                <tr class=""><th colspan="22" style="text-align:center"> TOTAL REGISTRADO </th></tr>
                <?php
                foreach ($leads_general as $l => $ar) :
                    $ttgen = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $ar);
                    $ttgen2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arr_reg, $ar);
                    ?>
                    <tr>
                        <th><?= $l ?></th>
                        <td style="text-align: center; background: #9dd4d0"  class="h4"><button class="btn btn-primary mdal" data-id="<?= $l . '-digital' ?>"><?= Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, $leads_digital) ?><label style="display:<?= $oculto ?>"> / <?= Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $ar, $arr_reg, $leads_digital) ?></label></button></td>
                        <td style="text-align: center; background: #9dd4d0"  class="h4"><?= number_format((Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, $leads_digital) / (($totalregistrado > 0 ) ? $totalregistrado : 1) ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format((Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $ar, $arr_reg, $leads_digital) / (($totalregistrado2 > 0 ) ? $totalregistrado2 : 1) ) * 100) ?>%</label></td>
                        <td style="text-align: center; background: #d36b7c"  class="h4"><button class="btn btn-primary mdal" data-id="<?= $l . '-digital' ?>"><?= Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, $leads_tradicional) ?><label style="display:<?= $oculto ?>"> / <?= Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $ar, $arr_reg, $leads_tradicional) ?></label></button></td>
                        <td style="text-align: center; background: #d36b7c"  class="h4"><?= number_format((Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, $leads_tradicional) / (($totalregistrado > 0 ) ? $totalregistrado : 1) ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format((Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $ar, $arr_reg, $leads_tradicional) / (($totalregistrado2 > 0 ) ? $totalregistrado2 : 1) ) * 100) ?>%</label></td>
                        <td style="text-align: center; background: #f4f2d7"  class="h4"><button class="btn btn-primary mdal" data-id="<?= trim($l) . '-all' ?>"><?= $ttgen ?><label style="display:<?= $oculto ?>"> / <?= $ttgen2 ?></label></button></td>
                        <td style="text-align: center; background: #f4f2d7"  class="h4"><?= number_format($ttgen / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format($ttgen2 / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                    </tr>
                    <?php
                endforeach;
                ?>                
            </table>
        </div>
    </div>
</div>

<br><br><br>

<?php
if ($comparacion == 'true') :
    $width = 700;
else :
    $width = 1680;
endif;
?>

<script>
    Highcharts.chart('container', {
        chart: {
            type: 'line',
            width: <?= $width ?>
        },
        title: {
            text: 'LEADS POR CAMPAÑAS'
        },
        subtitle: {
            text: 'Llamada Entrante'
        },
        xAxis: {
            categories: <?= json_encode($categorias) ?>
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series:
                [{
                        name: 'DIGITAL',
                        data: <?= json_encode($arr_digital) ?>
                    }, {
                        name: 'TRADICIONAL',
                        data: <?= json_encode($arr_tradicional) ?>
                    }
                ]
    });
    Highcharts.chart('container2', {
        chart: {
            type: 'line',
            width: <?= $width ?>
        },
        title: {
            text: 'LEADS POR CAMPAÑAS'
        },
        subtitle: {
            text: 'Llamada Entrante'
        },
        xAxis: {
            categories: <?= json_encode($categorias2) ?>
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series:
                [{
                        name: 'DIGITAL',
                        data: <?= json_encode($arr_digital2) ?>
                    }, {
                        name: 'TRADICIONAL',
                        data: <?= json_encode($arr_tradicional2) ?>
                    }
                ]
    });
</script>