<div class="col-12">
    <br>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close"  style="color: black"></i></span>
    </button>
</div>
<div class="modal-header">
    <b><h4 class="modal-title"><?= $texto ?></h4></b>
</div>
<div class="modal-body">
    <div class="esquina_superior_derecha_simple_comercial"><h1><?= $ttcon ?></h1></div>
    <?= $this->renderPartial('tab3_1', array('texto' => strtoupper($texto), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $medio, 'arr_reg' => $arr_reg, 'sin' => '', 'ttcon' => $ttcon), true); ?>
</div>
<div class="clearfix"></div>

<?php

function sort_by_orden($a, $b) {
    return $b['orden'] - $a['orden'];
}
?>