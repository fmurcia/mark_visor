<?php
$primerdia = Yii::app()->user->getState('primerdia');
$segundodia = Yii::app()->user->getState('segundodia');
$tercerdia = Yii::app()->user->getState('tercerdia');
$cuartodia = Yii::app()->user->getState('cuartodia');
$ciudad = Yii::app()->user->getState('regional');
$totalgeneral = $this->totalTipos(1, $primerdia, $segundodia, $ciudad) +
//        $this->totalTipos(2, $primerdia, $segundodia, $ciudad) + 
        $this->totalTipos(3, $primerdia, $segundodia, $ciudad) +
        $this->totalTipos(5, $primerdia, $segundodia, $ciudad)
?>

<input type="hidden" id="tipocontacto" name="tipocontacto" value="<?= $tipocontacto ?>">
<input type="hidden" id="totalregistrado" name="totalregistrado" value="<?= $totalregistrado ?>">

<div id="page-content-wrapper">
    <div class="page-content inset">
        <center><h1><i class="fa fa-comment"></i> CHAT</h1></center>
        <div class="contenidofiltrado">                       
            <div class="row">
                <div class="col-xl-12 col-md-12 mb-12"> 
                    <div class="card border-left-danger shadow h-100">
                        <table class="table autoajuste">
                            <tr>
                                <td class="mdaltwo mr-2 clsPend" data-id="1">
                                    <br>
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-connectdevelop fa-2x"></i><br>Leads Pendientes</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['nuevos'] ?></div>
                                </td>
                                <td class="mdaltwo clsDupl mr-2" data-id="2">
                                    <br>
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-envelope fa-2x"></i><br>En Mailing</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['duplicados'] ?></div>
                                </td>
                                <td class="mdaltwo clsErra mr-2" data-id="3">
                                    <br>    
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-times fa-2x"></i><br>Errados</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['errados'] ?></div>
                                </td>
                                <td class="mdaltwo clsDesc mr-2" data-id="4">    
                                    <br>
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-trash fa-2x"></i><br>Descartados</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['descartados'] ?></div>
                                </td>
                                <td class="mdaltwo clsSegu mr-2" data-id="5">
                                    <br>
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-edit fa-2x"></i><br>En Gestion</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['seguimiento'] ?></div>
                                </td>
                                <td class="mdaltwo clsCita mr-2" data-id="6">
                                    <br>
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-calendar fa-2x"></i><br>Citas</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['cita'] ?></div>
                                </td>
                                <td class="mdaltwo clsDesd mr-2" data-id="7">
                                    <br>
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-times-circle fa-2x"></i><br>Descartados DC</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['descartadosdc'] ?></div>                
                                </td>
                                <td class="mdaltwo clsCoti mr-2" data-id="8">
                                    <br>
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-dollar fa-2x"></i><br>Cotizaciones</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['cotizados'] ?></div>
                                </td>
                                <td class="mdaltwo clsVent mr-2" data-id="9">
                                    <br>
                                    <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-users fa-2x"></i><br>Ventas</div>
                                    <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['venta'] ?></div>
                                </td>                    
                                <td class="mr-2" style="font-size: 70pt">
                                    <?= $totalregistrado ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-md-12 mb-12">
                    <div class="card border-left-danger shadow h-100 py-2" id="container"  style="min-width: 310px; height: 330px; margin: 0 auto"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-md-12 mb-12">
                    <div class="card border-left-danger shadow h-100 py-2">
                        <table class="table table-striped table-bordered">

                            <tr class="">
                                <th colspan="4" style="text-align:center">INFORME DE CAMPAÑAS</th>
                            </tr>

                            <tr class="info">
                                <th colspan="2" style="text-align:center">ESTADO</th><th style="text-align: center" colspan="20"><?= $primerdia . ' / ' . $segundodia ?></th>
                            </tr>

                            <tr class="">
                                <th  colspan="2">TOTAL REGISTROS</th>
                                <th class="h1" style="text-align:center" colspan="2"><?= $totalregistrado ?></th>
                            </tr>

                            <tr class="info"><td colspan="22"></td></tr>
                            <?php
                            $i = 1;
                            foreach ($leads_general as $l => $ar) :
                                ?>
                                <tr>
                                    <th colspan="2" ><?= $l ?></th>
                                    <td style="text-align: center;" class="h4"><button class="btn btn-primary mdal" data-id="<?= $i . '-all' ?>"><?= Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array()) ?></button></td>
                                    <td style="text-align: center;" class="h4"><?= number_format((Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array()) / (($totalregistrado > 0 ) ? $totalregistrado : 1) ) * 100) ?>%</td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                            ?>
                            <tr class="info"><td colspan="22"></td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br>

        <script>
            Highcharts.chart('container', {
                chart: {
                    type: 'line',
                    width: 1680
                },
                title: {
                    text: 'LEADS POR CAMPAÑA'
                },
                subtitle: {
                    text: 'CHAT'
                },
                xAxis: {
                    categories: <?= json_encode($categorias) ?>
                },
                yAxis: {
                    title: {
                        text: 'Cantidad'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: <?= json_encode($grafica) ?>
            });
        </script>
    </div>      
</div>