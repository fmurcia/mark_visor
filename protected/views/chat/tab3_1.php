<table class="table table-striped">
    <tr>
        <?php
        $total =  $ttcon;
        ?>
        <th style="font-size: 20pt; text-align:center" colspan="3"><?= $total ?></th>    
    </tr>
    <tr>
        <th style="text-align: center">COMERCIAL</th>
        <th style="text-align: center" colspan="2">CANTIDAD</th>
    </tr>
    <?php
    $i = 0;
    $arr_data = array();
    $arr_grafica = array();
    $comercial = Asesor::model()->findAll('Nivel = 3 AND Estado = 1 AND Activo = 1 ');
    foreach ($comercial as $c) :
        $arr_grafica[$i]['name'] = $c->Nombre;
        $arr_grafica[$i]['y'] = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $arr_estado, array(), $c->ID);
        
        $arr_data[$i]['nombre'] = $c->Nombre;
        $arr_data[$i]['orden'] = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $arr_estado, array(), $c->ID);
        $arr_data[$i]['promedio'] = number_format((Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $arr_estado, array(), $c->ID) / (($total > 0) ? $total : 1) )*100);
        $i++;
    endforeach;
    uasort($arr_data, 'sort_by_orden');
        
    foreach ($arr_data as $es) :
        ?>
        <tr>
            <td><?= $es['nombre'] ?></td>
            <th style="text-align: right"><?= $es['orden'] ?></th>
            <th style="text-align: right"><?= $es['promedio'] ?>%</th>
        </tr>
        <?php
    endforeach;
    ?>
</table>



<div id="container_detalle_tab1_1" style="width: 550px; height: 300px"></div>

<script>
    // Build the chart
    Highcharts.chart('container_detalle_tab1_1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Reporte'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
                name: 'Items',
                data: <?= json_encode($arr_grafica) ?>
            }]
    });
</script>