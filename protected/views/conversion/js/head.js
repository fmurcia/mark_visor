function comparacion(obj) {
    if (obj) {
        $('.comparacion').fadeIn('fast');
    } else {
        $('.comparacion').fadeOut('fast');
    }
}

function regional() {
    /*PROCESO*/
    renderizada();
}

function renderizada() {
    $("#modalAlerta").modal('show');
    $.ajax({
        type: "post",
        url: "<?= Yii::app()->createUrl('web/qryrender')?>",
        data: {
            regional: $('#regional').val(),
            campania: $('#campania').val(),
            subcampania: $('#subcampania').val(),
            fecha_rango1: $('#rango_fecha1').val(),
            fecha_rango2: $('#rango_fecha2').val(),
            fecha_rango3: $('#rango_fecha3').val(),
            fecha_rango4: $('#rango_fecha4').val(),
            comparacion: $('#toggle-event').prop('checked')
        },
        beforeSend: function () {
            $('.ocultarcontenidofiltrado').fadeOut('fast');
            $('#modalwait').fadeIn('fast');
        },
        success: function (data) {
            contadores();
            $("#modalAlerta").modal('hide');
            $('.mostrarcontenidofiltrado').html(data);
            $('.mostrarcontenidofiltrado').fadeIn('fast');
        }
    });
}
