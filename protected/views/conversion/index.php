<?php
$primerdia = Yii::app()->user->getState('primerdia');
$segundodia = Yii::app()->user->getState('segundodia');
$tercerdia = Yii::app()->user->getState('tercerdia');
$cuartodia = Yii::app()->user->getState('cuartodia');
$ciudad = Yii::app()->user->getState('regional');
$totalgeneral = $this->totalTipos(1, $primerdia, $segundodia, $ciudad) + $this->totalTipos(2, $primerdia, $segundodia, $ciudad) + $this->totalTipos(3, $primerdia, $segundodia, $ciudad) + $this->totalTipos(5, $primerdia, $segundodia, $ciudad);

$arr_descartados = array();
$descartados = Contacto::model()->getDescartados($primerdia, $segundodia, $ciudad);
$i = 0;

if (sizeof($descartados) > 1 && $descartados != NULL) :
    foreach ($descartados as $d) :
        $estadoWeb = EstadosWeb::model()->findByPk($d->Id_Estadoweb);
        $arr_descartados[$i]['name'] = $estadoWeb->Descripcion;
        $arr_descartados[$i]['y'] = (int) $d->Contador;
        $i++;
    endforeach;
endif;
?>

<div id="page-content-wrapper">
    <div class="page-content inset">
        <div class="row">
            <div class="col-lg-12">
                <div id="etapas_general" style="min-width: 410px; max-width: 600px; height: 400px; margin: 0 auto"></div>
            </div>
            <div class="col-lg-6">
                <div id="tipos_general" style="min-width: 410px; max-width: 600px; height: 400px; margin: 0 auto"></div>
            </div>
            <div class="col-lg-6">
                <div id="descartados_general" style="min-width: 410px; max-width: 600px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>

<?php
$arr_grafica_pie = array();

$arr_grafica_pie[0]['name'] = 'WEB';
$arr_grafica_pie[0]['y'] = (int) number_format(($this->totalTipos(3, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100));
$arr_grafica_pie[1]['name'] = 'LLAMADA';
$arr_grafica_pie[1]['y'] = (int) number_format(($this->totalTipos(1, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100));
$arr_grafica_pie[2]['name'] = 'CHAT';
$arr_grafica_pie[2]['y'] = (int) number_format(($this->totalTipos(5, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100));
$arr_grafica_pie[3]['name'] = 'OPORTUNIDAD';
$arr_grafica_pie[3]['y'] = (int) number_format(($this->totalTipos(2, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100));
?>


<script type="text/javascript">

    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });

    Highcharts.chart('etapas_general', {
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: {
            text: 'Etapas de Contacto',
            x: -50
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b> <br>({point.y:,.0f})',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '1%',
                neckHeight: '1%'
            }
        },
        legend: {
            enabled: false
        },
        series: [{
                name: 'Por',
                data: [
                    ['LEADS PENDIENTES ', <?= $this->getConTipFecGeneral($primerdia, $segundodia, array(1), $ciudad) ?>],
                    ['DESCARTADAS ', <?= $this->getConTipFecGeneral($primerdia, $segundodia, array(2, 3, 4, 6, 7, 15, 16, 17, 18, 20), $ciudad) ?>],
                    ['EN GESTION', <?= $this->getConTipFecGeneral($primerdia, $segundodia, array(5, 19, 24), $ciudad) ?>],
                    ['CITA ', <?= $this->getConTipFecGeneral($primerdia, $segundodia, array(8), $ciudad) ?>],
                    ['COTIZACION ', <?= $this->getConTipFecGeneral($primerdia, $segundodia, array(9, 10, 11, 12), $ciudad) ?>],
                    ['VENTA ', <?= $this->getConTipFecGeneral($primerdia, $segundodia, array(13, 14), $ciudad) ?>]
                ]
            }]
    });

    Highcharts.chart('tipos_general', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Tipos de Contacto'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
        series: [
            {
                "name": "Browsers",
                "colorByPoint": true,
                "data": <?= json_encode($arr_grafica_pie) ?>
            }
        ]
    });


    Highcharts.chart('descartados_general', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Descartados'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Cantidad Total'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.0f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> Total<br/>'
        },

        series: [
            {
                "name": "Items",
                "colorByPoint": true,
                "data": <?= json_encode($arr_descartados) ?>
            }
        ]
    });
</script>