<?php
$primerdia = Yii::app()->user->getState('primerdia');
$segundodia = Yii::app()->user->getState('segundodia');
$tercerdia = Yii::app()->user->getState('tercerdia');
$cuartodia = Yii::app()->user->getState('cuartodia');
$ciudad = Yii::app()->user->getState('regional');
$totalgeneral = 
        $this->totalTipos(1, $primerdia, $segundodia, $ciudad) + 
//        $this->totalTipos(2, $primerdia, $segundodia, $ciudad) + 
        $this->totalTipos(3, $primerdia, $segundodia, $ciudad) + 
        $this->totalTipos(5, $primerdia, $segundodia, $ciudad)
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= CHtml::encode($this->pageTitle); ?></title>
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/animate.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/custom.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/sweetalert.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/jquery.datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/bootstrap-toggle.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/style.css" />
        <link rel="shortcut icon" href="<?= Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />

        <style>
            @import url('https://fonts.googleapis.com/css?family=Exo'); 
        </style>

        <?php
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . "/js/head.js", CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/highcharts.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/highcharts-3d.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/funnel.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . "/js/ready.js", CClientScript::POS_READY);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/sweetalert.min.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/select2.full.min.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/moment/moment.min.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/datepicker/daterangepicker.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/bootstrap-toggle.min.js', CClientScript::POS_HEAD);
        ?>
    </head>
    <body style="font-family: 'Exo', sans-serif;">
        <?php
        $usuario = ucwords(strtolower(Yii::app()->user->getState("nombre_usuario")));
        $this->widget(
                'booster.widgets.TbNavbar', array(
            'type' => 'null',
            'brand' => '<i class="fa fa-signal"></i> Telelego Telesentinel ',
            'brandUrl' => '#',
            'collapse' => true,
            'fixed' => false,
            'fluid' => true,
            'items' => array(
                array(
                    'class' => 'booster.widgets.TbMenu',
                    'encodeLabel' => false,
                    'type' => 'navbar',
                    'items' => array(
//                        array('label' => '<i class="fa fa-comments"></i> Conversiones', 'url' => Yii::app()->createUrl('web/')),
//                        array('label' => '<i class="fa fa-dollar"></i> Ventas', 'url' => Yii::app()->createUrl('ventas/')),
//                        array('label' => '<i class="fa fa-users"></i> Comerciales', 'url' => Yii::app()->createUrl('comerciales/'))
                    ),
                ),
                array(
                    'class' => 'booster.widgets.TbMenu',
                    'encodeLabel' => false,
                    'htmlOptions' => array('class' => 'pull-right'),
                    'type' => 'navbar',
                    'items' => array(
                        array('label' => '<i class="fa fa-cog"></i> ' . $usuario, 'url' => '#', 'items' => array(array('label' => 'Salir', 'url' => Yii::app()->createUrl('site/logout'))))
                    ),
                ),
            )
                )
        );

        $regionales = Contacto::model()->getRegionales();
        ?>
        <div class="row filtrosuperior">
            <div class="col-xl-12 col-md-12 mb-12">
                <table class="table">
                    <tr>
                        <th><i class="fa fa-home"></i> Ciudad </th>
                        <th><select class="form-control" onchange="regional(this.value)" id="regional" name="regional">   
                                <option value="all">..Todas..</option>
                                <?php 
                                $ar_reg = Yii::app()->user->getState('regional');
                                foreach ($regionales as $r) : ?>
                                    <option value="<?= $r->ID_Regional ?>" <?= (isset($ar_reg[0]) && $ar_reg[0] == $r->ID_Regional) ? 'selected' : '' ?> ><?= $r->Descripcion ?></option>
                                <?php endforeach; ?>
                            </select></th>
                        <th><i class="fa fa-globe"></i> Campaña </th>
                        <th><select class="form-control" onchange=" subcampanas(this.value)" id="campania" name="campania">
                                <option value="all">..Todas..</option>
                                <option value="Facebook">Facebook</option>
                                <option value="Adwords">Google</option>
                                <option value="registroseo">Seo</option>
                                <option value="mailchimp">Mailchimp</option>
                            </select></th>
                        <th><div class="col-lg-6 subcampania"></div></th>
                        <th><i class="fa fa-clock-o"></i> Tiempo </h2></th>
                        <th><div style="width: 40px; height: 30px; background: black; position: absolute"></div>
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 4px 20px; border: 1px solid #ccc">
                                <i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;<span></span>&nbsp;<i class="fa fa-caret-down"></i>
                            </div></th>
                        <th class="comparacion" style="display:none"><div style="width: 40px; height: 30px; background: #FF8316; position: absolute"></div>
                            <div id="reportrange2" class="pull-right" style="background: #fff; cursor: pointer; padding: 4px 20px; border: 1px solid #ccc">
                                <i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;<span></span>&nbsp;<i class="fa fa-caret-down"></i>
                            </div>      </th>
                        <th><i class="fa fa-globe"></i> Comparacion <input type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" id="toggle-event" onchange="comparacion($(this).prop('checked'))"></th>
                    </tr>
                </table>
            </div>      
        </div>

        <div class="loadings"></div>

        <input type="hidden" id="range" value="<?= Yii::app()->user->getState('range') ?>"/>
        <input type="hidden" id="range2" value="<?= Yii::app()->user->getState('range2') ?>"/>
        <input type="hidden" id="rango_fecha1" value="<?= $primerdia ?>"/>
        <input type="hidden" id="rango_fecha2" value="<?= $segundodia ?>"/>
        <input type="hidden" id="rango_fecha3" value="<?= $tercerdia ?>"/>
        <input type="hidden" id="rango_fecha4" value="<?= $cuartodia ?>"/>

        <div class="row" style="font-size: 9pt">
            <div class="col-lg-1">
                <div class="contadores">
                    <div class="sidebar-nav">
                        <div class="card btn btn-default" style="background: #F3F4FF">
                            <a id="lnkweb" classs="link" href="#">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">WEB</div>
                                            <div class="mb-0 font-weight-bold text-gray-800"><span class="contweb h1"><?= $this->totalTipos(3, $primerdia, $segundodia, $ciudad) . '</span>  <br> <hr style="border-top: 1px solid red;">  <span class="h3"> ' . number_format(($this->totalTipos(3, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-globe fa-3x text-primary"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card btn btn-default" style="background: #F3F4FF">
                            <a id="lnklla" classs="link" href="#">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">LLAMADA</div>
                                            <div class="mb-0 font-weight-bold text-gray-800"><span class="contlla h1"><?= $this->totalTipos(1, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;">  <span class="h3">' . number_format(($this->totalTipos(1, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-phone fa-2x text-danger"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card btn btn-default"  style="background: #F3F4FF">
                            <a id="lnkcha" classs="link" href="#">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">CHAT</div>
                                            <div class="mb-0 font-weight-bold text-gray-800"><span class="contcha h1"><?= $this->totalTipos(5, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;"> <span class="h3"> ' . number_format(($this->totalTipos(5, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-comment fa-2x text-info"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--<div class="card btn btn-default"  style="background: #F3F4FF">
                            <a id="lnkopo" classs="link" href="<?= Yii::app()->createUrl('oportunidad') ?>">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">OPORTU.</div>
                                            <div class="mb-0 font-weight-bold text-gray-800"><span class="contopo h1"><?= $this->totalTipos(2, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;"> <span class="h3">' . number_format(($this->totalTipos(2, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-users fa-2x text-success"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>-->
                        <hr class="h1"></hr>
                        <div class="card btn btn-default"  style="background: #F3F4FF">
                            <a id="lnktot" classs="link" href="<?= Yii::app()->createUrl('conversion') ?>">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">TOTAL</div>
                                            <div class="h1 mb-0 font-weight-bold text-gray-800">
                                                <span class="contgen">
                                                    <?= $totalgeneral ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-signal fa-2x text-warning"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-11">
                <?= $content; ?>
            </div>
        </div>
        <?php
        ///////////////////////////Ventana modal destallado/////////////////////
        $this->beginWidget(
                'booster.widgets.TbModal', array(
            'id' => 'modalDetallado',
            "htmlOptions" => array(
                'size' => "modal-lg"
            )
        ));
        ?>
        <div id="modalcuerpodetallado"></div>
        <div id="modalwaitdetallado" style="display:none;">
            <div class="col-lg-12 modal-body" style="text-align:center;background-color:#fff;  border-radius: 10px;">
<?= CHtml::image(Yii::app()->baseUrl . "/images/loading.gif") ?>
                <br />
                Un Momento ... Espere...
            </div>
        </div>
<?php $this->endWidget(); ?>
<?php
///////////////////////////Ventana modal general/////////////////////
$this->beginWidget(
        'booster.widgets.TbModal', array(
    'id' => 'modalAlerta',
    "htmlOptions" => array(
        'size' => "modal-lg"
    )
));
?>
        <div id="modalcuerpo"></div>
        <div id="modalwait" style="display:none;">
            <div class="col-lg-12 modal-body" style="text-align:center;background-color:#fff;  border-radius: 10px;">
<?= CHtml::image(Yii::app()->baseUrl . "/images/loading.gif") ?>
                <br />
                Un Momento ... Espere...
            </div>
        </div>
<?php $this->endWidget(); ?>
        <footer>
            <div class="copyright-info">
                <p class="pull-right">Telesentinel Ltda - Dto Sistemas</p>
            </div>
        </footer>
    </body>
    <script>
        $(document).ready(function () {

            $("li").on("click", function () {
                $(".nav navbar-nav li").removeClass("active");
                $(this).addClass("active");
            });

            Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
                return {
                    radialGradient: {
                        cx: 0.5,
                        cy: 0.3,
                        r: 0.7
                    },
                    stops: [
                        [0, color],
                        [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                    ]
                };
            });

            document.getElementById('lnkweb').addEventListener('click', function (event) {
                event.preventDefault(); //esto cancela el comportamiento del click
                setTimeout(function () {
                    $("#modalAlerta").modal('show');
                    $('#modalwait').fadeIn('fast');
                    location.href = "web";
                }, 200);
            });
            document.getElementById('lnklla').addEventListener('click', function (event) {
                event.preventDefault(); //esto cancela el comportamiento del click
                setTimeout(function () {
                    $("#modalAlerta").modal('show');
                    $('#modalwait').fadeIn('fast');
                    location.href = "llamada";
                }, 200);
            });
            document.getElementById('lnkcha').addEventListener('click', function (event) {
                event.preventDefault(); //esto cancela el comportamiento del click
                setTimeout(function () {
                    $("#modalAlerta").modal('show');
                    $('#modalwait').fadeIn('fast');
                    location.href = "chat";
                }, 200);
            });
//            document.getElementById('lnkopo').addEventListener('click', function (event) {
//                event.preventDefault(); //esto cancela el comportamiento del click
//                setTimeout(function () {
//                    $("#modalAlerta").modal('show');
//                    $('#modalwait').fadeIn('fast');
//                    location.href = "oportunidad";
//                }, 200);
//            });


            var start = moment().startOf('month');
            var end = moment().endOf('month');
//            var start = moment().subtract(29, 'days');
//            var end = moment();

            function cb(start, end) {
                if ($('#range').val().length > 0) {
                    $('#reportrange span').html($('#range').val());
                } else {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            }

            function cb1(start, end) {
                $('#reportrange2 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
                    'Ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                    'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            $('#reportrange2').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
                    'Ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                    'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb1);

            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                spanCalendar(picker, 'range');
                $("#rango_fecha1").val(picker.startDate.format('YYYY-MM-DD'));
                $("#rango_fecha2").val(picker.endDate.format('YYYY-MM-DD'));
                renderizada();
            });

            $('#reportrange2').on('apply.daterangepicker', function (ev, picker) {
                spanCalendar(picker, 'range2');
                $("#rango_fecha3").val(picker.startDate.format('YYYY-MM-DD'));
                $("#rango_fecha4").val(picker.endDate.format('YYYY-MM-DD'));
                renderizada();
            });

            cb(start, end);
            cb1(start, end);
        });
    </script>
</html>