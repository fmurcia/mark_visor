<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- blueprint CSS framework --> 
            <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/sweetalert.css"/>
            <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css"/>
            <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/style.css"/>
            <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/custom.css"/>
            <link rel="shortcut icon" href="<?= Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />

            <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/sweetalert.min.js', CClientScript::POS_HEAD); ?>

            <title>TELESENTINEL</title>
    </head>
    <body class="fondologin">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="card o-hidden border-2 my-2">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="p-5">   
                                <!--<div class="bg-login-image"></div>-->
                                <div class="text-center">
                                    <h1 class="mb-4" style="color: #263279">Bienvenido!</h1>
                                    <strong style="color: #263279">TELELEGO</strong>,  Una Herramienta interna de <strong style="color: #263279">TELESENTINEL LTDA</strong>, si no tienes cuenta comunicate con el departamento de Sistemas</p>
                                </div>
                                <div class="col-lg-8 col-lg-offset-2">
                                    <?= $content; ?>
                                </div>

                                <div class="col-lg-8 col-lg-offset-2">
                                    <hr>
                                        <div class="text-center">
                                            <a class="small lnkforgot" href="#">Se te olvidó tu contraseña?</a>
                                            <br />
                                            <a class="small lnkregister" href="#">Crea una cuenta!</a>
                                            <br />
                                        </div>
                                        <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->beginWidget('booster.widgets.TbModal', array('id' => 'recordarClave', 'htmlOptions' => array('style' => 'z-index:1200')));
        ?>

        <div class="modal-header">
            <a class="close" data-dismiss="modal">&times;</a>
            <h4>Restablecer Clave</h4>
        </div>

        <?php
        $form1 = $this->beginWidget('CActiveForm', array(
            'id' => 'formClave',
            'enableAjaxValidation' => false,
            'action' => "javascript:void(0)",
        ));
        ?>
        <div class="modal-body">
            <center><img src="<?php echo Yii::app()->request->baseUrl . '/images/email.png' ?>" />
                <p>Escriba el correo electronico para poder enviarle el link de cambio de contraseña
                    <input type="text" name="emailr" id="emailr" style="text-align:right;width: 20%" class="form-control-feedback" placeholder="Email" required="true" />
                    @telesentinel.com</p>
            </center>
        </div>

        <div class="modal-footer">
            <?php
            $this->widget(
                    'booster.widgets.TbButton', array(
                'context' => 'primary',
                'label' => 'Enviar',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal', 'onclick' => 'enviarAutenticacion()'),
                    )
            );
            ?>
            <?php
            $this->widget(
                    'booster.widgets.TbButton', array(
                'label' => 'Cerrar',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
                    )
            );
            ?>
            <?php $this->endWidget(); ?>
        </div>

        <?php $this->endWidget(); ?>

        <script type="text/javascript">
            function enviarAutenticacion()
            {
                if ($('#emailr').val() == '')
                {
                    swal("¡Error!", "Email Vacio", "error");
                } else
                {
                    jQuery.ajax({
                        'type': 'POST',
                        'url': '<?= Yii::app()->createUrl('site/password') ?>',
                        'data': $("#formClave").serialize(),
                        'success':
                                function (respuesta)
                                {
                                    if (respuesta == 'OK')
                                    {
                                        swal({
                                            title: "Email Enviado",
                                            text: "Revisar Correspondencia",
                                            timer: 3000,
                                            showConfirmButton: false
                                        });
                                    } else
                                    {
                                        swal("¡Error!", "Email Incorrecto", "error");
                                    }
                                    document.getElementById("emailr").value = '';
                                },
                        'error':
                                function (m, e, a)
                                {
                                    swal("¡Error!", "Consulte al Administrador del Sistema...", "error");
                                },
                        'cache': false
                    });
                    return false;
                }
            }
        </script> 
    </body>
</html>