<!-- Page content -->
<div class="row">    
    <div class="col-xl-12 col-md-12 mb-12">
        <div class="card border-left-danger">
            <table class="table autoajuste">
                <tr>
                    <td class="mdaltwo mr-2 clsPend" data-id="1">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-connectdevelop fa-2x"></i><br>Leads Pendientes</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['nuevos'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['nuevos2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsDupl mr-2" data-id="2">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-files-o fa-2x"></i><br>Duplicados</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['duplicados'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['duplicados2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsErra mr-2" data-id="3">
                        <br>    
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-times fa-2x"></i><br>Errados</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['errados'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['errados2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsDesc mr-2" data-id="4">    
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-trash fa-2x"></i><br>Descartados</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['descartados'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['descartados2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsSegu mr-2" data-id="5">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-edit fa-2x"></i><br>En Gestion</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['seguimiento'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['seguimiento2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsCita mr-2" data-id="6">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-calendar fa-2x"></i><br>Citas</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['cita'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['cita2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsDesd mr-2" data-id="7">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-times-circle fa-2x"></i><br>Descartados DC</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['descartadosdc'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['descartadosdc2'] ?> </label></div>                
                    </td>
                    <td class="mdaltwo clsCoti mr-2" data-id="8">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-dollar fa-2x"></i><br>Cotizaciones</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['cotizados'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['cotizados2'] ?> </label></div>
                    </td>
                    <td class="mdaltwo clsVent mr-2" data-id="9">
                        <br>
                        <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-users fa-2x"></i><br>Ventas</div>
                        <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['venta'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['venta2'] ?> </label></div>
                    </td>                    
                    <td class="mr-2" style="font-size: 70pt">
                        <?= $totalregistrado ?>
                        <label style="display:<?= $oculto ?>"> / <?= $totalregistrado2 ?></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <?php
    if ($comparacion == 'true') :
        $div = 'col-xl-6 col-md-5 mb-6';
    else :
        $div = 'col-xl-12 col-md-12 mb-12';
    endif;
    ?>
    <div class="col-xl-12 col-md-12 mb-12">
        <div class="<?= $div ?> card border-left-danger shadow h-100 py-2" id="grafica1"  style="min-width: 310px; height: 330px; margin: 0 auto;"></div>
        <div class="<?= $div ?> card border-left-danger shadow h-100 py-2" id="grafica2"  style="min-width: 310px; height: 330px; margin: 0 auto; background: #FF8316; display: <?= $graficaoculta ?>"></div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
        <div class="card border-left-danger">
            <table class="table">
                <tr>
                    <th colspan="<?= $space + $space1 ?>" style="text-align:center">INFORME DE CAMPAÑAS</th>
                </tr>

                <tr class="info">
                    <th colspan="2" style="text-align:center">ESTADO</th><th style="text-align: center" colspan="<?= $space + 2 ?>"><?= $primerdia . ' / ' . $segundodia ?><label style="display:<?= $oculto ?>"> / <?= $tercerdia . ' / ' . $cuartodia ?> </label></th>
                </tr>

                <tr>
                    <th colspan="2">CAMPAÑA</th>
                    <?php
                    if ($campania == 'all') :
                        ?>
                        <th colspan="6" style="text-align:center; background: #bbbcbb">GOOGLE</th>
                        <th colspan="8" style="text-align:center; background: #89abce">FACEBOOK</th>
                        <th colspan="2" style="text-align:center; background: #9dd4d0">REGRISTROSEO</th>
                        <th colspan="2" style="text-align:center; background: #d36b7c">MAILCHIMP</th>
                        <?php
                    elseif ($campania == 'Adwords') :
                        ?>
                        <th colspan="6" style="text-align:center; background: #bbbcbb">GOOGLE</th>
                        <?php
                    elseif ($campania == 'Facebook') :
                        ?>
                        <th colspan="8" style="text-align:center; background: #89abce">FACEBOOK</th>
                        <?php
                    elseif ($campania == 'registroseo') :
                        ?>
                        <th colspan="2" style="text-align:center; background: #9dd4d0">REGRISTROSEO</th>
                        <?php
                    elseif ($campania == 'mailchimp') :
                        ?>
                        <th colspan="2" style="text-align:center; background: #d36b7c">MAILCHIMP</th>
                        <?php
                    endif;
                    ?>
                    <th colspan="2" style="text-align:center; background: #f3f1c0">TOTAL</th>
                </tr>
                <tr class="">
                    <th colspan="2">TOTAL REGISTROS</th>
                    <?php
                    if ($campania == 'all') :
                        ?>
                        <td colspan="3" class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_google) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_google2) ?></label></td>
                        <td colspan="3" class="h1" style="text-align:center; background: #bbbcbb"><?= number_format(array_sum($arr_google) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_google2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td colspan="4" class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_facebook) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_facebook2) ?></label></td>
                        <td colspan="4" class="h1" style="text-align:center; background: #89abce"><?= number_format(array_sum($arr_facebook) / (($totalregistrado > 0) ? $totalregistrado : 1) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_facebook2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <th style="text-align:center; background: #9dd4d0" class="h1"><?= array_sum($arr_seo) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_seo2) ?></label></th>
                        <th style="text-align:center; background: #9dd4d0" class="h1"><?= number_format(array_sum($arr_seo) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_seo2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></th>
                        <th style="text-align:center; background: #d36b7c" class="h1"><?= array_sum($arr_mailchimp) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_mailchimp2) ?></label></th>
                        <th style="text-align:center; background: #d36b7c" class="h1"><?= number_format(array_sum($arr_mailchimp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_mailchimp2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></th>
                        <?php
                    elseif ($campania == 'Adwords') :
                        ?>
                        <td colspan="3" class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_google) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_google2) ?></label></td>
                        <td colspan="3" class="h1" style="text-align:center; background: #bbbcbb"><?= number_format(array_sum($arr_google) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_google2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <?php
                    elseif ($campania == 'Facebook') :
                        ?>
                        <td colspan="4" class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_facebook) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_facebook2) ?></label></td>
                        <td colspan="4" class="h1" style="text-align:center; background: #89abce"><?= number_format(array_sum($arr_facebook) / (($totalregistrado > 0) ? $totalregistrado : 1) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_facebook2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <?php
                    elseif ($campania == 'registroseo') :
                        ?>
                        <th style="text-align:center; background: #9dd4d0" class="h1"><?= array_sum($arr_seo) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_seo2) ?></label></th>
                        <th style="text-align:center; background: #9dd4d0" class="h1"><?= number_format(array_sum($arr_seo) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_seo2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></th>
                        <?php
                    elseif ($campania == 'mailchimp') :
                        ?>
                        <th style="text-align:center; background: #d36b7c" class="h1"><?= array_sum($arr_mailchimp) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_mailchimp2) ?></label></th>
                        <th style="text-align:center; background: #d36b7c" class="h1"><?= number_format(array_sum($arr_mailchimp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_mailchimp2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></th>
                        <?php
                    endif;
                    ?>
                    <th colspan="2" style="text-align:center; background: #f3f1c0" class="h1"><?= $totalregistrado ?><label style="display:<?= $oculto ?>"> / <?= $totalregistrado2 ?></label></th>
                </tr>
                <tr class="">
                    <th colspan="2">SUB CAMPAÑA</th>
                    <?php
                    if ($campania == 'all') :
                        ?>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">ADWORDS</th>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">DISPLAY</th>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">VIDEO</th>

                        <th colspan="2" style="text-align:center; background: #89abce">FACEBOOK PAGO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">FACEBOOK SEO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">INSTAGRAM PAGO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">INSTAGRAM SEO</th>

                        <?php
                    elseif ($campania == 'Adwords') :
                        ?>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">BUSQUEDA</th>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">DISPLAY</th>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">VIDEO</th>
                        <?php
                    elseif ($campania == 'Facebook') :
                        ?>
                        <th colspan="2" style="text-align:center; background: #89abce">FACEBOOK PAGO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">FACEBOOK SEO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">INSTAGRAM PAGO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">INSTAGRAM SEO</th>
                        <?php
                    endif;
                    ?>
                </tr>
                <tr class="">
                    <th colspan="2">SUBTOTAL REGISTROS</th>
                    <?php
                    if ($campania == 'all') :
                        ?>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_busqueda) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_busqueda2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format((array_sum($arr_busqueda) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_busqueda2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_isplay) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_isplay2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format((array_sum($arr_isplay) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_isplay2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_video) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_video2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format((array_sum($arr_video) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_video2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_facebookp) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_facebookp2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format((array_sum($arr_facebookp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_facebook2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_facebooks) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_facebooks2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format((array_sum($arr_facebooks) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_facebooks2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_instagramp) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_instagramp2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format((array_sum($arr_instagramp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_instagramp2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_instagrams) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_instagrams2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format((array_sum($arr_instagrams) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_instagrams2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <?php
                    elseif ($campania == 'Adwords') :
                        ?>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_busqueda) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_busqueda2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format((array_sum($arr_busqueda) / (($totalregistrado > 0) ? $totalgoogle : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_busqueda2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_isplay) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_isplay2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format((array_sum($arr_isplay) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_isplay2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_video) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_video2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format((array_sum($arr_video) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>">/ <?= number_format(array_sum($arr_video2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <?php
                    elseif ($campania == 'Facebook') :
                        ?>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_facebookp) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_facebookp2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format((array_sum($arr_facebookp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_facebookp2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_facebooks) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_facebooks2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format((array_sum($arr_facebooks) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_facebooks2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_instagramp) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_instagramp2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format((array_sum($arr_instagramp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_instagramp2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_instagrams) ?><label style="display:<?= $oculto ?>"> / <?= array_sum($arr_instagrams2) ?></label></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format((array_sum($arr_instagrams) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100)) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format(array_sum($arr_instagrams2) / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                        <?php
                    endif;
                    ?>
                </tr>

                <tr class="info"><td colspan="<?= $space + 2 ?>"></td></tr>
                <tr class=""><th colspan="<?= $space + 2 ?>" style="text-align:center"> TOTAL REGISTRADO </th></tr>
                <?php
                $i = 1;
                foreach ($leads_general as $l => $ar) :
                    $ttgen = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $ar);
                    $ttgen2 = Contacto::model()->estadoRegistroDetallado($tipocontacto, $tercerdia, $cuartodia, $arr_reg, $ar);
                    ?>
                    <tr>
                        <th colspan="2"><?= $l ?></th>
                        <?php
                        foreach ($arr_medios as $m) :
                            if ($m == 'Adwords' || $m == 'Display' || $m == 'video') :
                                $color = "e2e2e2";
                            elseif ($m == 'facebook' || $m == 'facebooks' || $m == 'instagram' || $m == 'instagramp') :
                                $color = "d9ecfc";
                            elseif ($m == 'registroseo') :
                                $color = "dcf9f6";
                            elseif ($m == 'mailchimp') :
                                $color = "ead8db";
                            endif;
                            $cont = Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array($m));
                            $porc = number_format((Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array($m)) / (($totalregistrado > 0 ) ? $totalregistrado : 1) ) * 100);
                            $cont2 = Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $ar, $arr_reg, array($m));
                            $porc2 = number_format((Contacto::model()->getConTipFec($tercerdia, $cuartodia, $tipocontacto, $ar, $arr_reg, array($m)) / (($totalregistrado2 > 0 ) ? $totalregistrado2 : 1) ) * 100);
                            ?>
                            <td style="text-align: center; background: #<?= $color ?>" class="h4"><button class="btn btn-primary mdal" data-id="<?= $i . '-' . $m ?>"><?= $cont ?><label style="display:<?= $oculto ?>"> / <?= $cont2 ?></label></button></td>
                            <td style="text-align: center; background: #<?= $color ?>" class="h4"><?= $porc ?>%<label style="display:<?= $oculto ?>"> / <?= $porc2 ?>%</label></td>
                            <?php
                        endforeach;
                        ?>
                        <td style="text-align: center; background: #f4f2d7" class="h4"><button class="btn btn-primary mdal" data-id="<?= $i . '-all' ?>"><?= $ttgen ?><label style="display:<?= $oculto ?>"> / <?= $ttgen2 ?></label></button></td>
                        <td style="text-align: center; background: #f4f2d7" class="h4"><?= number_format($ttgen / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%<label style="display:<?= $oculto ?>"> / <?= number_format($ttgen2 / (($totalregistrado2 > 0) ? $totalregistrado2 : 1 ) * 100) ?>%</label></td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
                <tr class="info"><td colspan="<?= $space + 2 ?>"></td></tr>
            </table>
        </div>
    </div>
</div>
<?php
if ($comparacion == 'true') :
    $width = 700;
else :
    $width = 1680;
endif;
?>
<script>
    Highcharts.chart('grafica1', {
        chart: {
            type: 'line',
            width: <?= $width ?>
        },
        title: {
            text: 'LEADS POR CAMPAÑAS'
        },
        subtitle: {
            text: 'Web'
        },
        xAxis: {
            categories: <?= json_encode($categorias) ?>
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: <?= json_encode($grafica) ?>

    });

    Highcharts.chart('grafica2', {
        chart: {
            type: 'line',
            width: <?= $width ?>
        },
        title: {
            text: 'LEADS POR CAMPAÑAS'
        },
        subtitle: {
            text: 'Web'
        },
        xAxis: {
            categories: <?= json_encode($categorias2) ?>
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: <?= json_encode($grafica2) ?>

    });
</script>