<?php
$primerdia = Yii::app()->user->getState('primerdia');
$segundodia = Yii::app()->user->getState('segundodia');
$tercerdia = Yii::app()->user->getState('tercerdia');
$cuartodia = Yii::app()->user->getState('cuartodia');
$ciudad = Yii::app()->user->getState('regional');
$totalgeneral = $this->totalTipos(1, $primerdia, $segundodia, $ciudad) +
        //$this->totalTipos(2, $primerdia, $segundodia, $ciudad) + 
        $this->totalTipos(3, $primerdia, $segundodia, $ciudad) +
        $this->totalTipos(5, $primerdia, $segundodia, $ciudad)
?>

<input type="hidden" id="tipocontacto" name="tipocontacto" value="<?= $tipocontacto ?>">
<input type="hidden" id="totalregistrado" name="totalregistrado" value="<?= $totalregistrado ?>">

<center><h1><i class="fa fa-globe"></i> CAMPAÑA WEB</h1></center>
<div class="ocultarcontenidofiltrado">
    <div class="row">
        <div class="col-xl-12 col-md-12 mb-12"> 
            <div class="card border-left-danger shadow h-100">
                <table class="table autoajuste">
                    <tr>
                        <td class="mdaltwo clsPend mr-2" data-id="1">
                            <br>
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-connectdevelop fa-2x"></i><br>Leads Pendientes</div>
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['nuevos'] ?></div>
                        </td>
                        <td class="mdaltwo clsDupl mr-2" data-id="2">
                            <br>
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-envelope fa-2x"></i><br>En Mailing</div>
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['duplicados'] ?></div>
                        </td>
                        <td class="mdaltwo clsErra mr-2" data-id="3">
                            <br>    
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-times fa-2x"></i><br>Errados</div>
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['errados'] ?></div>
                        </td>
                        <td class="mdaltwo clsDesc mr-2" data-id="4">    
                            <br>
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-trash fa-2x"></i><br>Descartados AC</div>
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['descartados'] ?></div>
                        </td>
                        <td class="mdaltwo clsSegu mr-2" data-id="5">
                            <br>
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-edit fa-2x"></i><br>En Gestion</div>
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['seguimiento'] ?></div>
                        </td>
                        <td class="mdaltwo clsCita mr-2" data-id="6">
                            <br>
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-calendar fa-2x"></i><br>Citas</div>
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['cita'] ?></div>
                        </td>
                        <td class="mdaltwo clsDesd mr-2" data-id="7">
                            <br>
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-times-circle fa-2x"></i><br>Descartados DC</div>
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['descartadosdc'] ?></div>                
                        </td>
                        <td class="mdaltwo clsCoti mr-2" data-id="8">
                            <br>
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-dollar fa-2x"></i><br>Cotizaciones</div>  
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['cotizados'] ?></div>
                        </td>
                        <td class="mdaltwo clsVent mr-2" data-id="9">
                            <br>
                            <div class="text-xs font-weight-bold text-uppercase mb-1"><i class="fa fa-users fa-2x"></i><br>Ventas</div>
                            <div class="h1 mb-0 font-weight-bold"><?= $arr_tt['venta'] ?></div>
                        </td>                    
                        <td class="mr-2" style="font-size: 70pt">
                            <?= $totalregistrado ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12 col-md-12 mb-12">
            <div class="card border-left-danger" id="container"  style="min-width: 310px; height: 330px; margin: 0 auto"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12 col-md-12 mb-12">
            <div class="card border-left-danger">
                <table class="table">
                    <tr class="">
                        <th colspan="22" style="text-align:center">INFORME DE CAMPAÑAS</th>
                    </tr>
                    <tr class="info">
                        <th colspan="2" style="text-align:center">ESTADO</th><th style="text-align: center" colspan="20"><?= $primerdia . ' / ' . $segundodia ?></th>
                    </tr>
                    <tr class="">
                        <th colspan="2">CAMPAÑA</th>
                        <th colspan="6" style="text-align:center; background: #bbbcbb">GOOGLE</th>
                        <th colspan="8" style="text-align:center; background: #89abce">FACEBOOK</th>
                        <th colspan="2" style="text-align:center; background: #9dd4d0">REGRISTROSEO</th>
                        <th colspan="2" style="text-align:center; background: #d36b7c">MAILCHIMP</th>
                        <th colspan="2" style="text-align:center; background: #f3f1c0">TOTAL</th>
                    </tr>
                    <tr class="">
                        <th colspan="2">TOTAL REGISTROS</th>
                        <td class="h1" colspan="3" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_google) ?></td>
                        <td class="h1" colspan="3" style="text-align:center; background: #bbbcbb"><?= number_format(array_sum($arr_google) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        <td class="h1" colspan="4" style="text-align:center; background: #89abce"><?= array_sum($arr_facebook) ?></td>
                        <td class="h1" colspan="4" style="text-align:center; background: #89abce"><?= number_format(array_sum($arr_facebook) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        <th class="h1" style="text-align:center; background: #9dd4d0"><?= array_sum($arr_seo) ?></th>
                        <th class="h1" style="text-align:center; background: #9dd4d0"><?= number_format(array_sum($arr_seo) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</th>
                        <th class="h1" style="text-align:center; background: #d36b7c"><?= array_sum($arr_mailchimp) ?></th>
                        <th class="h1" style="text-align:center; background: #d36b7c"><?= number_format(array_sum($arr_mailchimp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</th>
                        <th class="h1" colspan="2" style="text-align:center; background: #f3f1c0"><?= $totalregistrado ?></th>
                    </tr>
                    <tr class="">
                        <th colspan="2">SUB CAMPAÑA</th>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">ADWORDS</th>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">DISPLAY</th>
                        <th colspan="2" style="text-align:center; background: #bbbcbb">VIDEO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">FACEBOOK PAGO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">FACEBOOK SEO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">INSTAGRAM PAGO</th>
                        <th colspan="2" style="text-align:center; background: #89abce">INSTAGRAM SEO</th>
                    </tr>
                    <tr class="">
                        <th colspan="2">SUBTOTAL REGISTROS</th>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_busqueda) ?></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format(array_sum($arr_busqueda) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_isplay) ?></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format(array_sum($arr_isplay) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= array_sum($arr_video) ?></td>
                        <td class="h1" style="text-align:center; background: #bbbcbb"><?= number_format(array_sum($arr_video) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_facebookp) ?></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format(array_sum($arr_facebookp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_facebooks) ?> </td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format(array_sum($arr_facebooks) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_instagramp) ?></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format(array_sum($arr_instagramp) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= array_sum($arr_instagrams) ?></td>
                        <td class="h1" style="text-align:center; background: #89abce"><?= number_format(array_sum($arr_instagrams) / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                    </tr>

                    <tr class="info"><td colspan="22"></td></tr>
                    <tr class=""><th colspan="22" style="text-align:center"> TOTAL REGISTRADO </th></tr>
                    <?php
                    $i = 1;
                    foreach ($leads_general as $l => $ar) :
                        $ttgen = Contacto::model()->estadoRegistroDetallado($tipocontacto, $primerdia, $segundodia, $arr_reg, $ar);
                        ?>
                        <tr>    
                            <th colspan="2" ><?= $l ?></th>
                            <?php
                            foreach ($arr_medios as $m) :
                                if ($m == 'Adwords' || $m == 'Display' || $m == 'video') :
                                    $color = "e2e2e2";
                                elseif ($m == 'facebook' || $m == 'facebooks' || $m == 'instagram' || $m == 'instagramp') :
                                    $color = "d9ecfc";
                                elseif ($m == 'registroseo') :
                                    $color = "dcf9f6";
                                elseif ($m == 'mailchimp') :
                                    $color = "ead8db";
                                endif;
                                ?>
                                <td style="text-align: center; background: #<?= $color ?>"  class="h4"><button class="btn btn-primary mdal" data-id="<?= $i . '-' . $m ?>"><?= Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array($m)) ?></button></td>
                                <td style="text-align: center; background: #<?= $color ?>"  class="h4"><?= number_format((Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array($m)) / (($totalregistrado > 0 ) ? $totalregistrado : 1) ) * 100) ?>%</td>
                                <?php
                            endforeach;
                            ?>
                            <td style="text-align: center; background: #f4f2d7"  class="h4"><button class="btn btn-primary mdal" data-id="<?= $i . '-all' ?>"><?= $ttgen ?></button></td>
                            <td style="text-align: center; background: #f4f2d7"  class="h4"><?= number_format($ttgen / (($totalregistrado > 0) ? $totalregistrado : 1 ) * 100) ?>%</td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                    <tr class="info"><td colspan="22"></td></tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="mostrarcontenidofiltrado"></div>
<script>
    Highcharts.chart('container', {
        chart: {
            type: 'line',
            width: 1680
        },
        title: {
            text: 'LEADS POR CAMPAÑAS'
        },
        subtitle: {
            text: 'WEB'
        },
        xAxis: {
            categories: <?= json_encode($categorias) ?>
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
                name: 'GOOGLE',
                data: <?= json_encode($arr_google) ?>
            }, {
                name: 'FACEBOOK',
                data: <?= json_encode($arr_facebook) ?>
            }, {
                name: 'SEO',
                data: <?= json_encode($arr_seo) ?>
            }, {
                name: 'MAILCHIMP',
                data: <?= json_encode($arr_mailchimp) ?>
            }
        ]
    });
</script>