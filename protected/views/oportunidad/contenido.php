<!-- Page content -->
<div class="row">                       
    <div class="col-xl-2 col-md-6 mb-4">    
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-3">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Descartados</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['descartados'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['descartados2'] ?> </label> </div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-trash fa-3x text-danger"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <div class="col-xl-2 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Leads Pendientes</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['nuevos'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['nuevos2'] ?> </label></div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-connectdevelop fa-3x  text-warning"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Seguimiento</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['seguimiento'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['seguimiento2'] ?> </label></div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-edit fa-3x text-primary"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Citas</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['cita'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['cita2'] ?> </label></div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-calendar fa-3x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Cotizaciones</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['cotizados'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['cotizados2'] ?> </label></div>
                    </div>
                    <div class="col-auto">  
                        <i class="fa fa-dollar fa-3x text-dark"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Ventas</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['venta'] ?><label style="display:<?= $oculto ?>"> / <?= $arr_tt['venta2'] ?> </label></div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-users fa-3x text-success"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>
<div class="row">
    <?php
    if ($comparacion == 'true') :
        $div = 'col-xl-6 col-md-5 mb-6';
    else :
        $div = 'col-xl-12 col-md-12 mb-12';
    endif;
    ?>
    <div class="col-xl-10 col-md-10 mb-10">
        <div class="<?= $div ?> card border-left-danger shadow h-100 py-2" id="container"  style="min-width: 310px; height: 330px; margin: 0 auto;"></div>
        <div class="<?= $div ?> card border-left-danger shadow h-100 py-2" id="container2"  style="min-width: 310px; height: 330px; margin: 0 auto; background: #FF8316; display: <?= $graficaoculta ?>"></div>
    </div>
    <div class="col-xl-2 col-md-2 mb-2" style="text-align: center; font-size: 80pt">
        <?= $totalregistrado ?>
        <label style="display:<?= $oculto ?>"> / <?= $totalregistrado2 ?></label>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-md-12 mb-12">
        <div class="card border-left-danger shadow h-100 py-2">
            <table class="table table-striped table-bordered">

                <tr>
                    <th colspan="<?= 22 ?>" style="text-align:center">INFORME DE CAMPAÑAS</th>
                </tr>

                <tr class="info">
                    <th colspan="2" style="text-align:center">ESTADO</th><th style="text-align: center" colspan="20"><?= $primerdia . ' / ' . $segundodia ?></th>
                </tr>
                <tr class="">
                    <th  colspan="2">TOTAL REGISTROS</th>
                    <th class="h1" colspan="2" style="text-align:center"><?= $totalregistrado ?></th>
                </tr>

                <tr class="info"><td colspan="22"></td></tr>
                <?php
                foreach ($leads_general as $l => $ar) :
                    ?>
                    <tr>
                        <th colspan="2" ><?= $l ?></th>
                        <td style="text-align: center;" class="h4"><?= Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array()) ?></td>
                        <td style="text-align: center;" class="h4"><?= number_format((Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array()) / (($totalregistrado > 0 ) ? $totalregistrado : 1) ) * 100) ?>%</td>
                    </tr>
                    <?php
                endforeach;
                ?>
                <tr class="info"><td colspan="22"></td></tr>
            </table>
        </div>
    </div>
</div>

<br><br><br>

<?php
if ($comparacion == 'true') :
    $width = 700;
else :
    $width = 1400;
endif;
?>

<script>
    Highcharts.chart('container', {
        chart: {
            type: 'line',
            width: <?= $width ?>
        },
        title: {
            text: 'LEADS POR CAMPAÑAS'
        },
        subtitle: {
            text: 'Oportunidad'
        },
        xAxis: {
            categories: <?= json_encode($categorias) ?>
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: <?= json_encode($grafica) ?>

    });

    Highcharts.chart('container2', {
        chart: {
            type: 'line',
            width: <?= $width ?>
        },
        title: {
            text: 'LEADS POR CAMPAÑAS'
        },
        subtitle: {
            text: 'Oportunidad'
        },
        xAxis: {
            categories: <?= json_encode($categorias2) ?>
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: <?= json_encode($grafica2) ?>

    });
</script>