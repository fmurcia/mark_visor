<div class="col-12">
    <br>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close"  style="color: black"></i></span>
    </button>
</div>
<div class="modal-header">
    <b><h4 class="modal-title"><?= $texto ?></h4></b>
</div>
<div class="modal-body">
    <?php
    if ($texto == 'DESCARTADOS' || $texto == 'ENGESTION') :
        ?>
        <div class="esquina_superior_derecha_multiple"><h1><?= $ttsin + $ttcon ?></h1></div>
        <?php
        $this->widget('booster.widgets.TbTabs', array(
            'id' => 'idtabs',
            'type' => 'tabs',
            'placement' => 'above', // 'above', 'right', 'below' or 'left'
            'encodeLabel' => false,
            'htmlOptions' => array('class' => 'nav-item'),
            'tabs' => array(
                array('id' => 'tab1', 'label' => '<i class="fa fa-calendar "></i> En Cita ', 'content' => $this->renderPartial('tab1_1', array('texto' => strtoupper($texto), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $medio, 'arr_reg' => $arr_reg, 'sin' => 'con', 'ttsin' => $ttsin, 'ttcon' => $ttcon), true), 'active' => true),
                array('id' => 'tab2', 'label' => '<i class="fa fa-users "></i> En Gestion ', 'content' => $this->renderPartial('tab2_1', array('texto' => strtoupper($texto), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $medio, 'arr_reg' => $arr_reg, 'sin' => 'sin', 'ttsin' => $ttsin, 'ttcon' => $ttcon), true)),
            ),
            'events' => array('shown' => 'js:loadContent')
        ));
    else :
        ?>
        <div class="esquina_superior_derecha_simple_comercial"><h1><?= $ttsin + $ttcon ?></h1></div>
        <?php
        echo $this->renderPartial('tab3_1', array('texto' => strtoupper($texto), 'primerdia' => $primerdia, 'segundodia' => $segundodia, 'tipocontacto' => $tipocontacto, 'arr_estado' => $arr_estado, 'medio' => $medio, 'arr_reg' => $arr_reg, 'sin' => '', 'ttsin' => $ttsin, 'ttcon' => $ttcon), true);
    endif;
    ?>
</div>
<div class="clearfix"></div>

<?php
function sort_by_orden($a, $b) {
    return $b['orden'] - $a['orden'];
}
?>