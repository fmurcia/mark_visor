<?php
$primerdia = Yii::app()->user->getState('primerdia');
$segundodia = Yii::app()->user->getState('segundodia');
$tercerdia = Yii::app()->user->getState('tercerdia');
$cuartodia = Yii::app()->user->getState('cuartodia');
$ciudad = Yii::app()->user->getState('regional');
$totalgeneral = $this->totalTipos(1, $primerdia, $segundodia, $ciudad) + $this->totalTipos(2, $primerdia, $segundodia, $ciudad) + $this->totalTipos(3, $primerdia, $segundodia, $ciudad) + $this->totalTipos(5, $primerdia, $segundodia, $ciudad)
?>

<input type="hidden" id="tipocontacto" name="tipocontacto" value="<?= $tipocontacto ?>">
<input type="hidden" id="totalregistrado" name="totalregistrado" value="<?= $totalregistrado ?>">

<div class="row" style="font-size: 9pt">
    <div class="col-lg-1">
        <div class="sidebar-nav">
            <div class="contadores">
                <div class="card btn btn-default"  style="background: #F3F4FF">
                    <a id="lnkweb" classs="link" href="<?= Yii::app()->createUrl('web') ?>">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">WEB</div>
                                    <div class="mb-0 font-weight-bold text-gray-800"><span class="contweb h1"><?= $this->totalTipos(3, $primerdia, $segundodia, $ciudad) . '</span>  <br> <hr style="border-top: 1px solid red;">  <span class="h3"> ' . number_format(($this->totalTipos(3, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-globe fa-3x text-primary"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card btn btn-default" style="background: #F3F4FF">
                    <a id="lnklla" classs="link" href="<?= Yii::app()->createUrl('llamada') ?>">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">LLAMADA</div>
                                    <div class="mb-0 font-weight-bold text-gray-800"><span class="contlla h1"><?= $this->totalTipos(1, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;">  <span class="h3">' . number_format(($this->totalTipos(1, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-phone fa-2x text-danger"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card btn btn-default"  style="background: #F3F4FF">
                    <a id="lnkcha" classs="link" href="<?= Yii::app()->createUrl('chat') ?>">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">CHAT</div>
                                    <div class="mb-0 font-weight-bold text-gray-800"><span class="contcha h1"><?= $this->totalTipos(5, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;"> <span class="h3"> ' . number_format(($this->totalTipos(5, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-comment fa-2x text-info"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="card btn btn-default"  style="background: #F3F4FF">
                    <a id="lnkopo" classs="link" href="<?= Yii::app()->createUrl('oportunidad') ?>">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">OPORTU.</div>
                                    <div class="mb-0 font-weight-bold text-gray-800"><span class="contopo h1"><?= $this->totalTipos(2, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;"> <span class="h3">' . number_format(($this->totalTipos(2, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-users fa-2x text-success"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <hr class="h1"></hr>
                <div class="card btn btn-default"  style="background: #F3F4FF">
                    <a id="lnktot" classs="link" href="<?= Yii::app()->createUrl('conversion') ?>">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">TOTAL</div>
                                    <div class="h1 mb-0 font-weight-bold text-gray-800"><span class="contgen">
                                            <?= $totalgeneral ?>
                                        </span></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-signal fa-2x text-warning"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-11">
        <div id="page-content-wrapper">
            <div class="page-content inset">
                <div class="row">                    
                    <div class="col-xl-12 col-md-12 mb-12">
                        <div class="card border-left-primary shadow h-100">
                            <div class="card-body">
                                <div class="col-lg-12">  
                                    <h1><i class="fa fa-file"></i> OPORTUNIDAD</h1>
                                </div>
                                <div class="col-lg-2">  
                                    <h2><i class="fa fa-home"></i> Ciudad </h2> 
                                    <select class="form-control" onchange="regional(this.value)" id="regional" name="regional">   
                                        <option value="all">..Todas..</option>
                                        <?php foreach ($regionales as $r) : ?>
                                            <option value="<?= $r->ID_Regional ?>"><?= $r->Descripcion ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <center><h2><i class="fa fa-clock-o"></i> Tiempo </h2></center>
                                    <div class="col-lg-6">
                                        <div style="width: 40px; height: 30px; background: black; position: absolute"></div>
                                        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 20px; border: 1px solid #ccc">
                                            <i class="fa fa-calendar"></i>
                                            <span></span><b class="caret"></b>
                                        </div>
                                    </div>    
                                    <div class="col-lg-6 comparacion" style="display:none">
                                        <div style="width: 40px; height: 30px; background: #FF8316; position: fixed"></div>
                                        <div id="reportrange2" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 20px; border: 1px solid #ccc">
                                            <i class="fa fa-calendar"></i>
                                            <span></span><b class="caret"></b>
                                        </div>
                                    </div>    
                                </div>
                                <div class="col-lg-1" style="text-align: center">
                                    <h2><i class="fa fa-globe"></i> Comparacion <input type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" id="toggle-event" onchange="comparacion($(this).prop('checked'))"></h2>
                                </div>
                            </div>
                        </div>      
                    </div>  
                </div>
                <div class="contenidofiltrado">                       
                    <div class="row">                       
                        <div class="col-xl-2 col-md-6 mb-4 mdaltwo" data-id="pendientes">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Leads Pendientes</div>
                                            <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['nuevos'] ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-connectdevelop fa-3x  text-warning"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 mb-4 mdaltwo" data-id="descartados">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Descartados</div>
                                            <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['descartados'] ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-trash fa-3x text-danger"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 mb-4 mdaltwo" data-id="engestion">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">En Gestion</div>
                                            <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['seguimiento'] ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-edit fa-3x text-primary"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 mb-4 mdaltwo" data-id="citas">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Citas</div>
                                            <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['cita'] ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-calendar fa-3x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 mb-4 mdaltwo" data-id="cotizaciones">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Cotizaciones</div>
                                            <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['cotizados'] ?></div>
                                        </div>
                                        <div class="col-auto">  
                                            <i class="fa fa-dollar fa-3x text-dark"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 mb-4 mdaltwo" data-id="ventas">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Ventas</div>
                                            <div class="h1 mb-0 font-weight-bold text-gray-800"><?= $arr_tt['venta'] ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-users fa-3x text-success"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    </div>
                    <div class="row">
                        <div class="col-xl-10 col-md-10 mb-10">
                            <div class="card border-left-danger shadow h-100 py-2" id="container"  style="min-width: 310px; height: 330px; margin: 0 auto"></div>
                        </div>
                        <div class="col-xl-2 col-md-2 mb-2" style="text-align: center; font-size: 80pt">
                            <?= $totalregistrado ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-md-12 mb-12">
                            <div class="card border-left-danger shadow h-100 py-2">
                                <table class="table table-striped table-bordered">
                                    <tr class="">
                                        <th colspan="4" style="text-align:center">INFORME DE CAMPAÑAS</th>
                                    </tr>
                                    <tr class="info">
                                        <th colspan="2" style="text-align:center">ESTADO</th><th style="text-align: center" colspan="20"><?= $primerdia . ' / ' . $segundodia ?></th>
                                    </tr>
                                    <tr class="">
                                        <th  colspan="2">TOTAL REGISTROS</th>
                                        <th class="h1" colspan="2" style="text-align:center"><?= $totalregistrado ?></th>
                                    </tr>

                                    <tr class="info"><td colspan="22"></td></tr>
                                    <?php
                                    foreach ($leads_general as $l => $ar) :
                                        ?>
                                        <tr>
                                            <th colspan="2" ><?= $l ?></th>
                                            <td style="text-align: center;" class="h4"><button class="btn btn-primary mdal" data-id="<?= $l . '-all' ?>"><?= Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array()) ?></button></td>
                                            <td style="text-align: center;" class="h4"><?= number_format((Contacto::model()->getConTipFec($primerdia, $segundodia, $tipocontacto, $ar, $arr_reg, array()) / (($totalregistrado > 0 ) ? $totalregistrado : 1) ) * 100) ?>%</td>
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>
                                    <tr class="info"><td colspan="22"></td></tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>

                <script>
                    Highcharts.chart('container', {
                        chart: {
                            type: 'line',
                            width: 1400
                        },
                        title: {
                            text: 'LEADS POR CAMPAÑA'
                        },
                        subtitle: {
                            text: 'OPORTUNIDAD'
                        },
                        xAxis: {
                            categories: <?= json_encode($categorias) ?>
                        },
                        yAxis: {
                            title: {
                                text: 'Cantidad'
                            }
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                                enableMouseTracking: false
                            }
                        },
                        series: <?= json_encode($grafica) ?>
                    });
                </script>
            </div>      
        </div>
    </div>      
</div>