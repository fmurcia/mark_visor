<?php
$primerdia = Yii::app()->user->getState('primerdia');
$segundodia = Yii::app()->user->getState('segundodia');
$tercerdia = Yii::app()->user->getState('tercerdia');
$cuartodia = Yii::app()->user->getState('cuartodia');
$ciudad = Yii::app()->user->getState('regional');

$totalgeneral = 
        $this->totalTipos(1, $primerdia, $segundodia, $ciudad) + 
//        $this->totalTipos(2, $primerdia, $segundodia, $ciudad) +
        $this->totalTipos(3, $primerdia, $segundodia, $ciudad) + 
        $this->totalTipos(5, $primerdia, $segundodia, $ciudad)
?>
<div class="card btn btn-default"  style="background: #F3F4FF">
    <a id="lnkweb" classs="link" href="<?= Yii::app()->createUrl('web') ?>">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">WEB</div>
                    <div class="mb-0 font-weight-bold text-gray-800"><span class="contweb h1"><?= $this->totalTipos(3, $primerdia, $segundodia, $ciudad) . '</span>  <br> <hr style="border-top: 1px solid red;">  <span class="h3"> ' . number_format(($this->totalTipos(3, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                </div>
                <div class="col-auto">
                    <i class="fa fa-globe fa-3x text-primary"></i>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="card btn btn-default" style="background: #F3F4FF">
    <a id="lnklla" classs="link" href="<?= Yii::app()->createUrl('llamada') ?>">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">LLAMADA</div>
                    <div class="mb-0 font-weight-bold text-gray-800"><span class="contlla h1"><?= $this->totalTipos(1, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;">  <span class="h3">' . number_format(($this->totalTipos(1, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                </div>
                <div class="col-auto">
                    <i class="fa fa-phone fa-2x text-danger"></i>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="card btn btn-default"  style="background: #F3F4FF">
    <a id="lnkcha" classs="link" href="<?= Yii::app()->createUrl('chat') ?>">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">CHAT</div>
                    <div class="mb-0 font-weight-bold text-gray-800"><span class="contcha h1"><?= $this->totalTipos(5, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;"> <span class="h3"> ' . number_format(($this->totalTipos(5, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                </div>
                <div class="col-auto">
                    <i class="fa fa-comment fa-2x text-info"></i>
                </div>
            </div>
        </div>
    </a>
</div>
<!--<div class="card btn btn-default"  style="background: #F3F4FF">
    <a id="lnkopo" classs="link" href="<?= Yii::app()->createUrl('oportunidad') ?>">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">OPORTU.</div>
                    <div class="mb-0 font-weight-bold text-gray-800"><span class="contopo h1"><?= $this->totalTipos(2, $primerdia, $segundodia, $ciudad) . ' </span> <br> <hr style="border-top: 1px solid red;"> <span class="h3">' . number_format(($this->totalTipos(2, $primerdia, $segundodia, $ciudad) / (($totalgeneral > 0) ? $totalgeneral : 1) * 100)) ?>%</span></div>
                </div>
                <div class="col-auto">
                    <i class="fa fa-users fa-2x text-success"></i>
                </div>
            </div>
        </div>
    </a>
</div>-->
<hr class="h1"></hr>
<div class="card btn btn-default"  style="background: #F3F4FF">
    <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">TOTAL</div>
                <div class="h1 mb-0 font-weight-bold text-gray-800">
                    <span class="contgen">
                        <?= $totalgeneral ?>
                    </span>
                </div>
            </div>
            <div class="col-auto">
                <i class="fa fa-signal fa-2x text-warning"></i>
            </div>
        </div>
    </div>
</div>