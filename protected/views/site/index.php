<div class="nav_menu col-lg-10 col-md-10 col-sx-12 col-xs-12">

</div>
<div class="col-lg-2 col-md-2 col-sx-12 col-xs-12 ajustetree">
    <div class="filtventas">

    </div>
</div>
<div class="col-lg-10 col-md-10 col-sx-12 col-xs-12">
    <div class="contadores">

    </div>
</div>
<?php
$contadoractual = Contacto::model()->getTemporalidadsemanal('0 day', $fechainicial, $fechafinal);
$registroactual = Contacto::model()->temporalidadContacto($fechainicial, $fechafinal, 'diario');
$totalactual = 0;
foreach ($registroactual as $c) :
    $totalactual += $c->Contador;
endforeach;
?>
<input type="hidden" id="contador" value="<?= $contadoractual['total'] /* $contadoractual */ ?>">

<div class="x_panel" style="font-size: 9pt">
    <div class="x_title">
        <h2> Filtro de Temporalidad</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
            <li><a href="#"><i class="fa fa-wrench"></i></a></li>
            <li><a href="#"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div style="top:50px;">
            <div class="col-sm-2" style="text-align:center">Atras<br>
                <?php
                $this->widget(
                        'booster.widgets.TbButton', array(
                    'url' => '#',
                    'context' => 'danger',
                    'encodeLabel' => false,
                    'size' => 'small',
                    'label' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                    'htmlOptions' => array('onclick' => 'change("atras")')
                        )
                );
                ?>
            </div>
            <div class="col-sm-2" style="text-align:center">
                Fecha Inicial
                <?php
                $this->widget(
                        'booster.widgets.TbDatePicker', array(
                    'name' => 'datefechainicial',
                    'value' => $fechainicial,
                    'htmlOptions' => array('placeholder' => 'Fecha Inicial', 'style' => 'text-align:center', 'id' => 'datefechainicial'),
                    'options' => array(
                        'format' => 'yyyy-mm-dd',
                        'language' => 'es',
                    )
                        )
                );
                ?>    
            </div>
            <div class="col-sm-2" style="text-align:center">
                Fecha Final
                <?php
                $this->widget(
                        'booster.widgets.TbDatePicker', array(
                    'name' => 'datefechafinal',
                    'value' => $fechafinal,
                    'htmlOptions' => array('placeholder' => 'Fecha Final', 'style' => 'text-align:center', 'onchange' => 'paginarFecha()', 'id' => 'datefechafinal'),
                    'options' => array(
                        'format' => 'yyyy-mm-dd',
                        'language' => 'es'
                    )
                        )
                );
                ?>
            </div>
            <div class="col-sm-2" style="text-align:center">Siguiente<br>
                <?php
                $this->widget(
                        'booster.widgets.TbButton', array(
                    'url' => '#',
                    'context' => 'danger',
                    'encodeLabel' => false,
                    'size' => 'small',
                    'label' => '<span class="glyphicon glyphicon-chevron-right"></span>',
                    'htmlOptions' => array('onclick' => 'change("siguiente")')
                        )
                );
                ?>
            </div>
            <div class="col-sm-4" style="text-align:center">
                <b>Total Registrado <br><?= Contacto::model()->getMes(date_format(date_create($contadoractual['fecha']), 'M-d')) . " " . Contacto::model()->getSemanario(date_format(date_create($contadoractual['fecha']), 'D')); ?></b>
                <p style="font-size: 40pt"><?= $totalactual ?></p>
            </div>
        </div>
        <div style="top:50px;" class="loadData">
            <?php
            $this->widget(
                    'booster.widgets.TbTabs', array(
                'tabs' => array(
                    array(
                        'icon' => 'earphone',
                        'label' => 'TIPO CONTACTO',
                        'content' => $this->renderPartial('tipocontacto', array('fechainicial' => $fechainicial, 'fechafinal' => $fechafinal, 'primerdia' => $primerdia, 'ultimodia' => $ultimodia, 'mes' => $mes), TRUE),
                        'active' => true,
                    ),
                    array(
                        'icon' => 'folder-open',
                        'label' => 'TOTAL MENSUAL',
                        'content' => $this->renderPartial('contadoresmes', array('primerdia' => $primerdia, 'ultimodia' => $ultimodia, 'mes' => $mes), TRUE),
                    ),
                    array(
                        'icon' => 'list',
                        'label' => 'ETAPAS',
                        'content' => $this->renderPartial('tabetapas', array('contadoractual' => $contadoractual['total'], 'fechainicial' => $fechainicial, 'fechafinal' => $fechafinal), TRUE),
                    ),
                    array(
                        'icon' => 'user',
                        'label' => 'ASESORES',
                        'content' => $this->renderPartial('asesores', array('contadoractual' => $contadoractual['total'], 'fechainicial' => $fechainicial, 'fechafinal' => $fechafinal), TRUE),
                    ),
                    array(
                        'icon' => 'map-marker',
                        'label' => 'ESTADISTICAS',
                        'content' => $this->renderPartial('estadistica', array('mes' => $mes, 'primerdia' => $primerdia, 'ultimodia' => $ultimodia), TRUE),
                    ),
                    array(
                        'icon' => 'globe',
                        'label' => 'CONVERSIONES',
                        'content' => $this->renderPartial('conversiones', array('mes' => $mes, 'fechainicial' => $primerdia, 'fechafinal' => $ultimodia), TRUE),
                    ),
                    array(
                        'icon' => 'calendar',
                        'label' => 'PROMOCIONES',
                        'content' => $this->renderPartial('promos', array('fechafinal' => $fechafinal, 'mes' => $mes), TRUE),
                    ),
                    array(
                        'icon' => 'edit',
                        'label' => 'CAMPAÑAS DIGITALES',
                        'content' => $this->renderPartial('informes', array('fechainicial' => $fechainicial, 'fechafinal' => $fechafinal, 'primerdia' => $primerdia, 'ultimodia' => $ultimodia, 'mes' => $mes), TRUE),
                    )
                ),
                    )
            );
            ?>
        </div>
    </div>
</div>

<?php
/*
 * ##############################################################################
 * Modal Detalle Tipo
 */
$this->beginWidget(
        'booster.widgets.TbModal', array('id' => 'detalletipo')
);
?>
<div class="modal-header">  
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>DETALLADO</h4>
</div>
<div class="modal-body" id="detalle" style="font-size: 9pt"></div>
<?php
$this->endWidget();
?>

<script type="text/javascript">
    $(document).ready(function () {

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2015',
            maxDate: '12/31/2017',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' a ',
            locale: {
                applyLabel: 'Enviar',
                cancelLabel: 'Limpiar',
                fromLabel: 'Desde',
                toLabel: 'Hasta',
                customRangeLabel: 'Rango',
                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' Hasta ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " a " + picker.endDate.format('MMMM D, YYYY'));
            $("#datefechainicial").val(picker.startDate.format('YYYY-MM-DD'));
            $("#datefechafinal").val(picker.endDate.format('YYYY-MM-DD'));
        });
        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>