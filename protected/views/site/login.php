
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login_form',
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>

<p style="color:white; font-weight: bold"><i class="fa fa-users"></i> Solo personal autorizado (*)</p>

<div class="form-group">
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <?= $form->textField($model, "username", array("class" => "form-control", "placeholder" => "Email")); ?>
            <?= $form->error($model, 'username'); ?>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <?= $form->passwordField($model, "password", array("class" => "form-control", "placeholder" => "Clave")); ?>
            <?= $form->error($model, 'password'); ?>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="text-align:center">
            <?php
            $this->widget(
                    'booster.widgets.TbButton', array(
                'buttonType' => 'submit',
                'label' => '<i class="fa fa-sign-in"></i> INGRESO',
                'encodeLabel' => false,
                'context' => 'default',
                'htmlOptions' => array(
                    'class' => 'btn btn-danger',
                    'style' => 'width: 100%;'
                ),
                    )
            );
            ?>
        </div>
    </div>    
</div>    
<!--<div class="form-group">
    <div class="row">    
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="text-align:center">
            <?php
            $this->widget(
                    'booster.widgets.TbButton', array(
                'buttonType' => 'button',
                'label' => '<i class="fa fa-refresh "></i> Olvido su Clave?',
                'encodeLabel' => false,
                'context' => 'default',
                'htmlOptions' => array(
                    'class' => 'btn btn-danger',
                    'style' => 'width: 100%;',
                    'data-toggle' => 'modal',
                    'data-target' => '#recordarClave'
                ),
                    )
            );
            ?>
        </div>
    </div>
</div>-->
<?php
$this->endWidget();
?>